﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CSMGLibrary
{
    public static class MapTerrainWriter
    {
        public enum Status
        {
            Success,
            InvalidGridDimension,
            Truncated,
            FileFailure
        }

        public const int MAP_ROW_SECTIONS = 8, MAP_COL_SECTIONS = 8;
        public const int MAP_SECTION_ROW_CELLS = 256, MAP_SECTION_COL_CELLS = 256;
        public static String WorkingDirectory = "";

        public static Status WriteMap(byte[,,] map)
        {
            if (map.GetLength(2) != 4)
                return Status.InvalidGridDimension;

            Status status = Status.Success;
            byte[,,] section = new byte[MAP_SECTION_ROW_CELLS, MAP_SECTION_COL_CELLS, map.GetLength(2)];

            int rowSections, colSections;
            rowSections = map.GetLength(0) / MAP_SECTION_ROW_CELLS;
            if (map.GetLength(0) % MAP_SECTION_ROW_CELLS > 0)
                rowSections++;
            if (rowSections >= MAP_ROW_SECTIONS)
            {
                rowSections = MAP_ROW_SECTIONS;
                status = Status.Truncated;
            }
            colSections = map.GetLength(1) / MAP_SECTION_COL_CELLS;
            if (map.GetLength(1) % MAP_SECTION_COL_CELLS > 0)
                colSections++;
            if (colSections >= MAP_COL_SECTIONS)
            {
                colSections = MAP_COL_SECTIONS;
                status = Status.Truncated;
            }


            for (int row = 0; row < rowSections; row++)
            {
                for (int col = 0; col < colSections; col++)
                {
                    for (int i = 0; i < MAP_SECTION_ROW_CELLS; i++)
                    {
                        for (int j = 0; j < MAP_SECTION_COL_CELLS; j++)
                        {
                            if ((i+row*MAP_SECTION_ROW_CELLS) >= map.GetLength(0) || (j+col*MAP_SECTION_COL_CELLS) >= map.GetLength(1))
                            {
                                for (int k = 0; k < section.GetLength(2); k++)
                                    section[i, j, k] = 0x00;
                                continue;
                            }

                            for (int k = 0; k < section.GetLength(2); k++)
                                section[i, j, k] = map[(i+row*MAP_SECTION_ROW_CELLS), (j+col*MAP_SECTION_COL_CELLS), k];
                        }
                    }

                    String name = "Monde_" + ((row * MAP_ROW_SECTIONS) + col);
                    WriteSection(section, name);
                }
            }


            return status;
        }

        private static Status WriteSection(byte[,,] section, String name)
        {
            Status status = Status.Success;

            FileStream fs;
            byte[] preFill = { 0x00, 0x00 };
            fs = File.Create(WorkingDirectory + name);
            if (!fs.CanWrite)
            {
                fs.Close();
                return Status.FileFailure;
            }

            fs.Write(preFill, 0, preFill.Length);
            for (int i = 0; i < section.GetLength(0); i++)
                for (int j = 0; j < section.GetLength(1); j++)
                    for (int k = 0; k < section.GetLength(2); k++)
                        fs.WriteByte(section[i, j, k]);
            fs.Close();

            return status;
        }

        public static bool WriteStartPoint(int xPos, int yPos, int zPos)
        {
            FileStream fs;
            fs = File.Create(WorkingDirectory + "Monde_Doodads");
            if (!fs.CanWrite)
                return false;

            int index;
            CoordinateConverter.coordsToIndex(xPos, yPos, zPos, out index);

            String str = "StartingPoint " + index + " Crystal " + index;
            char[] chars = str.ToCharArray();
            byte[] data = new byte[chars.Length];
            for (int i = 0; i < chars.Length; i++)
                data[i] = (byte)chars[i];
            fs.Write(data, 0, data.Length);
            fs.Close();
            return true;
        }

        public static bool WriteObjects(int mineXPos, int mineYPos, int mineZPos, int enemyXPos, int enemyYPos, int enemyZPos)
        {
            FileStream fs;
            fs = File.Create(WorkingDirectory + "Monde_Objets.json");
            if (!fs.CanWrite)
                return false;

            int mineIndex, enemyIndex;
            CoordinateConverter.coordsToIndex(mineXPos, mineYPos, mineZPos, out mineIndex);
            CoordinateConverter.coordsToIndex(enemyXPos, enemyYPos, enemyZPos, out enemyIndex);

            String str = "{\n  \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n  \"Version\": 2,\n  \"ObjectDynamiques\": {\n    \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n    \"ForetMagiques\": [],\n    \"Palettes\": [],\n    \"Balises\": [\n      {\n        \"$type\": \"BaliseAI, Assembly-UnityScript\",\n        \"index\": " + enemyIndex + ",\n        \"rayon\": 0,\n        \"groupeID\": 2,\n        \"groupeDesc\": \"Custom\",\n        \"groupeTag\": \"CorruptronSpawnPoint\"\n      }\n],\n    \"MineMagiques\": [\n      {\n        \"$type\": \"MineMagique, Assembly-UnityScript\",\n        \"index\": " + mineIndex + ",\n        \"dir\": 1,\n        \"network_id\": 0,\n      }\n    ]\n  }\n}";
            char[] chars = str.ToCharArray();
            byte[] data = new byte[chars.Length];
            for (int i = 0; i < chars.Length; i++)
                data[i] = (byte)chars[i];
            fs.Write(data, 0, data.Length);
            fs.Close();
            return true;
        }

        public static bool WriteTrees(bool[,] trees, byte[,] heights)
        {
            if (trees.GetLength(0) > heights.GetLength(0) || trees.GetLength(1) > heights.GetLength(1))
                return false;

            FileStream fs;
            fs = File.Create(WorkingDirectory + "Monde_Arbre");
            if (!fs.CanWrite)
                return false;

            int index;

            for (int i = 0; i < trees.GetLength(0); i++)
            {
                for (int j = 0; j < trees.GetLength(1); j++)
                {
                    if (trees[i, j] == true)
                    {
                        CoordinateConverter.coordsToIndex(i, j, heights[i, j], out index); // TODO: Figure out x,y polarity in Test
                        byte[] bytes = BitConverter.GetBytes(index);
                        fs.Write(bytes, 0, 4);
                    }
                }
            }
            fs.Close();

            return true;
        }

        public static bool ClearFolder()
        {
            for (int i = 0; i < MAP_ROW_SECTIONS * MAP_COL_SECTIONS; i++)
                File.Delete(WorkingDirectory + "Monde_" + i);
            File.Delete("Monde_Doodads");
            File.Delete("Monde_Arbre");
            File.Delete("Monde_Objets.json");

            return true;
        }
    }
}
