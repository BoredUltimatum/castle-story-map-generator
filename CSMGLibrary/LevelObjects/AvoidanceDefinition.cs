﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CSMGLibrary.UsefulDefines;

namespace CSMGLibrary.LevelObjects
{
    /// <summary>
    /// Defines the minimum distance of an object to other object types
    /// </summary>
    public class AvoidanceDefinition
    {
        /// <summary>
        /// The main object tested against
        /// </summary>
        public LevelObject levelObject;
        /// <summary>
        /// The minimum distance of object types without a specific definition
        /// </summary>
        public int minimumDistance;
        /// <summary>
        /// The per object type minimum distances
        /// </summary>
        public List<Tuple<ObjectType, int>> typeDefinitions;
        
        
        public AvoidanceDefinition(LevelObject levelObject, int minimumDistance)
        {
            this.levelObject = levelObject;
            this.minimumDistance = minimumDistance;
            this.typeDefinitions = new List<Tuple<ObjectType, int>>();
        }


        /// <summary>
        /// Adds a new type distance definition
        /// </summary>
        /// <param name="type">The type to avoid</param>
        /// <param name="minimumDistance">The minimum distance to the type</param>
        /// <returns>Whether an old definition was updated with the new minimum distance</returns>
        public bool addTypeDefinition(ObjectType type, int minimumDistance)
        {
            foreach (Tuple<ObjectType, int> def in typeDefinitions)
            {
                if (def.Item1 == type)
                {
                    def.Item2 = minimumDistance;
                    return true;
                }
            }
            typeDefinitions.Add(new Tuple<ObjectType, int>(type, minimumDistance));
            return false;
        }

        public bool withinDistance(LevelObject otherObject)
        {
            for (int i = 0; i < typeDefinitions.Count; i++)
                if (typeDefinitions[i].Item1 == otherObject.ObjectType)
                    return levelObject.withinDistance(otherObject, (double)(typeDefinitions[i].Item2));

            return levelObject.withinDistance(otherObject, minimumDistance);
        }

        /// <summary>
        /// Checks if the provided object is within the minimum distance
        /// </summary>
        /// <param name="type">The type to check against. If a definition doesn't exist for the provided type the default is used</param>
        /// <param name="x">The x coordinate of the point to check against</param>
        /// <param name="y">The y coordinate of the point to check against</param>
        /// <returns>Whether the point is within the minimum distance</returns>
        public bool withinDistance(ObjectType type, int x, int y)
        {
            for (int i = 0; i < typeDefinitions.Count; i++)
                if (typeDefinitions[i].Item1 == type)
                    return levelObject.withinDistance(x, y, (double)(typeDefinitions[i].Item2));

            return levelObject.withinDistance(x, y, minimumDistance);
        }
        
        /// <summary>
        /// Checks if the provided object is within the minimum distance
        /// </summary>
        /// <param name="x">The x coordinate of the point to check against</param>
        /// <param name="y">The y coordinate of the point to check against</param>
        /// <returns>Whether the point is within the minimum distance</returns>
        public bool withinDistance(int x, int y)
        {
            return levelObject.withinDistance(x, y, minimumDistance);
        }
    }
}
