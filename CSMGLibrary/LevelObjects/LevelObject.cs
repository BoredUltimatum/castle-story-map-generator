﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CSMGLibrary.UsefulDefines;

namespace CSMGLibrary.LevelObjects
{
    public class LevelObject
    {
        #region Properties
        #region Privates
        /// <summary>X position in the global reference frame of this object's origin</summary>
        private int xPos;
        /// <summary>Y position in the global reference frame of this object's origin</summary>
        private int yPos;
        /// <summary>Z position in the global reference frame of this object's origin</summary>
        private int zPos;
        /// <summary>Distance from xPos to xLBound with the object facing the default direction</summary>
        private int xUpperBuffer;
        /// <summary>Distance from xPos to xRBound with the object facing the default direction</summary>
        private int xLowerBuffer;
        /// <summary>Distance from yPos to yLBound with the object facing the default direction</summary>
        private int yUpperBuffer;
        /// <summary>Distance from yPos to yRBound with the object facing the default direction</summary>
        private int yLowerBuffer;
        /// <summary>Distance from zPos to zLBound with the object facing the default direction</summary>
        private int zUpperBuffer;
        /// <summary>Distance from zPos to zRBound with the object facing the default direction</summary>
        private int zLowerBuffer;
        /// <summary>The direction this object is facing in the global reference frame</summary>
        private Direction facingDirection;
        /// <summary>Lower X bound of this object in the global reference frame</summary>
        private int xLBound;
        /// <summary>Upper X bound of this object in the global reference frame</summary>
        private int xRBound;
        /// <summary>Lower Y bound of this object in the global reference frame</summary>
        private int yLBound;
        /// <summary>Upper Y bound of this object in the global reference frame</summary>
        private int yRBound;
        /// <summary>Lower Z bound of this object in the global reference frame</summary>
        private int zLBound;
        /// <summary>Upper Z bound of this object in the global reference frame</summary>
        private int zRBound;
        #endregion

        #region Publics
        /// <summary>
        /// The object's type
        /// </summary>
        public readonly ObjectType ObjectType;
        /// <summary>
        /// The default direction the object is facing in the global reference frame
        /// </summary>
        public const Direction DefaultFacingDirection = Direction.YF;

        #region GetterSetters
        /// <summary>
        /// The current direction the object is facing in the global reference frame
        /// </summary>
        public Direction FacingDirection
        {
            get { return facingDirection; }
            set { facingDirection = value; calculateDynamics(); }
        }
        /// <summary>Inclusive lower X bound of this object in the global reference frame</summary>
        public int XLBound { get { return xLBound; } }
        /// <summary>Inclusive upper X bound of this object in the global reference frame</summary>
        public int XRBound { get { return xRBound; } }
        /// <summary>Inclusive lower Y bound of this object in the global reference frame</summary>
        public int YLBound { get { return yLBound; } }
        /// <summary>Inclusive upper Y bound of this object in the global reference frame</summary>
        public int YRBound { get { return yRBound; } }
        /// <summary>Inclusive lower Z bound of this object in the global reference frame</summary>
        public int ZLBound { get { return zLBound; } }
        /// <summary>Inclusive upper Z bound of this object in the global reference frame</summary>
        public int ZRBound { get { return zRBound; } }

        /// <summary>X position in the global reference frame of this object's origin</summary>
        public int XPos
        {
            get { return xPos; }
            set
            {
                int diff = value - xPos;
                xLBound += diff;
                xRBound += diff;
                xPos = value;
            }
        }
        /// <summary>Y position in the global reference frame of this object's origin</summary>
        public int YPos
        {
            get { return yPos; }
            set
            {
                int diff = value - yPos;
                yLBound += diff;
                yRBound += diff;
                yPos = value;
            }
        }
        /// <summary>Z position in the global reference frame of this object's origin</summary>
        public int ZPos
        {
            get { return zPos; }
            set
            {
                int diff = value - zPos;
                zLBound += diff;
                zRBound += diff;
                zPos = value;
            }
        }
        /// <summary>Distance from xPos to xLBound with the object facing XF</summary>
        public int XUpperBuffer
        {
            get { return xUpperBuffer; }
            set
            {
                xUpperBuffer = value;
                calculateDynamics();
            }
        }
        /// <summary>Distance from xPos to xRBound with the object facing XF</summary>
        public int XLowerBuffer
        {
            get { return xLowerBuffer; }
            set
            {
                xLowerBuffer = value;
                calculateDynamics();
            }
        }
        /// <summary>Distance from yPos to yLBound with the object facing XF</summary>
        public int YUpperBuffer
        {
            get { return yUpperBuffer; }
            set
            {
                yUpperBuffer = value;
                calculateDynamics();
            }
        }
        /// <summary>Distance from yPos to yRBound with the object facing XF</summary>
        public int YLowerBuffer
        {
            get { return yLowerBuffer; }
            set
            {
                yLowerBuffer = value;
                calculateDynamics();
            }
        }
        /// <summary>Distance from zPos to zLBound with the object facing XF</summary>
        public int ZUpperBuffer
        {
            get { return zUpperBuffer; }
            set
            {
                zUpperBuffer = value;
                calculateDynamics();
            }
        }
        /// <summary>Distance from zPos to zRBound with the object facing XF</summary>
        public int ZLowerBuffer
        {
            get { return zLowerBuffer; }
            set
            {
                zLowerBuffer = value;
                calculateDynamics();
            }
        }
        #endregion
        #endregion
        #endregion


        public LevelObject(ObjectType objectType, int xLowerBuffer, int xUpperBuffer, int yLowerBuffer, int yUpperBuffer, int zLowerBuffer, int zUpperBuffer, int xPos, int yPos, int zPos, Direction facingDirection)
        {
            this.ObjectType = objectType;
            this.xLowerBuffer = xLowerBuffer;
            this.xUpperBuffer = xUpperBuffer;
            this.yLowerBuffer = yLowerBuffer;
            this.yUpperBuffer = yUpperBuffer;
            this.zLowerBuffer = zLowerBuffer;
            this.zUpperBuffer = zUpperBuffer;
            this.facingDirection = facingDirection;
            this.xPos = xPos;
            this.yPos = yPos;
            this.zPos = zPos;

            calculateDynamics();
        }

        public LevelObject(ObjectType objectType, int xLowerBuffer, int xUpperBuffer, int yLowerBuffer, int yUpperBuffer, int zLowerBuffer, int zUpperBuffer, int xPos, int yPos, int zPos)
            : this(objectType, xLowerBuffer, xUpperBuffer, yLowerBuffer, yUpperBuffer, zLowerBuffer, zUpperBuffer, xPos, yPos, zPos, DefaultFacingDirection) {}

        private void calculateDynamics()
        {
            switch (facingDirection)
            {
                case Direction.XB: // 90 Degrees CounterClockwise
                    /*     XU    
                     * YU <    YL
                     *     XL     */
                    xLBound = xPos - yUpperBuffer;
                    xRBound = xPos + yLowerBuffer;
                    yLBound = yPos - xLowerBuffer;
                    yRBound = yPos + xUpperBuffer;
                    break;
                case Direction.XF: // 90 Degrees Clockwise
                    /*     XL    
                     * YL    > YU
                     *     XU     */
                    xLBound = xPos - yLowerBuffer;
                    xRBound = xPos + yUpperBuffer;
                    yLBound = yPos - xUpperBuffer;
                    yRBound = yPos + xLowerBuffer;
                    break;
                case Direction.YB: // 180 Degrees
                    /*     YL    
                     * XU   v  XL
                     *     YU     */
                    xLBound = xPos - xUpperBuffer;
                    xRBound = xPos + xLowerBuffer;
                    yLBound = yPos - yUpperBuffer;
                    yRBound = yPos + yLowerBuffer;
                    break;
                case Direction.YF: // Default Direction
                    /*     YU    
                     * XL   ^  XU
                     *     YL     */
                    xLBound = xPos - xLowerBuffer;
                    xRBound = xPos + xUpperBuffer;
                    yLBound = yPos - yLowerBuffer;
                    yRBound = yPos + yUpperBuffer;
                    break;
                default:
                    // Shouldn't reach here...
                    xLBound = xPos;
                    xRBound = xPos;
                    yLBound = yPos;
                    yRBound = yPos;
                    break;
            }
            zLBound = zPos - zLowerBuffer;
            zRBound = zPos + zUpperBuffer;
        }


        public bool withinDistance(LevelObject otherObject, double distance)
        {
            // Modified from http://www.gamedev.net/topic/568111-any-quick-way-to-calculate-min-distance-between-2-bounding-boxes/
            double sqrDist = 0;

            if (otherObject.xRBound < xLBound)
            {
                double d = otherObject.xRBound - xLBound;
                sqrDist += d * d;
            }
            else if (otherObject.xLBound > xRBound)
            {
                double d = otherObject.xLBound - xRBound;
                sqrDist += d * d;
            }

            if (otherObject.yRBound < yLBound)
            {
                double d = otherObject.yRBound - yLBound;
                sqrDist += d * d;
            }
            else if (otherObject.yLBound > yRBound)
            {
                double d = otherObject.yLBound - yRBound;
                sqrDist += d * d;
            }

            if (Math.Sqrt(sqrDist) < distance)
                return true;
            else
                return false;
        }

        public bool withinDistance(int x, int y, double distance)
        {
            // Modified from http://www.gamedev.net/topic/568111-any-quick-way-to-calculate-min-distance-between-2-bounding-boxes/
            double sqrDist = 0;

            if (x < xLBound)
            {
                double d = x - xLBound;
                sqrDist += d * d;
            }
            else if (x > xRBound)
            {
                double d = x - xRBound;
                sqrDist += d * d;
            }

            if (y < yLBound)
            {
                double d = y - yLBound;
                sqrDist += d * d;
            }
            else if (y > yRBound)
            {
                double d = y - yRBound;
                sqrDist += d * d;
            }

            if (Math.Sqrt(sqrDist) < distance)
                return true;
            else
                return false;
        }
    }
}
