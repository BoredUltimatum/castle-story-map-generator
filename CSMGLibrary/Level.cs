﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using CSMGLibrary.UsefulDefines;
using CSMGLibrary.LevelObjects;
using CSMGLibrary.TerrainGenerators;

namespace CSMGLibrary
{
    /// <summary>
    /// Represents the level as terrain and objects
    /// </summary>
    public class Level
    {
        #region Properties
        /// <summary>
        /// The lowest height a non-empty cell can be
        /// </summary>
        public const byte LOWEST_HEIGHT = 1;
        /// <summary>
        /// Lateral length of level along x-axis
        /// </summary>
        private int xLength;
        /// <summary>
        /// Lateral length of level along x-axis
        /// </summary>
        public int XLength { get { return xLength; } }
        /// <summary>
        /// Lateral length of level along y-axis
        /// </summary>
        private int yLength;
        /// <summary>
        /// Lateral length of level along y-axis
        /// </summary>
        public int YLength { get { return yLength; } }
        /// <summary>
        /// The smaller of the x,y lengths
        /// </summary>
        private int smallerLength;
        /// <summary>
        /// The longest grid corner to corner distance
        /// </summary>
        private int maxGridDistance;
        /// <summary>
        /// This level's terrain
        /// </summary>
        private Terrain terrain;
        /// <summary>
        /// This level's terrain. THIS SHOULD NOT BE MODIFIED
        /// </summary>
        public Terrain Terrain { get { return terrain; } }
        /// <summary>
        /// Grid of tree cells
        /// </summary>
        private bool[,] trees;
        /// <summary>
        /// this level's tree grid. THIS SHOULD NOT BE MODIFIED
        /// </summary>
        public bool[,] Trees { get { return trees; } }
        /// <summary>
        /// Whether there are any trees defined
        /// </summary>
        private bool treesEmpty;
        private LevelObject crystal;
        public LevelObject Crystal { get { return crystal; } }
        private AvoidanceDefinition crystalDefinition;
        private LevelObject playerSpawn;
        public LevelObject PlayerSpawn { get { return playerSpawn; } }
        private AvoidanceDefinition playerSpawnDefinition;
        private List<LevelObject> mines;
        public ReadOnlyCollection<LevelObject> Mines { get { return mines.AsReadOnly(); } }
        private List<AvoidanceDefinition> mineDefinitions;
        public int mineCount { get { return mines.Count; } }
        private List<LevelObject> forests;
        public ReadOnlyCollection<LevelObject> Forests { get { return forests.AsReadOnly(); } }
        private List<AvoidanceDefinition> forestDefinitions;
        private List<LevelObject> enemySpawns;
        public ReadOnlyCollection<LevelObject> EnemySpawns { get { return enemySpawns.AsReadOnly(); } }
        private List<AvoidanceDefinition> enemySpawnDefinitions;
        public int enemyCount { get { return enemySpawns.Count; } }
        private List<AvoidanceDefinition> allAvoidanceDefinitions;

        private readonly int DEFAULT_CRYS_CRYS_DIST;
        private readonly int DEFAULT_CRYS_PLYR_DIST;
        private readonly int DEFAULT_CRYS_MINE_DIST;
        private readonly int DEFAULT_CRYS_FRST_DIST;
        private readonly int DEFAULT_CRYS_ENMY_DIST;
        private readonly int DEFAULT_PLYR_PLYR_DIST;
        private readonly int DEFAULT_PLYR_MINE_DIST;
        private readonly int DEFAULT_PLYR_FRST_DIST;
        private readonly int DEFAULT_PLYR_ENMY_DIST;
        private readonly int DEFAULT_MINE_MINE_DIST;
        private readonly int DEFAULT_MINE_FRST_DIST;
        private readonly int DEFAULT_MINE_ENMY_DIST;
        private readonly int DEFAULT_FRST_FRST_DIST;
        private readonly int DEFAULT_FRST_ENMY_DIST;
        private readonly int DEFAULT_ENMY_ENMY_DIST;
        private readonly int DEFAULT_CRYS_DIST;
        private readonly int DEFAULT_PLYR_DIST;
        private readonly int DEFAULT_MINE_DIST;
        private readonly int DEFAULT_FRST_DIST;
        private readonly int DEFAULT_ENMY_DIST;


        private Random rand;
        #endregion

        /// <summary>
        /// Creates a new level of the given dimensions and seed terrain. Any new cells past the seed dimensions are empty and seed cells past dimensions are truncated.
        /// </summary>
        /// <param name="xLength">X-axis length</param>
        /// <param name="yLength">Y-axis length</param>
        /// <param name="seedTerrain">Terrain to prefill from</param>
        public Level(int xLength, int yLength, Terrain seedTerrain)
        {
            this.xLength = xLength;
            this.yLength = yLength;
            if (seedTerrain != null)
                terrain = new Terrain(seedTerrain, xLength, yLength);
            else
                terrain = new Terrain(xLength, yLength);
            trees = new bool[xLength, yLength];
            clearTrees();
            for (int i = 0; i < xLength; i++)
                for (int j = 0; j < yLength; j++)
                    trees[i, j] = false;

            smallerLength = (xLength < yLength) ? xLength : yLength;
            maxGridDistance = (int)Math.Sqrt(Math.Pow(xLength, 2) + Math.Pow(yLength, 2));

            DEFAULT_CRYS_CRYS_DIST = 0;
            DEFAULT_CRYS_PLYR_DIST = 0;
            DEFAULT_CRYS_MINE_DIST = 10;
            DEFAULT_CRYS_FRST_DIST = 05;
            DEFAULT_CRYS_ENMY_DIST = 20;
            DEFAULT_PLYR_PLYR_DIST = 0;
            DEFAULT_PLYR_MINE_DIST = 10;
            DEFAULT_PLYR_FRST_DIST = 10;
            DEFAULT_PLYR_ENMY_DIST = 20;
            DEFAULT_MINE_MINE_DIST = 50;
            DEFAULT_MINE_FRST_DIST = 10;
            DEFAULT_MINE_ENMY_DIST = 50;
            DEFAULT_FRST_FRST_DIST = 5;
            DEFAULT_FRST_ENMY_DIST = 10;
            DEFAULT_ENMY_ENMY_DIST = 25;
            DEFAULT_CRYS_DIST = 10;
            DEFAULT_PLYR_DIST = 10;
            DEFAULT_MINE_DIST = 10;
            DEFAULT_FRST_DIST = 10;
            DEFAULT_ENMY_DIST = 10;

            crystal = new LevelObject(ObjectType.Crystal, 1, 1, 1, 1, 0, 2, 0, 0, 0, Direction.YF);
            crystalDefinition = new AvoidanceDefinition(crystal, DEFAULT_CRYS_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.Crystal, DEFAULT_CRYS_CRYS_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.PlayerSpawn, DEFAULT_CRYS_PLYR_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.EnemySpawn, DEFAULT_CRYS_ENMY_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.Mine, DEFAULT_CRYS_MINE_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.Forest, DEFAULT_CRYS_FRST_DIST);
            playerSpawn = new LevelObject(ObjectType.PlayerSpawn, 1, 1, 1, 1, 0, 2, 0, 0, 0, Direction.YF);
            playerSpawnDefinition = new AvoidanceDefinition(playerSpawn, DEFAULT_PLYR_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.PlayerSpawn, DEFAULT_PLYR_PLYR_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.Crystal, DEFAULT_CRYS_PLYR_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.EnemySpawn, DEFAULT_PLYR_ENMY_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.Mine, DEFAULT_PLYR_MINE_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.Forest, DEFAULT_PLYR_FRST_DIST);
            mines = new List<LevelObject>();
            mineDefinitions = new List<AvoidanceDefinition>();
            forests = new List<LevelObject>();
            forestDefinitions = new List<AvoidanceDefinition>();
            enemySpawns = new List<LevelObject>();
            enemySpawnDefinitions = new List<AvoidanceDefinition>();
            allAvoidanceDefinitions = new List<AvoidanceDefinition>();

            rand = new Random();
        }

        /// <summary>
        /// Creates a new level of the given dimensions
        /// </summary>
        /// <param name="xLength">X-axis length</param>
        /// <param name="yLength">Y-axis length</param>
        public Level(int xLength, int yLength)
            : this(xLength, yLength, null) {}

        /// <summary>
        /// Creates a new level of the given dimensions and seed terrain. Any new cells past the seed dimensions are empty and seed cells past dimensions are truncated.
        /// </summary>
        /// <param name="seedTerrain">Terrain to prefill from</param>
        public Level(Terrain seedTerrain)
            : this(seedTerrain.XLength, seedTerrain.YLength, seedTerrain) {}

        /// <summary>
        /// Resizes the level truncating overflow or filling new cells with null values.
        /// </summary>
        /// <param name="newXLength">New X-axis length</param>
        /// <param name="newYLength">New Y-axis length</param>
        public void Resize(int newXLength, int newYLength)
        {
            bool[,] newTrees = new bool[newXLength, newYLength];
            terrain.Resize(newXLength, newYLength);

            for (int i = 0; i < newXLength; i++)
                for (int j = 0; j < newYLength; j++)
                    if (i < xLength && j < yLength)
                        newTrees[i, j] = trees[i, j];
                    else
                        newTrees[i, j] = false;

            xLength = newXLength;
            yLength = newYLength;
            trees = newTrees;
        }

        /// <summary>
        /// Populates the terrain using the provided generator
        /// </summary>
        /// <param name="generator">The generator to populate the terrain with</param>
        public void GenerateTerrain(ITerrainGenerator generator)
        {
            clearAll();
            generator.GenerateTerrain(terrain);
        }

        /// <summary>
        /// Removes all but the largest island in the terrain
        /// </summary>
        public void ReduceToLargestIsland()
        {
            terrain.removeAllButLargestIsland();
        }

        /// <summary>
        /// Fills the terrain with trees
        /// </summary>
        public void GenerateTrees()
        {
            // Noise grid for now

            NoiseGeneratorNS.NoiseGenerator noiseGenerator = NoiseGeneratorNS.NoiseGenerator.GENERATOR;
            noiseGenerator.generateNoise(2, 10.0, 0.085, 0.3);

            const int sparsity = 3;
            clearTrees();

            for (int i = 1; i < xLength - 1; i++)
            {
                for (int j = 1; j < yLength - 1; j++)
                {
                    double noise = noiseGenerator.Noise(i, j);
                    if (noise != 1.0 || terrain.Heights[i, j] == 0)
                        trees[i, j] = false;
                    else
                    {
                        // Check against other trees
                        for (int r = i - sparsity; r <= i + sparsity; r++)
                        {
                            for (int c = j - sparsity; c <= j + sparsity; c++)
                            {
                                if (r >= 0 && r < xLength && c >= 0 && c < yLength
                                    && (trees[r, c] == true))
                                    goto invalidSpot;
                            }
                        }

                        // Check if next to ledge/cliff
                        for (int r = i-1; r <= i+1; r++)
                            for (int c = j-1; c <= j+1; c++)
                                if (r >= 0 && r < xLength && c >= 0 && c < yLength
                                    && (terrain.Heights[i, j] - terrain.Heights[r, c]) > 1)
                                    goto invalidSpot;

                        // Check against other objects' proximities
                        if (crystal.withinDistance(i, j, sparsity))
                            goto invalidSpot;
                        if (playerSpawn.withinDistance(i, j, sparsity))
                            goto invalidSpot;
                        foreach (LevelObject mine in mines)
                            if (mine.withinDistance(i, j, sparsity+5))
                                goto invalidSpot;
                        foreach (LevelObject forest in forests)
                            if (forest.withinDistance(i, j, sparsity))
                                goto invalidSpot;
                        foreach (LevelObject enemy in enemySpawns)
                            if (enemy.withinDistance(i, j, sparsity+10))
                                goto invalidSpot;

                        // If reached here no collisions
                        trees[i, j] = true;
                        treesEmpty = false;
                        continue;

                    invalidSpot:
                        trees[i, j] = false;
                    }
                }
            }
        }

        /// <summary>
        /// Finds a suitable spot and places the crystal and player spawn
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool placePlayer()
        {
            bool status = false;
            allAvoidanceDefinitions.Remove(crystalDefinition);
            allAvoidanceDefinitions.Remove(playerSpawnDefinition);
            if (terrain.PlaceLevelObjectNearestPoint(5, 5, rand.Next(xLength), rand.Next(yLength), playerSpawnDefinition, allAvoidanceDefinitions))
            {
                crystal.XPos = playerSpawn.XPos;
                crystal.YPos = playerSpawn.YPos;
                crystal.ZPos = playerSpawn.ZPos;
                status = true;
            }
            allAvoidanceDefinitions.Add(crystalDefinition);
            allAvoidanceDefinitions.Add(playerSpawnDefinition);

            return status;
        }

        /// <summary>
        /// Randomly places a mine in the level
        /// </summary>
        /// <returns>The mine created or null</returns>
        public LevelObject placeMine()
        {
            return placeMine(DEFAULT_MINE_MINE_DIST, DEFAULT_CRYS_MINE_DIST, DEFAULT_PLYR_MINE_DIST, DEFAULT_MINE_ENMY_DIST, DEFAULT_MINE_FRST_DIST);
        }

        /// <summary>
        /// Randomly places a mine in the level
        /// </summary>
        /// <param name="minimumMineDist">The minimum [0 to 1] percentage distance of the terrain between mines</param>
        /// <param name="minimumCrystalDist">The minimum [0 to 1] percentage distance of the terrain between the mine and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum [0 to 1] percentage distance of the terrain between the mine and the player spawn</param>
        /// <param name="minimumEnemyDist">The minimum [0 to 1] percentage distance of the terrain between the mine and enemy spawns</param>
        /// <param name="minimumForestDist">The minimum [0 to 1] percentage distance of the terrain between the mine and forests</param>
        /// <returns>The mine created or null</returns>
        public LevelObject placeMine(double minimumMineDist, double minimumCrystalDist, double minimumPlayerDist, double minimumEnemyDist, double minimumForestDist)
        {
            int mineDist = (minimumMineDist < 0) ? DEFAULT_MINE_MINE_DIST : (int)(maxGridDistance * minimumMineDist);
            int crysDist = (minimumCrystalDist < 0) ? DEFAULT_CRYS_MINE_DIST : (int)(maxGridDistance * minimumCrystalDist);
            int plyrDist = (minimumPlayerDist < 0) ? DEFAULT_PLYR_MINE_DIST : (int)(maxGridDistance * minimumPlayerDist);
            int enmyDist = (minimumEnemyDist < 0) ? DEFAULT_MINE_ENMY_DIST : (int)(maxGridDistance * minimumEnemyDist);
            int frstDist = (minimumForestDist < 0) ? DEFAULT_MINE_FRST_DIST : (int)(maxGridDistance * minimumForestDist);
            return placeMine(mineDist, crysDist, plyrDist, enmyDist, frstDist);
        }

        /// <summary>
        /// Randomly places a mine in the level
        /// </summary>
        /// <param name="minimumMineDist">The minimum distance between mines</param>
        /// <param name="minimumCrystalDist">The minimum distance between the mine and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum distance between the mine and the player spawn</param>
        /// <param name="minimumEnemyDist">The minimum distance between the mine and enemy spawns</param>
        /// <param name="minimumForestDist">The minimum distance between the mine and forests</param>
        /// <returns>The mine created or null</returns>
        public LevelObject placeMine(int minimumMineDist, int minimumCrystalDist, int minimumPlayerDist, int minimumEnemyDist, int minimumForestDist)
        {
            LevelObject newMine = new LevelObject(ObjectType.Mine, 3, 0, 3, 0, 0, 3, 0, 0, 0, Direction.YF);
            AvoidanceDefinition newMineDefinition = new AvoidanceDefinition(newMine, DEFAULT_MINE_DIST);
            newMineDefinition.addTypeDefinition(ObjectType.Crystal, minimumCrystalDist);
            newMineDefinition.addTypeDefinition(ObjectType.PlayerSpawn, minimumPlayerDist);
            newMineDefinition.addTypeDefinition(ObjectType.EnemySpawn, minimumEnemyDist);
            newMineDefinition.addTypeDefinition(ObjectType.Mine, minimumMineDist);
            newMineDefinition.addTypeDefinition(ObjectType.Forest, minimumForestDist);
            if (terrain.PlaceLevelObjectNearestPoint(3, 3, rand.Next(xLength), rand.Next(yLength), newMineDefinition, allAvoidanceDefinitions))
            {
                mines.Add(newMine);
                mineDefinitions.Add(newMineDefinition);
                allAvoidanceDefinitions.Add(newMineDefinition);
                int newHeight = newMine.ZRBound + 1;
                byte z = (newHeight > byte.MaxValue) ? byte.MaxValue : (byte)newHeight;
                int xMin, xMax, yMin, yMax;
                switch (newMine.FacingDirection)
                {
                    case Direction.XF:
                        xMin = newMine.XLBound - 1;
                        xMax = newMine.XRBound - 1;
                        yMin = newMine.YLBound + 1;
                        yMax = newMine.YRBound - 1;
                        break;
                    case Direction.XB:
                        xMin = newMine.XLBound + 1;
                        xMax = newMine.XRBound + 1;
                        yMin = newMine.YLBound - 1;
                        yMax = newMine.YRBound + 1;
                        break;
                    case Direction.YF:
                        xMin = newMine.XLBound - 1;
                        xMax = newMine.XRBound + 1;
                        yMin = newMine.YLBound - 1;
                        yMax = newMine.YRBound - 1;
                        break;
                    case Direction.YB:
                        xMin = newMine.XLBound + 1;
                        xMax = newMine.XRBound + 1;
                        yMin = newMine.YLBound - 1;
                        yMax = newMine.YRBound + 1;
                        break;
                    default:
                        xMin = newMine.XLBound;
                        xMax = newMine.XRBound;
                        yMin = newMine.YLBound;
                        yMax = newMine.YRBound;
                        break;
                }
                for (int x = xMin; x <= xMax; x++)
                    for (int y = yMin; y <= yMax; y++)
                        if (x >= 0 && x < xLength && y >= 0 && y < yLength)
                            terrain.Heights[x, y] = z;
                return newMine;
            }
            
            return null;
        }

        /// <summary>
        /// Randomly places as many mines as possible up to the given amount in the level
        /// </summary>
        /// <param name="amount">The number of mines to try to place</param>
        /// <returns>The mines created</returns>
        public List<LevelObject> placeMines(int amount)
        {
            List<LevelObject> mines = new List<LevelObject>();
            LevelObject newMine;
            while (mines.Count < amount && (newMine = placeMine()) != null)
                mines.Add(newMine);
            return mines;
        }

        /// <summary>
        /// Randomly places as many mines as possible up to the given amount in the level
        /// </summary>
        /// <param name="amount">The number of mines to try to place</param>
        /// <param name="minimumMineDist">The minimum [0 to 1] percentage distance of the terrain between mines</param>
        /// <param name="minimumCrystalDist">The minimum [0 to 1] percentage distance of the terrain between mines and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum [0 to 1] percentage distance of the terrain between mines and the player spawn</param>
        /// <param name="minimumEnemyDist">The minimum [0 to 1] percentage distance of the terrain between mines and enemy spawns</param>
        /// <param name="minimumForestDist">The minimum [0 to 1] percentage distance of the terrain between mines and forests</param>
        /// <returns>The mines created</returns>
        public List<LevelObject> placeMines(int amount, double minimumMineDist, double minimumCrystalDist, double minimumPlayerDist, double minimumEnemyDist, double minimumForestDist)
        {
            int mineDist = (minimumMineDist < 0) ? DEFAULT_MINE_MINE_DIST : (int)(maxGridDistance * minimumMineDist);
            int crysDist = (minimumCrystalDist < 0) ? DEFAULT_CRYS_MINE_DIST : (int)(maxGridDistance * minimumCrystalDist);
            int plyrDist = (minimumPlayerDist < 0) ? DEFAULT_PLYR_MINE_DIST : (int)(maxGridDistance * minimumPlayerDist);
            int enmyDist = (minimumEnemyDist < 0) ? DEFAULT_MINE_ENMY_DIST : (int)(maxGridDistance * minimumEnemyDist);
            int frstDist = (minimumForestDist < 0) ? DEFAULT_MINE_FRST_DIST : (int)(maxGridDistance * minimumForestDist);
            
            List<LevelObject> mines = new List<LevelObject>();
            LevelObject newMine;
            while (mines.Count < amount && (newMine = placeMine(mineDist, crysDist, plyrDist, enmyDist, frstDist)) != null)
                mines.Add(newMine);
            return mines;
        }

        /// <summary>
        /// Randomly places as many mines as possible up to the given amount in the level
        /// </summary>
        /// <param name="amount">The number of mines to try to place</param>
        /// <param name="minimumMineDist">The minimum distance between mines</param>
        /// <param name="minimumCrystalDist">The minimum distance between mines and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum distance between mines and the player spawn</param>
        /// <param name="minimumEnemyDist">The minimum distance between mines and enemy spawns</param>
        /// <param name="minimumForestDist">The minimum distance between mines and forests</param>
        /// <returns>The mines created</returns>
        public List<LevelObject> placeMines(int amount, int minimumMineDist, int minimumCrystalDist, int minimumPlayerDist, int minimumEnemyDist, int minimumForestDist)
        {
            List<LevelObject> mines = new List<LevelObject>();
            LevelObject newMine;
            while (mines.Count < amount && (newMine = placeMine(minimumMineDist, minimumCrystalDist, minimumPlayerDist, minimumEnemyDist, minimumForestDist)) != null)
                mines.Add(newMine);
            return mines;
        }

        /// <summary>
        /// Randomly places an enemy spawn in the level
        /// </summary>
        /// <returns>The enemy spawn created or null</returns>
        public LevelObject placeEnemy()
        {
            return placeEnemy(DEFAULT_ENMY_ENMY_DIST, DEFAULT_CRYS_ENMY_DIST, DEFAULT_PLYR_ENMY_DIST, DEFAULT_MINE_ENMY_DIST, DEFAULT_FRST_ENMY_DIST);
        }

        /// <summary>
        /// Randomly places an enemy spawn in the level
        /// </summary>
        /// <param name="minimumEnemyDist">The minimum [0 to 1] percentage distance of the terrain between the enemy spawn and other enemy spawns</param>
        /// <param name="minimumCrystalDist">The minimum [0 to 1] percentage distance of the terrain between the enemy spawn and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum [0 to 1] percentage distance of the terrain between the enemy spawn and player spawns</param>
        /// <param name="minimumMineDist">The minimum [0 to 1] percentage distance of the terrain between the enemy spawn and mines</param>
        /// <param name="minimumForestDist">The minimum [0 to 1] percentage distance of the terrain between the enemy spawn and forests</param>
        /// <returns>The enemy spawn created or null</returns>
        public LevelObject placeEnemy(double minimumEnemyDist, double minimumCrystalDist, double minimumPlayerDist, double minimumMineDist, double minimumForestDist)
        {
            int enmyDist = (minimumEnemyDist < 0) ? DEFAULT_ENMY_ENMY_DIST : (int)(maxGridDistance * minimumEnemyDist);
            int crysDist = (minimumCrystalDist < 0) ? DEFAULT_CRYS_ENMY_DIST : (int)(maxGridDistance * minimumCrystalDist);
            int plyrDist = (minimumPlayerDist < 0) ? DEFAULT_PLYR_ENMY_DIST : (int)(maxGridDistance * minimumPlayerDist);
            int mineDist = (minimumMineDist < 0) ? DEFAULT_MINE_ENMY_DIST : (int)(maxGridDistance * minimumMineDist);
            int frstDist = (minimumForestDist < 0) ? DEFAULT_FRST_ENMY_DIST : (int)(maxGridDistance * minimumForestDist);
            return placeEnemy(enmyDist, crysDist, plyrDist, mineDist, frstDist);
        }

        /// <summary>
        /// Randomly places an enemy spawn in the level
        /// </summary>
        /// <param name="minimumEnemyDist">The minimum distance between the enemy spawn and other enemy spawns</param>
        /// <param name="minimumCrystalDist">The minimum distance between the enemy spawn and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum distance between the enemy spawn and player spawns</param>
        /// <param name="minimumMineDist">The minimum distance between the enemy spawn and mines</param>
        /// <param name="minimumForestDist">The minimum distance between the enemy spawn and forests</param>
        /// <returns>The enemy spawn created or null</returns>
        public LevelObject placeEnemy(int minimumEnemyDist, int minimumCrystalDist, int minimumPlayerDist, int minimumMineDist, int minimumForestDist)
        {
            LevelObject newEnemy = new LevelObject(ObjectType.EnemySpawn, 1, 1, 1, 1, 0, 2, 0, 0, 0, Direction.YF);
            AvoidanceDefinition newEnemyDefinition = new AvoidanceDefinition(newEnemy, DEFAULT_ENMY_DIST);
            newEnemyDefinition.addTypeDefinition(ObjectType.Crystal, minimumCrystalDist);
            newEnemyDefinition.addTypeDefinition(ObjectType.PlayerSpawn, minimumPlayerDist);
            newEnemyDefinition.addTypeDefinition(ObjectType.EnemySpawn, minimumEnemyDist);
            newEnemyDefinition.addTypeDefinition(ObjectType.Mine, minimumMineDist);
            newEnemyDefinition.addTypeDefinition(ObjectType.Forest, minimumForestDist);
            if (terrain.PlaceLevelObjectNearestPoint(3, 3, rand.Next(xLength), rand.Next(yLength), newEnemyDefinition, allAvoidanceDefinitions))
            {
                enemySpawns.Add(newEnemy);
                enemySpawnDefinitions.Add(newEnemyDefinition);
                allAvoidanceDefinitions.Add(newEnemyDefinition);

                return newEnemy;
            }

            return null;
        }

        /// <summary>
        /// Randomly places as many enemy spawns as possible up to the given amount in the level
        /// </summary>
        /// <param name="amount">The number of enemy spawns to try to place</param>
        /// <returns>The enemy spawns created</returns>
        public List<LevelObject> placeEnemies(int amount)
        {
            List<LevelObject> enemies = new List<LevelObject>();
            LevelObject newEnemy;
            while (enemies.Count < amount && (newEnemy = placeEnemy()) != null)
                enemies.Add(newEnemy);
            return enemies;
        }

        /// <summary>
        /// Randomly places as many enemy spawns as possible up to the given amount in the level
        /// </summary>
        /// <param name="amount">The number of enemy spawns to try to place</param>
        /// <param name="minimumEnemyDist">The minimum [0 to 1] percentage distance of the terrain between enemy spawns</param>
        /// <param name="minimumCrystalDist">The minimum [0 to 1] percentage distance of the terrain between enemy spawns and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum [0 to 1] percentage distance of the terrain between enemy spawns and the player spawn</param>
        /// <param name="minimumMineDist">The minimum [0 to 1] percentage distance of the terrain between enemy spawns and mines</param>
        /// <param name="minimumForestDist">The minimum [0 to 1] percentage distance of the terrain between enemy spawns and forests</param>
        /// <returns>The enemy spawns created</returns>
        public List<LevelObject> placeEnemies(int amount, double minimumEnemyDist, double minimumCrystalDist, double minimumPlayerDist, double minimumMineDist, double minimumForestDist)
        {
            int enmyDist = (minimumEnemyDist < 0) ? DEFAULT_ENMY_ENMY_DIST : (int)(maxGridDistance * minimumEnemyDist);
            int crysDist = (minimumCrystalDist < 0) ? DEFAULT_CRYS_ENMY_DIST : (int)(maxGridDistance * minimumCrystalDist);
            int plyrDist = (minimumPlayerDist < 0) ? DEFAULT_PLYR_ENMY_DIST : (int)(maxGridDistance * minimumPlayerDist);
            int mineDist = (minimumMineDist < 0) ? DEFAULT_MINE_ENMY_DIST : (int)(maxGridDistance * minimumMineDist);
            int frstDist = (minimumForestDist < 0) ? DEFAULT_FRST_ENMY_DIST : (int)(maxGridDistance * minimumForestDist);
            
            List<LevelObject> enemies = new List<LevelObject>();
            LevelObject newEnemies;
            while (enemies.Count < amount && (newEnemies = placeEnemy(enmyDist, crysDist, plyrDist, mineDist, frstDist)) != null)
                enemies.Add(newEnemies);
            return enemies;
        }

        /// <summary>
        /// Randomly places as many enemy spawns as possible up to the given amount in the level
        /// </summary>
        /// <param name="amount">The number of enemy spawns to try to place</param>
        /// <param name="minimumEnemyDist">The minimum distance between enemy spawns</param>
        /// <param name="minimumCrystalDist">The minimum distance between enemy spawns and the crystal</param>
        /// <param name="minimumPlayerDist">The minimum distance between enemy spawns and the player spawn</param>
        /// <param name="minimumMineDist">The minimum distance between enemy spawns and mines</param>
        /// <param name="minimumForestDist">The minimum distance between enemy spawns and forests</param>
        /// <returns>The enemy spawns created</returns>
        public List<LevelObject> placeEnemies(int amount, int minimumEnemyDist, int minimumCrystalDist, int minimumPlayerDist, int minimumMineDist, int minimumForestDist)
        {
            List<LevelObject> enemies = new List<LevelObject>();
            LevelObject newEnemies;
            while (enemies.Count < amount && (newEnemies = placeEnemy(minimumEnemyDist, minimumCrystalDist, minimumPlayerDist, minimumMineDist, minimumForestDist)) != null)
                enemies.Add(newEnemies);
            return enemies;
        }

        public void clearTrees()
        {
            for (int i = 0; i < xLength; i++)
                for (int j = 0; j < yLength; j++)
                    trees[i, j] = false;
            treesEmpty = true;
        }

        public void resetCrystal()
        {
            //crystal;
            //crystalDefinition;

            crystal.XPos = 0;
            crystal.YPos = 0;
            crystal.ZPos = 0;
            crystalDefinition.addTypeDefinition(ObjectType.Crystal, DEFAULT_CRYS_CRYS_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.PlayerSpawn, DEFAULT_CRYS_PLYR_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.EnemySpawn, DEFAULT_CRYS_ENMY_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.Mine, DEFAULT_CRYS_MINE_DIST);
            crystalDefinition.addTypeDefinition(ObjectType.Forest, DEFAULT_CRYS_FRST_DIST);
            allAvoidanceDefinitions.Remove(crystalDefinition);
        }

        public void resetPlayer()
        {
            //playerSpawn;
            //playerSpawnDefinition;

            playerSpawn.XPos = 0;
            playerSpawn.YPos = 0;
            playerSpawn.ZPos = 0;
            playerSpawnDefinition.addTypeDefinition(ObjectType.PlayerSpawn, DEFAULT_PLYR_PLYR_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.Crystal, DEFAULT_CRYS_PLYR_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.EnemySpawn, DEFAULT_PLYR_ENMY_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.Mine, DEFAULT_PLYR_MINE_DIST);
            playerSpawnDefinition.addTypeDefinition(ObjectType.Forest, DEFAULT_PLYR_FRST_DIST);
            allAvoidanceDefinitions.Remove(playerSpawnDefinition);
        }

        public void clearMine(LevelObject mine)
        {
            allAvoidanceDefinitions.RemoveAll(x => x.levelObject == mine);
            mineDefinitions.RemoveAll(x => x.levelObject == mine);
            foreach (LevelObject m in mines)
            {
                if (m == mine)
                {
                    // Remove ground border
                    int xMin, xMax, yMin, yMax;
                    switch (m.FacingDirection)
                    {
                        case Direction.XF:
                            xMin = m.XLBound - 1;
                            xMax = m.XRBound - 1;
                            yMin = m.YLBound + 1;
                            yMax = m.YRBound - 1;
                            break;
                        case Direction.XB:
                            xMin = m.XLBound + 1;
                            xMax = m.XRBound + 1;
                            yMin = m.YLBound - 1;
                            yMax = m.YRBound + 1;
                            break;
                        case Direction.YF:
                            xMin = m.XLBound - 1;
                            xMax = m.XRBound + 1;
                            yMin = m.YLBound - 1;
                            yMax = m.YRBound - 1;
                            break;
                        case Direction.YB:
                            xMin = m.XLBound + 1;
                            xMax = m.XRBound + 1;
                            yMin = m.YLBound - 1;
                            yMax = m.YRBound + 1;
                            break;
                        default:
                            xMin = m.XLBound;
                            xMax = m.XRBound;
                            yMin = m.YLBound;
                            yMax = m.YRBound;
                            break;
                    }
                    for (int i = xMin; i <= xMax; i++)
                        for (int j = yMin; j <= yMax; j++)
                            terrain.Heights[i, j] = terrain.Heights[m.XPos, m.YPos];
                }
            }
            mines.RemoveAll(x => x == mine);

        }

        public void clearMines(List<LevelObject> minesCollection)
        {
            foreach (LevelObject mine in minesCollection)
                clearMine(mine);
        }

        public void clearMines(Collection<LevelObject> minesCollection)
        {
            foreach (LevelObject mine in minesCollection)
                clearMine(mine);
        }

        public void clearMines()
        {
            //mines;
            //mineDefinitions;
            //allAvoidanceDefinitions;

            foreach (AvoidanceDefinition ad in mineDefinitions)
                allAvoidanceDefinitions.Remove(ad);
            mineDefinitions.Clear();
            // Remove ground buffer
            foreach (LevelObject mine in mines)
            {
                int xMin, xMax, yMin, yMax;
                switch (mine.FacingDirection)
                {
                    case Direction.XF:
                        xMin = mine.XLBound-1;
                        xMax = mine.XRBound-1;
                        yMin = mine.YLBound+1;
                        yMax = mine.YRBound-1;
                        break;
                    case Direction.XB:
                        xMin = mine.XLBound+1;
                        xMax = mine.XRBound+1;
                        yMin = mine.YLBound-1;
                        yMax = mine.YRBound+1;
                        break;
                    case Direction.YF:
                        xMin = mine.XLBound-1;
                        xMax = mine.XRBound+1;
                        yMin = mine.YLBound-1;
                        yMax = mine.YRBound-1;
                        break;
                    case Direction.YB:
                        xMin = mine.XLBound+1;
                        xMax = mine.XRBound+1;
                        yMin = mine.YLBound-1;
                        yMax = mine.YRBound+1;
                        break;
                    default:
                        xMin = mine.XLBound;
                        xMax = mine.XRBound;
                        yMin = mine.YLBound;
                        yMax = mine.YRBound;
                        break;
                }
                for (int i = xMin; i <= xMax; i++)
                    for (int j = yMin; j <= yMax; j++)
                        terrain.Heights[i, j] = terrain.Heights[mine.XPos, mine.YPos];
            }
            mines.Clear();
        }

        public void clearForests()
        {
            //forests;
            //forestDefinitions;
            //allAvoidanceDefinitions;

            foreach (AvoidanceDefinition ad in forestDefinitions)
                allAvoidanceDefinitions.Remove(ad);
            forestDefinitions.Clear();
            forests.Clear();
        }

        public void clearEnemy(LevelObject enemy)
        {
            allAvoidanceDefinitions.RemoveAll(x => x.levelObject == enemy);
            enemySpawnDefinitions.RemoveAll(x => x.levelObject == enemy);
            enemySpawns.RemoveAll(x => x == enemy);
        }

        public void clearEnemies(List<LevelObject> enemiesCollection)
        {
            foreach (LevelObject enemy in enemiesCollection)
                clearEnemy(enemy);
        }

        public void clearEnemies()
        {
            //enemySpawns;
            //enemySpawnDefinitions;
            //allAvoidanceDefinitions;

            foreach (AvoidanceDefinition ad in enemySpawnDefinitions)
                allAvoidanceDefinitions.Remove(ad);
            enemySpawnDefinitions.Clear();
            enemySpawns.Clear();
        }

        public void clearObjects()
        {
            clearMines();
            clearForests();
            clearEnemies();
        }

        public void clearAll()
        {
            resetCrystal();
            resetPlayer();
            clearObjects();
            clearTrees();
            terrain.Clear();
        }

        public void WriteLevel()
        {
#if DEBUG
            MapWriter.WorkingDirectory = "C:/Program Files (x86)/Steam/steamapps/common/Castle Story/Info/Niveaux/Test Random Generation/";
#endif
            MapWriter.ClearFolder();
            MapWriter.WriteTerrain(terrain);
            MapWriter.WriteDoodads(playerSpawn.XPos, playerSpawn.YPos, playerSpawn.ZPos, crystal.XPos, crystal.YPos, crystal.ZPos);
            MapWriter.WriteObjects(forests, mines, enemySpawns);
            if (!treesEmpty)
                MapWriter.WriteTrees(trees, terrain.Heights);

        }

    }
}
