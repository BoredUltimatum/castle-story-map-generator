﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NoiseGeneratorNS;
using CSMGLibrary.UsefulDefines;

namespace CSMGLibrary
{
    public class LevelOld
    {
        public const byte LOWEST_HEIGHT = 1;
        /// <summary>
        /// Grid of terrain cell heights
        /// </summary>
        public readonly byte[,] Heights;
        /// <summary>
        /// Heights grid is populated with valid data
        /// </summary>
        private bool heightsValid;
        /// <summary>
        /// Grid of terrain cell lows
        /// </summary>
        public readonly byte[,] Lows;
        /// <summary>
        /// Lows grid is populated with valid data
        /// </summary>
        private bool lowsValid;
        /// <summary>
        /// Private terrain grid for writing to file
        /// </summary>
        private readonly byte[,,] Terrain;
        /// <summary>
        /// Terrain grid is valid (e.g. highs/lows haven't changed)
        /// </summary>
        private bool terrainValid;
        /// <summary>
        /// Grid of tree cells
        /// </summary>
        public readonly bool[,] Trees;
        /// <summary>
        /// Trees grid is valid (e.g. underlying terrain hasn't changed)
        /// </summary>
        private bool treesValid;
        /// <summary>
        /// Rows of level cells
        /// </summary>
        public readonly int rows;
        /// <summary>
        /// Columns of level cells
        /// </summary>
        public readonly int cols;
        /// <summary>
        /// NoiseGenerator for randomly populating grids
        /// </summary>
        private readonly NoiseGenerator noiseGenerator;

        private int Octaves;
        private double Amplitude;
        private double Frequency;
        private double Persistence;

        private readonly Random rand;

        private int playerX;
        public int PlayerX
        {
            get { return playerX; }
        }

        private int playerY;
        public int PlayerY
        {
            get { return playerY; }
        }

        private int playerZ;
        public int PlayerZ
        {
            get { return playerZ; }
        }

        private bool playerValid;

        private int mineX;
        public int MineX
        {
            get { return mineX; }
        }

        private int mineY;
        public int MineY
        {
            get { return mineY; }
        }

        private int mineZ;
        public int MineZ
        {
            get { return mineZ; }
        }

        private bool mineValid;

        private int enemyX;
        public int EnemyX
        {
            get { return enemyX; }
        }

        private int enemyY;
        public int EnemyY
        {
            get { return enemyY; }
        }

        private int enemyZ;
        public int EnemyZ
        {
            get { return enemyZ; }
        }

        private bool enemyValid;

        /// <summary>
        /// Defines an object's x and y coordinates and the minimum distance from other objects
        /// </summary>
        public class ObjectTuple
        {
            public readonly int distance, xPos, yPos;
            public ObjectTuple(int x, int y, int d) { xPos = x; yPos = y; distance = d; }
            public bool withinDistance(int x, int y) { return (Math.Sqrt((double)(Math.Pow(x - xPos, 2)) + (double)(Math.Pow(y - yPos, 2))) < (double)distance); }
            public bool withinDistance(int x, int y, int dist) { return (Math.Sqrt((double)(Math.Pow(x - xPos, 2)) + (double)(Math.Pow(y- yPos, 2))) < (double)dist); }
        }



        public LevelOld(int rows, int cols, int octaves, double amplitude, double frequency, double persistence)
        {
            this.rows = rows;
            this.cols = cols;
            Heights = new byte[rows, cols];
            Lows = new byte[rows, cols];
            Terrain = new byte[rows, cols, 4];
            Trees = new bool[rows, cols];
            invalidateAll();
            this.Octaves = octaves;
            this.Amplitude = amplitude;
            this.Frequency = frequency;
            this.Persistence = persistence;
            noiseGenerator = new NoiseGenerator(octaves, amplitude, frequency, persistence);
            rand = new Random();
        }



        private void invalidateAll()
        {
            heightsValid = false;
            lowsValid = false;
            terrainValid = false;
            treesValid = false;
            playerValid = false;
            mineValid = false;
            enemyValid = false;
        }
        
        public bool GenerateTerrain(byte upperBound, byte variation, byte thickness)
        {
            return GenerateTerrain(upperBound, byte.MaxValue, variation, thickness);
        }

        public bool GenerateTerrain(byte upperBound, byte variation, byte thickness, int octaves, double amplitude, double frequency, double persistence)
        {
            this.Octaves = octaves;
            this.Amplitude = amplitude;
            this.Frequency = frequency;
            this.Persistence = persistence;
            return GenerateTerrain(upperBound, byte.MaxValue, variation, thickness);
        }

        public bool GenerateTerrain(byte upperBound, byte lowerBound, byte variation, byte thickness, int octaves, double amplitude, double frequency, double persistence)
        {
            this.Octaves = octaves;
            this.Amplitude = amplitude;
            this.Frequency = frequency;
            this.Persistence = persistence;
            return GenerateTerrain(upperBound, lowerBound, variation, thickness);
        }

        public bool GenerateTerrain(byte upperBound, byte lowerBound, byte variation, byte thickness)
        {
            if (upperBound < LOWEST_HEIGHT)
                return false;

            invalidateAll();
            noiseGenerator.generateNoise(Octaves, Amplitude, Frequency, Persistence);

            // Heights
            for (int i = 0; i < Heights.GetLength(0); i++)
            {
                for (int j = 0; j < Heights.GetLength(1); j++)
                {
                    float offset = (float)noiseGenerator.Noise(i, j);
                    offset *= (float)variation;
                    int offsetInt = (int)offset;
                    byte offsetByte = (byte)offsetInt;
                    Heights[i, j] = (byte)(upperBound + offsetByte);
                }
            }
            heightsValid = true;

            // Lows
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    byte lowest = byte.MaxValue;

                    for (int r = i - 2; r < i + 2; r++)
                    {
                        for (int c = j - 2; c < j + 2; c++)
                        {
                            if (r >= 0 && r < rows && c >= 0 && c < cols
                                && Heights[r, c] < lowest)
                                lowest = Heights[r, c];
                        }
                    }

                    lowest -= thickness;
                    if (lowest < 0)
                        lowest = 0;
                    else if (lowest > lowerBound)
                        lowest = lowerBound;

                    Lows[i, j] = lowest;
                }
            }
            lowsValid = true;

            return true;
        }

        public bool FillTreeGrid()
        {
            return FillTreeGrid(new LevelOld.ObjectTuple[] { new LevelOld.ObjectTuple(playerX, playerY, 10), new LevelOld.ObjectTuple(mineX, mineY, 25), new LevelOld.ObjectTuple(enemyX, enemyY, 25) });
        }

        public bool FillTreeGrid(int octaves, double amplitude, double frequency, double persistence)
        {
            return FillTreeGrid(new LevelOld.ObjectTuple[] { new LevelOld.ObjectTuple(playerX, playerY, 10), new LevelOld.ObjectTuple(mineX, mineY, 25), new LevelOld.ObjectTuple(enemyX, enemyY, 25) }, octaves, amplitude, frequency, persistence);
        }

        public bool FillTreeGrid(ObjectTuple[] otherObjects)
        {
            return FillTreeGrid(otherObjects, 2, 10.0, 0.085, 0.3);
        }

        public bool FillTreeGrid(ObjectTuple[] otherObjects, int octaves, double amplitude, double frequency, double persistence)
        {
            if (heightsValid != true || lowsValid != true)
                return false;

            noiseGenerator.generateNoise(octaves, amplitude, frequency, persistence);

            const int sparsity = 3;
            int rows = Trees.GetLength(0), cols = Trees.GetLength(1);
            // Clear
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < cols; j++)
                    Trees[i, j] = false;

            for (int i = 1; i < rows-1; i++)
            {
                for (int j = 1; j < cols-1; j++)
                {
                    double noise = noiseGenerator.Noise(i, j);
                    if (noise != 1.0)
                        Trees[i, j] = false;
                    else
                    {
                        if (otherObjects == null)
                            Trees[i, j] = true;
                        else
                        {
                            // Check against otherObjects proximities
                            for (int k = 0; k < otherObjects.Length; k++)
                            {
                                if (otherObjects[k].withinDistance(i, j, sparsity))
                                    goto invalidSpot;
                            }
                            // Check against other trees
                            for (int r = i - sparsity; r < i + sparsity; r++)
                            {
                                for (int c = j - sparsity; c < j + sparsity; c++)
                                {
                                    if (r >= 0 && r < rows && c >= 0 && c < cols
                                        && (Trees[r, c] == true))
                                        goto invalidSpot;
                                }
                            }
                            // If reached here no collisions
                            Trees[i, j] = true;
                            continue;

                        invalidSpot:
                            Trees[i, j] = false;
                        }
                    }
                }
            }

            treesValid = true;

            return true;
        }

        public bool FindPlateau(int size, out int xPos, out int yPos, out int zPos, ObjectTuple[] otherObjects)
        {
            int rows = Heights.GetLength(0), cols = Heights.GetLength(1);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    if (i - size >= 0 && i + size < rows && j - size >= 0 && j + size < cols)
                    {
                        Int16 height = Heights[i, j];
                        for (int r = i - size; r < i + size; r++)
                        {
                            for (int c = j - size; c < j + size; c++)
                            {
                                if (Heights[r, c] != height)
                                    goto badPlateau;
                            }
                        }
                        // If reached here, is plateau
                        if (otherObjects != null)
                        {
                            for (int k = 0; k < otherObjects.Length; k++)
                            {
                                if (otherObjects[k].withinDistance(i, j))
                                    goto badPlateau;
                            }
                        }
                        xPos = i;
                        yPos = j;
                        zPos = height;
                        return true;

                    badPlateau:
                        continue;
                    }
                }
            }

            xPos = -1;
            yPos = -1;
            zPos = -1;
            return false;
        }
        public bool FindNearestPlateau(int size, int xStart, int yStart, out int xPos, out int yPos, out int zPos, ObjectTuple[] otherObjects)
        {
            int left = xStart, top = yStart, right = xStart, bottom = yStart;
            xPos = xStart;
            yPos = yStart;
            Direction direction = Direction.YF;
            do
            {
                if (yPos - size >= 0 && yPos + size < rows && xPos - size >= 0 && xPos + size < cols)
                {
                    byte height = Heights[yPos, xPos];
                    for (int r = yPos - size; r < yPos + size; r++)
                    {
                        for (int c = xPos - size; c < xPos + size; c++)
                        {
                            if (Heights[r, c] != height)
                                goto badPlateau;
                        }
                    }
                    if (otherObjects != null)
                    {
                        for (int k = 0; k < otherObjects.Length; k++)
                        {
                            if (otherObjects[k].withinDistance(yPos, xPos))
                                goto badPlateau;
                        }
                    }
                    zPos = height;
                    return true;
                }

                badPlateau:
                switch (direction)
                {
                    case Direction.YF:
                        if (yPos >= top - 1)
                        {
                            top++;
                            if (top > rows - 1)
                                top = rows - 1;
                            direction = Direction.XF;
                        }
                        else
                            yPos++;
                        break;
                    case Direction.XF:
                        if (xPos >= right - 1)
                        {
                            right++;
                            if (right > cols - 1)
                                right = cols - 1;
                            direction = Direction.YB;
                        }
                        else
                            xPos++;
                        break;
                    case Direction.YB:
                        if (yPos <= 0)
                        {
                            bottom--;
                            if (bottom < 0)
                                bottom = 0;
                            direction = Direction.XB;
                        }
                        else
                            yPos--;
                        break;
                    case Direction.XB:
                        if (xPos <= 0)
                        {
                            left--;
                            if (left < 0)
                                left = 0;
                            direction = Direction.YF;
                        }
                        else
                            xPos--;
                        break;
                }
            } while (!(top == rows && right == cols && bottom == 0 && left == 0));

            // Failed to find plateau
            xPos = -1;
            yPos = -1;
            zPos = -1;
            return false;
        }

        public bool placeCrystalAndPlayerSpawn()
        {
            return FindPlateau(4, out playerX, out playerY, out playerZ, null);
        }

        public bool placeMine()
        {
            bool status = FindNearestPlateau(5, playerX, playerY, out mineX, out mineY, out mineZ, new LevelOld.ObjectTuple[] { new LevelOld.ObjectTuple(playerX, playerY, 20) });
            if (status == true)
            {
                int temp;
                for (int i = mineX-4; i < mineX; i++)
                {
                    for (int j = mineY-4; j < mineY+2; j++)
                    {
                        temp = Heights[i, j] + 4;
                        if (temp > byte.MaxValue)
                            Heights[i, j] = byte.MaxValue;
                        else
                            Heights[i, j] = (byte)temp;
                        Terrain[i, j, 1] = Heights[i, j];
                    }
                }

            }
            return status;
        }

        public bool placeEnemySpawn()
        {
            return FindPlateau(2, out enemyX, out enemyY, out enemyZ, new LevelOld.ObjectTuple[] { new LevelOld.ObjectTuple(playerX, playerY, 100), new LevelOld.ObjectTuple(mineX, mineY, 100) });
        }

        private void populateTerrain()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Terrain[i, j, 0] = Lows[i, j];
                    Terrain[i, j, 1] = Heights[i, j];
                    Terrain[i, j, 2] = 0x02;
                    Terrain[i, j, 3] = 0x01;
                }
            }
            terrainValid = true;
        }

        public bool WriteLevel()
        {
            if (terrainValid != true)
                populateTerrain();
#if DEBUG
            MapTerrainWriter.WorkingDirectory = "C:/Program Files (x86)/Steam/steamapps/common/Castle Story/Info/Niveaux/Test Random Generation/";
#endif
            MapTerrainWriter.ClearFolder();
            MapTerrainWriter.WriteMap(Terrain);
            MapTerrainWriter.WriteStartPoint(playerX, playerY, playerZ);
            MapTerrainWriter.WriteObjects(mineX, mineY, mineZ, enemyX, enemyY, enemyZ);
            if (treesValid == true)
                MapTerrainWriter.WriteTrees(Trees, Heights);

            return true;
        }

    }
}
