﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSMGLibrary.UsefulDefines
{
    public class Tuple<T1, T2>
    {
        public T1 Item1;
        public T2 Item2;

        public Tuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }
    }
}
