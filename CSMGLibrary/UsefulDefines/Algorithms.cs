﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSMGLibrary.UsefulDefines
{
    public class Algorithms
    {

        static public double cartesianDistance(double x1, double y1, double x2, double y2)
        {
            // TODO: Time this and compare to Math.Pow()
            double xDist = x2 - x1;
            double yDist = y2 - y1;
            return Math.Sqrt((double)(xDist*xDist) + (double)(yDist*yDist));
        }

        static public double cartesianDistance(double x1, double y1, double z1, double x2, double y2, double z2)
        {
            // TODO: Time this and compare to Math.Pow()
            double xDist = x2 - x1;
            double yDist = y2 - y1;
            double zDist = z2 - z1;
            return Math.Sqrt((double)(xDist * xDist) + (double)(yDist * yDist) + (double)(zDist * zDist));
        }
    }
}
