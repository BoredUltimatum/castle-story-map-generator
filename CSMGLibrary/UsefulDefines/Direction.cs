﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSMGLibrary.UsefulDefines
{
    public enum Direction
    {
        /// <summary>
        /// XBack - back (lower) along x-axis
        /// </summary>
        XB,
        /// <summary>
        /// XForward - forward (greater) along x-axis
        /// </summary>
        XF,
        /// <summary>
        /// YBack - back (lower) along y-axis
        /// </summary>
        YB,
        /// <summary>
        /// YForward - forward (greater) along y-axis
        /// </summary>
        YF,
        /// <summary>
        /// ZBack - back (lower) along z-axis
        /// </summary>
        ZB,
        /// <summary>
        /// ZForward - forward (greater) along z-axis
        /// </summary>
        ZF
    }
}
