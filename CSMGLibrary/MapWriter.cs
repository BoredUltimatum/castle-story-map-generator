﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using CSMGLibrary.TerrainGenerators;
using CSMGLibrary.LevelObjects;

namespace CSMGLibrary
{
    class MapWriter
    {
        public enum Status
        {
            Success,
            InvalidGridDimension,
            Truncated,
            FileFailure
        }

        public const int MAP_ROW_SECTIONS = 8, MAP_COL_SECTIONS = 8;
        public const int MAP_SECTION_ROW_CELLS = 256, MAP_SECTION_COL_CELLS = 256;
        public static String WorkingDirectory = "";

        public static Status WriteTerrain(Terrain terrain)
        {
            Status status = Status.Success;
            byte[, ,] section = new byte[MAP_SECTION_COL_CELLS, MAP_SECTION_ROW_CELLS, 4];

            int rowSections, colSections;
            rowSections = terrain.XLength / MAP_SECTION_ROW_CELLS;
            if (terrain.XLength % MAP_SECTION_ROW_CELLS > 0)
                rowSections++;
            if (rowSections >= MAP_ROW_SECTIONS)
            {
                rowSections = MAP_ROW_SECTIONS;
                status = Status.Truncated;
            }
            colSections = terrain.YLength / MAP_SECTION_COL_CELLS;
            if (terrain.YLength % MAP_SECTION_COL_CELLS > 0)
                colSections++;
            if (colSections >= MAP_COL_SECTIONS)
            {
                colSections = MAP_COL_SECTIONS;
                status = Status.Truncated;
            }


            for (int row = 0; row < rowSections; row++)
            {
                for (int col = 0; col < colSections; col++)
                {
                    for (int i = 0; i < MAP_SECTION_ROW_CELLS; i++)
                    {
                        for (int j = 0; j < MAP_SECTION_COL_CELLS; j++)
                        {
                            if ((i + row * MAP_SECTION_ROW_CELLS) >= terrain.XLength || (j + col * MAP_SECTION_COL_CELLS) >= terrain.YLength)
                            {
                                for (int k = 0; k < 4; k++)
                                    section[j, i, k] = 0x00;
                                continue;
                            }

                            int iS = i + row * MAP_SECTION_ROW_CELLS;
                            int jS = j + col * MAP_SECTION_COL_CELLS;
                            section[j, i, 0] = terrain.Lows[iS, jS];
                            section[j, i, 1] = terrain.Heights[iS, jS];
                            section[j, i, 2] = terrain.Types[iS, jS];
                            section[j, i, 3] = terrain.Slopes[iS, jS];
                        }
                    }

                    String name = "Monde_" + ((col * MAP_COL_SECTIONS) + row);
                    WriteSection(section, name);
                }
            }


            return status;
        }

        private static Status WriteSection(byte[, ,] section, String name)
        {
            Status status = Status.Success;

            FileStream fs;
            byte[] preFill = { 0x00, 0x00 };
            fs = File.Create(WorkingDirectory + name);
            if (!fs.CanWrite)
            {
                fs.Close();
                return Status.FileFailure;
            }

            fs.Write(preFill, 0, preFill.Length);
            for (int i = 0; i < section.GetLength(0); i++)
                for (int j = 0; j < section.GetLength(1); j++)
                    for (int k = 0; k < section.GetLength(2); k++)
                        fs.WriteByte(section[i, j, k]);
            fs.Close();

            return status;
        }

        public static bool WriteDoodads(int startingPointXPos, int startingPointYPos, int startingPointZPos, int crystalXPos, int crystalYPos, int crystalZPos)
        {
            FileStream fs;
            fs = File.Create(WorkingDirectory + "Monde_Doodads");
            if (!fs.CanWrite)
                return false;

            int indexSP, indexC;
            CoordinateConverter.coordsToIndex(startingPointXPos, startingPointYPos, startingPointZPos, out indexSP);
            CoordinateConverter.coordsToIndex(crystalXPos, crystalYPos, crystalZPos, out indexC);

            String str = "StartingPoint " + indexSP + " Crystal " + indexC;
            char[] chars = str.ToCharArray();
            byte[] data = new byte[chars.Length];
            for (int i = 0; i < chars.Length; i++)
                data[i] = (byte)chars[i];
            fs.Write(data, 0, data.Length);
            fs.Close();
            return true;
        }

        public static bool WriteObjects(ICollection<LevelObject> forests, ICollection<LevelObject> mines, ICollection<LevelObject> enemies)
        {
            FileStream fs;
            fs = File.Create(WorkingDirectory + "Monde_Objets.json");
            if (!fs.CanWrite)
                return false;


            int tempIndex, objectNumber = 0;
            bool firstObject;

            String str = "{\n  \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n  \"Version\": 2,\n  \"ObjectDynamiques\": {\n    \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n    \"ForetMagiques\": [";
            
            // Forests
            firstObject = true;
            foreach (LevelObject lo in forests)
            {
                if (firstObject)
                    firstObject = false;
                else
                    str += ",";
                CoordinateConverter.coordsToIndex(lo.XPos, lo.YPos, lo.ZPos, out tempIndex);
                str += "\n      {\n        \"$type\": \"ForetMagique, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": ";
                if (lo.FacingDirection == UsefulDefines.Direction.YB)
                    str += "0";
                else if (lo.FacingDirection == UsefulDefines.Direction.YF)
                    str += "1";
                else if (lo.FacingDirection == UsefulDefines.Direction.XB)
                    str += "2";
                else if (lo.FacingDirection == UsefulDefines.Direction.XF)
                    str += "3";
                else
                    str += "0"; 
                str += ",\n        \"network_id\": " + objectNumber + "\n      }";
                objectNumber++;
            }

            str += "\n    ],\n    \"Palettes\": [],\n    \"Balises\": [";
            
            // Enemies
            firstObject = true;
            foreach (LevelObject lo in enemies)
            {
                if (firstObject)
                    firstObject = false;
                else
                    str += ",";
                CoordinateConverter.coordsToIndex(lo.XPos, lo.YPos, lo.ZPos, out tempIndex);
                str += "\n      {\n        \"$type\": \"BaliseAI, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"rayon\": " + objectNumber + ",\n        \"groupeID\": " + objectNumber + ",\n        \"groupeDesc\": \"Custom\",\n        \"groupeTag\": \"CorruptronSpawnPoint\"\n      }";
                objectNumber++;
            }

            str += "\n    ],\n    \"MineMagiques\": [";
            
            // Mines
            firstObject = true;
            foreach (LevelObject lo in mines)
            {
                if (firstObject)
                    firstObject = false;
                else
                    str += ",";
                CoordinateConverter.coordsToIndex(lo.XPos, lo.YPos, lo.ZPos, out tempIndex);
                str += "\n      {\n        \"$type\": \"MineMagique, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": ";
                if (lo.FacingDirection == UsefulDefines.Direction.YB)
                    str += "0";
                else if (lo.FacingDirection == UsefulDefines.Direction.YF)
                    str += "1";
                else if (lo.FacingDirection == UsefulDefines.Direction.XB)
                    str += "2";
                else if (lo.FacingDirection == UsefulDefines.Direction.XF)
                    str += "3";
                else
                    str += "0";
                str += ",\n        \"network_id\": " + objectNumber + ",\n      }";
                objectNumber++;
            }

            str += "\n    ]\n  }\n}";

            char[] chars = str.ToCharArray();
            byte[] data = new byte[chars.Length];
            for (int i = 0; i < chars.Length; i++)
                data[i] = (byte)chars[i];
            fs.Write(data, 0, data.Length);
            fs.Close();
            return true;
        }

        public static bool WriteTrees(bool[,] trees, byte[,] heights)
        {
            FileStream fs;
            fs = File.Create(WorkingDirectory + "Monde_Arbre");
            if (!fs.CanWrite)
                return false;

            int xBound = Math.Min(trees.GetLength(0), heights.GetLength(0));
            int yBound = Math.Min(trees.GetLength(1), heights.GetLength(1));
            int index;

            for (int i = 0; i < xBound; i++)
            {
                for (int j = 0; j < yBound; j++)
                {
                    if (trees[i, j] == true)
                    {
                        CoordinateConverter.coordsToIndex(i, j, heights[i, j], out index);
                        byte[] bytes = BitConverter.GetBytes(index);
                        fs.Write(bytes, 0, 4);
                    }
                }
            }
            fs.Close();

            return true;
        }

        public static bool ClearFolder()
        {
            for (int i = 0; i < MAP_ROW_SECTIONS * MAP_COL_SECTIONS; i++)
                File.Delete(WorkingDirectory + "Monde_" + i);
            File.Delete("Monde_Doodads");
            File.Delete("Monde_Arbre");
            File.Delete("Monde_Objets.json");

            return true;
        }
    }
}
