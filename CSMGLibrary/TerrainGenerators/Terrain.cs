﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CSMGLibrary.LevelObjects;
using CSMGLibrary.UsefulDefines;

namespace CSMGLibrary.TerrainGenerators
{
    public class Terrain
    {
        #region Properties
        private int xLength;
        private int yLength;
        private byte[,] heights;
        private byte[,] lows;
        private byte[,] types;
        private byte[,] slopes;

        public const byte LOWEST_HEIGHT = 2;
        public int XLength { get { return xLength; } }
        public int YLength { get { return yLength; } }
        public byte[,] Heights { get { return heights; } }
        public byte[,] Lows { get { return lows; } }
        public byte[,] Types { get { return types; } }
        public byte[,] Slopes { get { return slopes; } }
        #endregion

        /// <summary>
        /// Creates a new empty Terrain of the given dimensions
        /// </summary>
        /// <param name="xLength">Lateral length along the x-axis</param>
        /// <param name="yLength">Lateral length along the y-axis</param>
        public Terrain(int xLength, int yLength)
        {
            this.xLength = xLength;
            this.yLength = yLength;
            heights = new byte[xLength, yLength];
            lows = new byte[xLength, yLength];
            types = new byte[xLength, yLength];
            slopes = new byte[xLength, yLength];

            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    heights[i, j] = 0;
                    lows[i, j] = 0;
                    types[i, j] = 0;
                    slopes[i, j] = 0;
                }
            }
        }

        /// <summary>
        /// Deep copy constructor
        /// </summary>
        /// <param name="seed">Terrain to copy</param>
        public Terrain(Terrain seed)
        {
            this.xLength = seed.xLength;
            this.yLength = seed.yLength;
            heights = new byte[xLength, yLength];
            lows = new byte[xLength, yLength];
            types = new byte[xLength, yLength];
            slopes = new byte[xLength, yLength];

            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    heights[i, j] = seed.heights[i, j];
                    lows[i, j] = seed.lows[i, j];
                    types[i, j] = seed.types[i, j];
                    slopes[i, j] = seed.slopes[i, j];
                }
            }
        }

        /// <summary>
        /// Deep copy constructor, makes terrain of provided dimensions prefilling with seed where defined
        /// </summary>
        /// <param name="seed">Terrain to copy</param>
        public Terrain(Terrain seed, int xLength, int yLength)
        {
            this.xLength = xLength;
            this.yLength = yLength;
            heights = new byte[xLength, yLength];
            lows = new byte[xLength, yLength];
            types = new byte[xLength, yLength];
            slopes = new byte[xLength, yLength];

            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    if (i < seed.xLength && j < seed.yLength)
                    {
                        heights[i, j] = seed.heights[i, j];
                        lows[i, j] = seed.lows[i, j];
                        types[i, j] = seed.types[i, j];
                        slopes[i, j] = seed.slopes[i, j];
                    }
                    else
                    {
                        heights[i, j] = 0;
                        lows[i, j] = 0;
                        types[i, j] = 0;
                        slopes[i, j] = 0;
                    }
                }
            }
        }


        /// <summary>
        /// Resizes the terrain truncating overflow or filling new cells with null values.
        /// </summary>
        /// <param name="newXLength">New X-axis length</param>
        /// <param name="newYLength">New Y-axis length</param>
        public void Resize(int newXLength, int newYLength)
        {
            byte[,] newHeights = new byte[newXLength, newYLength];
            byte[,] newLows = new byte[newXLength, newYLength];
            byte[,] newTypes = new byte[newXLength, newYLength];
            byte[,] newSlopes = new byte[newXLength, newYLength];

            for (int i = 0; i < newXLength; i++)
            {
                for (int j = 0; j < newYLength; j++)
                {
                    if (i < xLength && j < yLength)
                    {
                        newHeights[i, j] = heights[i, j];
                        newLows[i, j] = lows[i, j];
                        newTypes[i, j] = types[i, j];
                        newSlopes[i, j] = slopes[i, j];
                    }
                    else
                    {
                        newHeights[i, j] = 0x00;
                        newLows[i, j] = 0x00;
                        newTypes[i, j] = 0x00;
                        newSlopes[i, j] = 0x00;
                    }
                }
            }

            xLength = newXLength;
            yLength = newYLength;
            heights = newHeights;
            lows = newLows;
            types = newTypes;
            slopes = newSlopes;
        }

        /// <summary>
        /// Clears the terrain to null
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    heights[i, j] = 0;
                    lows[i, j] = 0;
                    types[i, j] = 0;
                    slopes[i, j] = 0;
                }
            }
        }

        /// <summary>
        /// Finds a suitable flat area of the given size meeting the defined minimum ranges of the object and objects to avoid and sets the object's position accordingly. Starts from 0,0 moving along the x-axis then y-axis
        /// </summary>
        /// <param name="XBuffer">The amount of cells on each side along the x direction that should be the same height</param>
        /// <param name="YBuffer">The amount of cells on each side along the y direction that should be the same height</param>
        /// <param name="thisObject">The avoidance definition of the object to place</param>
        /// <param name="avoidObjects">Collection of avoidance definitions of objects to avoid. Ignores thisObject if in collection. May be null</param>
        /// <returns>True for success (object's position parameters set) or false for failure (object's position parameters preserved).</returns>
        public bool PlaceLevelObject(int XBuffer, int YBuffer, AvoidanceDefinition thisObject, ICollection<AvoidanceDefinition> avoidObjects)
        {
            LevelObject obj = thisObject.levelObject;
            int xBak = obj.XPos, yBak = obj.YPos, zBak = obj.ZPos;

            for (obj.XPos = 0; obj.XPos < xLength; obj.XPos++)
            {
                for (obj.YPos = 0; obj.YPos < yLength; obj.YPos++)
                {
                    if (obj.XLBound - XBuffer >= 0 && obj.XRBound + XBuffer < xLength && obj.YLBound - YBuffer >= 0 && obj.YRBound + YBuffer < yLength)
                    {
                        byte height = heights[obj.XPos, obj.YPos];
                        if (height == 0)
                            goto badPlateau;
                        for (int i = obj.XLBound - XBuffer; i <= obj.XRBound + XBuffer; i++)
                        {
                            for (int j = obj.YLBound - YBuffer; j <= obj.YRBound + YBuffer; j++)
                            {
                                if (heights[i, j] != height)
                                    goto badPlateau;
                            }
                        }
                        if (avoidObjects != null)
                        {
                            foreach (AvoidanceDefinition avoidObject in avoidObjects)
                            {
                                if (avoidObject != thisObject && avoidObject.withinDistance(obj))
                                    goto badPlateau;
                            }
                        }
                        obj.ZPos = height;
                        return true;

                    badPlateau:
                        continue;
                    }
                }
            }
            // Failed to find plateau
            obj.XPos = xBak;
            obj.YPos = yBak;
            obj.ZPos = zBak;
            return false;
        }

        /// <summary>
        /// Finds the nearest flat area of the given size starting from xStart,yStart in a clockwise pattern, starting in an upward motion
        /// </summary>
        /// <param name="XBuffer">The amount of cells on each side along the x direction that should be the same height</param>
        /// <param name="YBuffer">The amount of cells on each side along the y direction that should be the same height</param>
        /// <param name="xStart">The X coordinate to start from</param>
        /// <param name="yStart">The Y coordinate to start from</param>
        /// <param name="thisObject">The avoidance definition of the object to place</param>
        /// <param name="avoidObjects">Collection of avoidance definitions of objects to avoid. Ignores thisObject if in collection. May be null</param>
        /// <returns>True for success (object's position parameters set) or false for failure (object's position parameters preserved).</returns>
        public bool PlaceLevelObjectNearestPoint(int XBuffer, int YBuffer, int xStart, int yStart, AvoidanceDefinition thisObject, ICollection<AvoidanceDefinition> avoidObjects)
        {
            /*
             * Start at coordinates
             *   while !(top == depth && right == width && bottom == 0 && left == 0)
             *     Move up until top reached
             *       top++
             *       if top >= depth
             *         top = depth
             *     Move right until right reached
             *       right++
             *       if right >= width
             *         right = width
             *     Move down until bottom reached
             *       bottom--
             *       if bottom < 0
             *         bottom = 0
             *     Move left until left reached
             *       left--
             *       if left < 0
             *         left = 0
             * 
             * May be optimized later with finished left/right/top/bottom flags indicating when that
             *  bound has been reached (I.E. left < 0 or right >= width) to prevent rechecking cells
             *  along that bound
             * 
             */
            LevelObject obj = thisObject.levelObject;
            int xBak = obj.XPos, yBak = obj.YPos, zBak = obj.ZPos;
            int left = xStart, top = yStart, right = xStart, bottom = yStart;
            obj.XPos = xStart;
            obj.YPos = yStart;
            Direction direction = Direction.YF;
            do
            {
                if (obj.YLBound - YBuffer >= 0 && obj.YRBound + YBuffer < yLength && obj.XLBound - XBuffer >= 0 && obj.XRBound + XBuffer < xLength)
                {
                    byte height = heights[obj.XPos, obj.YPos];
                    if (height == 0)
                        goto badPlateau;
                    for (int i = obj.XLBound - XBuffer; i <= obj.XRBound + XBuffer; i++)
                    {
                        for (int j = obj.YLBound - YBuffer; j <= obj.YRBound + YBuffer; j++)
                        {
                            if (heights[i, j] != height)
                                goto badPlateau;
                        }
                    }
                    if (avoidObjects != null)
                    {
                        foreach (AvoidanceDefinition avoidObject in avoidObjects)
                        {
                            if (avoidObject == thisObject || avoidObject.withinDistance(obj) || thisObject.withinDistance(avoidObject.levelObject))
                                goto badPlateau;
                        }
                    }
                    obj.ZPos = height;
                    return true;
                }

                badPlateau:
                switch (direction)
                {
                    case Direction.YF:
                        if (obj.YPos >= top)
                        {
                            top++;
                            if (top > yLength - 1)
                                top = yLength - 1;
                            direction = Direction.XF;
                        }
                        else
                            obj.YPos++;
                        break;
                    case Direction.XF:
                        if (obj.XPos >= right)
                        {
                            right++;
                            if (right > xLength - 1)
                                right = xLength - 1;
                            direction = Direction.YB;
                        }
                        else
                            obj.XPos++;
                        break;
                    case Direction.YB:
                        if (obj.YPos <= bottom)
                        {
                            bottom--;
                            if (bottom < 0)
                                bottom = 0;
                            direction = Direction.XB;
                        }
                        else
                            obj.YPos--;
                        break;
                    case Direction.XB:
                        if (obj.XPos <= left)
                        {
                            left--;
                            if (left < 0)
                                left = 0;
                            direction = Direction.YF;
                        }
                        else
                            obj.XPos--;
                        break;
                }
            } while (!(top == yLength-1 && right == xLength-1 && bottom == 0 && left == 0));

            // Failed to find plateau
            obj.XPos = xBak;
            obj.YPos = yBak;
            obj.ZPos = zBak;
            return false;
        }

        /// <summary>
        /// Checks if cell at x,y is a non null cell bordering a null cell (horizontal, vertical, or diagonal)
        /// </summary>
        /// <param name="x">X position of cell</param>
        /// <param name="y">Y position of cell</param>
        /// <returns>If is boundry cell</returns>
        public bool IsBoundryCell(int x, int y)
        {
            if (x < 0 || x >= xLength || y < 0 || y >= yLength || heights[x, y] == 0)
                return false;

            if (x-1 >= 0 && x-1 < xLength && y-1 >= 0 && y-1 < yLength && Heights[x-1, y-1] == 0)
                return true;
            if (x-1 >= 0 && x-1 < xLength && Heights[x-1, y] == 0)
                return true;
            if (x-1 >= 0 && x-1 < xLength && y+1 >= 0 && y+1 < yLength && Heights[x-1, y+1] == 0)
                return true;
            if (y-1 >= 0 && y-1 < yLength && Heights[x, y-1] == 0)
                return true;
            if (y+1 >= 0 && y+1 < yLength && Heights[x, y+1] == 0)
                return true;
            if (x+1 >= 0 && x+1 < xLength && y-1 >= 0 && y-1 < yLength && Heights[x+1, y-1] == 0)
                return true;
            if (x+1 >= 0 && x+1 < xLength && Heights[x+1, y] == 0)
                return true;
            if (x+1 >= 0 && x+1 < xLength && y+1 >= 0 && y+1 < yLength && Heights[x+1, y+1] == 0)
                return true;

            return false;
        }

        /// <summary>
        /// Finds the largest island laterally in the x/y plane and removes all others
        /// </summary>
        /// <returns>Whether any islands were removed</returns>
        public bool removeAllButLargestIsland()
        {
            bool status = false;

            List<int> islandSizes = new List<int>();
            int[,] idGrid = new int[xLength, yLength]; // A particularly 'busy' map could have over 256 islands so int was chosen as this method shouldn't be repeatedly called
            bool[,] visitedGrid = new bool[xLength, yLength];
            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    idGrid[i, j] = -1;
                    visitedGrid[i, j] = false;
                }
            }
            int nextID = 0;

            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    if (idGrid[i, j] == -1 && heights[i, j] != 0)
                    {
                        idGrid[i, j] = nextID;
                        islandSizes.Add(0);
                        islandSizes[nextID++] = removeAllButLargestIslandHelper(i, j, ref idGrid, ref visitedGrid);
                    }
                }
            }

            int largestID = -1;
            int largestSize = -1;
            for (int i = 0; i < islandSizes.Count; i++)
            {
                if (islandSizes[i] > largestSize)
                {
                    largestSize = islandSizes[i];
                    largestID = i;
                }
            }

            for (int i = 0; i < xLength; i++)
            {
                for (int j = 0; j < yLength; j++)
                {
                    if (heights[i, j] != 0 && idGrid[i, j] != largestID)
                    {
                        status = true;
                        heights[i, j] = 0;
                        lows[i, j] = 0;
                        types[i, j] = 0;
                        slopes[i, j] = 0;
                    }
                }
            }

            return status;
        }
        private int removeAllButLargestIslandHelper(int x, int y, ref int[,] idGrid, ref bool[,] visitedGrid)
        {
            // Propogates the value at idGrid[x, y] to all connected non null heights in idGrid then returns amount of changed cells

            int count = 0;

            if (visitedGrid[x, y] == true)
                return count;

            int curX, curY;
            LinkedList<Tuple<int, int>> queue = new LinkedList<Tuple<int,int>>();
            LinkedList<Tuple<int, int>> tuplePool = new LinkedList<Tuple<int,int>>();

            queue.AddLast(new Tuple<int, int>(x, y));

            while (queue.Count != 0)
            {
                Tuple<int, int> tuple = queue.First.Value;
                queue.RemoveFirst();
                curX = tuple.Item1;
                curY = tuple.Item2;
                tuplePool.AddLast(tuple);

                if (visitedGrid[curX, curY] == true)
                    continue;

                int xM = curX-1;
                int xP = curX+1;
                int yM = curY-1;
                int yP = curY+1;
                bool xL = (xM >= 0) ? true : false;
                bool xU = (xP < xLength) ? true : false;
                bool yL = (yM >= 0) ? true : false;
                bool yU = (yP < yLength) ? true : false;
                Tuple<int, int> nextCell;

                // Top Left
                if (xL && yL && heights[xM, yM] != 0 && idGrid[xM, yM] == -1)
                {
                    idGrid[xM, yM] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = xM;
                        nextCell.Item2 = yM;
                        tuplePool.RemoveFirst();
                    } else
                        nextCell = new Tuple<int,int>(xM, yM);
                    queue.AddLast(nextCell);
                }
                // Top
                if (yL && heights[curX, yM] != 0 && idGrid[curX, yM] == -1)
                {
                    idGrid[curX, yM] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = curX;
                        nextCell.Item2 = yM;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(curX, yM);
                    queue.AddLast(nextCell);
                }
                // Top Right
                if (xU && yL && heights[xP, yM] != 0 && idGrid[xP, yM] == -1)
                {
                    idGrid[xP, yM] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = xP;
                        nextCell.Item2 = yM;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(xP, yM);
                    queue.AddLast(nextCell);
                }
                // Left
                if (xL && heights[xM, curY] != 0 && idGrid[xM, curY] == -1)
                {
                    idGrid[xM, curY] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = xM;
                        nextCell.Item2 = curY;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(xM, curY);
                    queue.AddLast(nextCell);
                }
                // Right
                if (xU && heights[xP, curY] != 0 && idGrid[xP, curY] == -1)
                {
                    idGrid[xP, curY] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = xP;
                        nextCell.Item2 = curY;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(xP, curY);
                    queue.AddLast(nextCell);
                }
                // Bottom Left
                if (xL && yU && heights[xM, yP] != 0 && idGrid[xM, yP] == -1)
                {
                    idGrid[xM, yP] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = xM;
                        nextCell.Item2 = yP;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(xM, yP);
                    queue.AddLast(nextCell);
                }
                // Bottom
                if (yU && heights[curX, yP] != 0 && idGrid[curX, yP] == -1)
                {
                    idGrid[curX, yP] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = curX;
                        nextCell.Item2 = yP;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(curX, yP);
                    queue.AddLast(nextCell);
                }
                // Bottom Right
                if (xU && yU && heights[xP, yP] != 0 && idGrid[xP, yP] == -1)
                {
                    idGrid[xP, yP] = idGrid[curX, curY];
                    if (tuplePool.Count > 0)
                    {
                        nextCell = tuplePool.First.Value;
                        nextCell.Item1 = xP;
                        nextCell.Item2 = yP;
                        tuplePool.RemoveFirst();
                    }
                    else
                        nextCell = new Tuple<int, int>(xP, yP);
                    queue.AddLast(nextCell);
                }

                visitedGrid[curX, curY] = true;
                count++;
            }

            return count;
        }


        // Find/place mine in cliff/wall?

        // Make wall and place cliff?

        // Connect islands?
    }
}
