﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NoiseGeneratorNS;

namespace CSMGLibrary.TerrainGenerators
{
    public class NoiseGrid : ITerrainGenerator
    {
        public const int DEFAULT_OCTAVES = 1;
        public const double DEFAULT_AMPLITUDE = 10;
        public const double DEFAULT_FREQUENCY = 0.04;
        public const double DEFAULT_PERSISTENCE = 0.15;
        public const byte DEFAULT_UPPERBOUND = 0x4c;
        public const byte DEFAULT_LOWERBOUND = 0x3f;
        public const byte DEFAULT_VARIATION = 0x04;
        public const byte DEFAULT_THICKNESS = 0x03;
        public const byte DEFAULT_HIGHPASS = 0x48;
        public const bool DEFAULT_HIGHPASSACTIVE = false;
        public const byte DEFAULT_LOWPASS = 0x49;
        public const bool DEFAULT_LOWPASSACTIVE = true;
        public int octaves;
        public double amplitude;
        public double frequency;
        public double persistence;
        /// <summary>
        /// The height the highest point/level should be
        /// </summary>
        public byte upperBound;
        /// <summary>
        /// The height the lowest point/level should be
        /// </summary>
        public byte lowerBound;
        /// <summary>
        /// The minimum amount of cells below each surface cell
        /// </summary>
        public byte thickness;
        /// <summary>
        /// The distance between the highest and lowest height points... I think; It's 0230 and I should have documented when originally writing
        /// </summary>
        public byte variation;
        /// <summary>
        /// Nulls cells with surface lower than this
        /// </summary>
        public byte lowPassFilter;
        /// <summary>
        /// Whether to apply the low pass filter
        /// </summary>
        public bool lowPassFilterActive;
        /// <summary>
        /// Nulls cells with surface higher than this
        /// </summary>
        public byte highPassFilter;
        /// <summary>
        /// Whether to apply the high pass filter
        /// </summary>
        public bool highPassFilterActive;
        private NoiseGenerator noiseGenerator;

        public NoiseGrid()
        {
            this.upperBound = DEFAULT_UPPERBOUND;
            this.lowerBound = DEFAULT_LOWERBOUND;
            this.thickness = DEFAULT_THICKNESS;
            this.variation = DEFAULT_VARIATION;
            this.octaves = DEFAULT_OCTAVES;
            this.amplitude = DEFAULT_AMPLITUDE;
            this.frequency = DEFAULT_FREQUENCY;
            this.persistence = DEFAULT_PERSISTENCE;
            this.lowPassFilter = 0;
            this.lowPassFilterActive = false;
            this.highPassFilter = 0;
            this.highPassFilterActive = false;
            noiseGenerator = new NoiseGenerator(octaves, amplitude, frequency, persistence);
        }

        public NoiseGrid(byte upperBound, byte thickness, byte variation)
        {
            this.upperBound = upperBound;
            this.lowerBound = byte.MaxValue;
            this.thickness = thickness;
            this.variation = variation;
            this.octaves = DEFAULT_OCTAVES;
            this.amplitude = DEFAULT_AMPLITUDE;
            this.frequency = DEFAULT_FREQUENCY;
            this.persistence = DEFAULT_PERSISTENCE;
            this.lowPassFilter = 0;
            this.lowPassFilterActive = false;
            this.highPassFilter = 0;
            this.highPassFilterActive = false;
            noiseGenerator = new NoiseGenerator(octaves, amplitude, frequency, persistence);
        }

        public NoiseGrid(byte upperBound, byte lowerBound, byte thickness, byte variation)
        {
            this.upperBound = upperBound;
            this.lowerBound = lowerBound;
            this.thickness = thickness;
            this.variation = variation;
            this.octaves = DEFAULT_OCTAVES;
            this.amplitude = DEFAULT_AMPLITUDE;
            this.frequency = DEFAULT_FREQUENCY;
            this.persistence = DEFAULT_PERSISTENCE;
            this.lowPassFilter = 0;
            this.lowPassFilterActive = false;
            this.highPassFilter = 0;
            this.highPassFilterActive = false;
            noiseGenerator = new NoiseGenerator(octaves, amplitude, frequency, persistence);
        }

        public NoiseGrid(byte upperBound, byte lowerBound, byte thickness, byte variation, int octaves, double amplitude, double frequency, double persistence)
        {
            this.upperBound = upperBound;
            this.lowerBound = lowerBound;
            this.thickness = thickness;
            this.variation = variation;
            this.octaves = octaves;
            this.amplitude = amplitude;
            this.frequency = frequency;
            this.persistence = persistence;
            this.lowPassFilter = 0;
            this.lowPassFilterActive = false;
            this.highPassFilter = 0;
            this.highPassFilterActive = false;
            noiseGenerator = new NoiseGenerator(octaves, amplitude, frequency, persistence);
        }

        public void setParameters(byte upperBound, byte lowerBound, byte thickness, byte variation, int octaves, double amplitude, double frequency, double persistence)
        {
            this.upperBound = upperBound;
            this.lowerBound = lowerBound;
            this.thickness = thickness;
            this.variation = variation;
            this.octaves = octaves;
            this.amplitude = amplitude;
            this.frequency = frequency;
            this.persistence = persistence;
        }

        public void setParameters(byte upperBound, byte lowerBound, byte thickness, byte variation, int octaves, double amplitude, double frequency, double persistence, byte lowPassFilter, byte highPassFilter)
        {
            this.upperBound = upperBound;
            this.lowerBound = lowerBound;
            this.thickness = thickness;
            this.variation = variation;
            this.octaves = octaves;
            this.amplitude = amplitude;
            this.frequency = frequency;
            this.persistence = persistence;
            this.lowPassFilter = lowPassFilter;
            this.lowPassFilterActive = true;
            this.highPassFilter = highPassFilter;
            this.highPassFilterActive = true;
        }

        public void GenerateTerrain(Terrain terrain)
        {
            if (upperBound < Terrain.LOWEST_HEIGHT)
                upperBound = Terrain.LOWEST_HEIGHT;
            if (lowerBound >= upperBound)
                lowerBound = (byte)(upperBound - 2);
            byte midpoint = (byte)(lowerBound + ((upperBound - lowerBound) / 2));

            noiseGenerator.generateNoise(octaves, amplitude, frequency, persistence);

            // Heights
            for (int i = 0; i < terrain.XLength; i++)
            {
                for (int j = 0; j < terrain.YLength; j++)
                {
                    int height = midpoint;
                    float offset = (float)noiseGenerator.Noise(i, j);
                    offset *= (float)variation;
                    height += (int)offset;
                    terrain.Heights[i, j] = (byte)((height < 0) ? 0 : (height < Byte.MaxValue) ? height : Byte.MaxValue);

                    // Types, Slopes
                    terrain.Types[i, j] = 0x02;
                    terrain.Slopes[i, j] = 0x01;
                }
            }
            if (lowPassFilterActive)
                ApplyLowPass(terrain);
            if (highPassFilterActive)
                ApplyHighPass(terrain);

            // Lows
            for (int i = 0; i < terrain.XLength; i++)
            {
                for (int j = 0; j < terrain.YLength; j++)
                {
                    if (terrain.Heights[i, j] == 0 || terrain.Heights[i, j] == 1)
                    {
                        terrain.Lows[i, j] = 0;
                        continue;
                    }

                    byte lowest = byte.MaxValue;

                    for (int r = i - 2; r < i + 2; r++)
                    {
                        for (int c = j - 2; c < j + 2; c++)
                        {
                            if (r >= 0 && r < terrain.XLength && c >= 0 && c < terrain.YLength
                                && terrain.Heights[r, c] != 0 && terrain.Heights[r, c] < lowest)
                                lowest = terrain.Heights[r, c];
                        }
                    }

                    lowest -= thickness;
                    if (lowest < 0)
                        lowest = 0;
                    else if (lowest > lowerBound)
                        lowest = lowerBound;
                    if (lowest >= terrain.Heights[i, j])
                        lowest = (byte)(terrain.Heights[i, j] - 0x01);

                    terrain.Lows[i, j] = lowest;
                }
            }
        }

        private void ApplyLowPass(Terrain terrain)
        {
            for (int i = 0; i < terrain.XLength; i++)
            {
                for (int j = 0; j < terrain.YLength; j++)
                {
                    if (terrain.Heights[i, j] < lowPassFilter)
                    {
                        terrain.Heights[i, j] = 0;
                        terrain.Types[i, j] = 0;
                        terrain.Slopes[i, j] = 0;
                    }
                }
            }
        }

        private void ApplyHighPass(Terrain terrain)
        {
            for (int i = 0; i < terrain.XLength; i++)
            {
                for (int j = 0; j < terrain.YLength; j++)
                {
                    if (terrain.Heights[i, j] > highPassFilter)
                    {
                        terrain.Heights[i, j] = 0;
                        terrain.Types[i, j] = 0;
                        terrain.Slopes[i, j] = 0;
                    }
                }
            }
        }

        // This should actually be in Terrain class
        /*private void ConnectIslands(Terrain terrain)
        {
            byte[,] temp = new byte[terrain.XLength, terrain.YLength];
            for (int i = 0; i < terrain.XLength; i++)
                for (int j = 0; j < terrain.YLength; j++)
                    temp[i, j] = 0;
            byte boundIndex = 1;

            for (int i = 0; i < terrain.XLength; i++)
            {
                for (int j = 0; j < terrain.YLength; j++)
                {
                    if (temp[i, j] != 0 || terrain.Heights[i, j] == 0)
                        continue;

                    if (terrain.IsBoundryCell(i, j))
                    {
                        temp[i, j] = boundIndex;

                    }
                }
            }
        }

        private void ConnectIslandFollowBound(Terrain terrain, int i, int j, int prevI, int prevJ, byte[,] boundIndexes)
        {
            
        }*/
    }
}
