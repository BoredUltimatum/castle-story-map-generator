﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSMGLibrary
{
    public static class CoordinateConverter
    {
        public static void indexToCoords(int index, out int x, out int y, out int z)
        {
            const int a = 1024 * 1024, b = 1024;
            z = index / a;
            index -= z * a;
            y = index / b;
            index -= y * b;
            x = index;

            z += 1;
        }

        public static void coordsToIndex(int x, int y, int z, out int index)
        {
            z -= 1;

            index = x + (y * 1024) + (z * 1024 * 1024);
        }
    }
}
