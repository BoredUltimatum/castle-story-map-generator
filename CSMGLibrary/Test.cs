﻿/*
 * Copyright 2013 BoredUltimatum
 * theboredultimatum@gmail.com
 * 
 * This software is released under the GNU General Public License v3
 * Available online at http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Generalized GPL v3 breakdown:
 * Requirements
 * - License and copyright notice
 * - State Changes
 * - Disclose Source
 * Permitted
 * - Commercial Use
 * - Modification
 * - Distribution
 * - Patent Grant
 * - Private Use
 * Forbidden
 * - Hold Liable
 * - Sublicensing
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using NoiseGeneratorNS;

namespace CSMGLibrary
{
    public static class Test
    {
        public static void RunTest()
        {
            //RandomMap(128, 128, 1, 10, 0.085, 0.15);
            LevelOld level = new LevelOld(128, 128, 1, 10, 0.085, 0.15);
            do
            {
                level.GenerateTerrain(0x4c, 0x04, 0x03);
            } while (!level.placeCrystalAndPlayerSpawn() || !level.placeMine() || !level.placeEnemySpawn());
            level.FillTreeGrid();
            level.WriteLevel();
        }

        private class ObjectTuple
        {
            public int distance, xPos, yPos;
            public ObjectTuple(int d, int x, int y) {distance = d; xPos = x; yPos = y;}
        }


        private static void RandomMap(int rows, int cols, int octaves, int amplitude, double frequency, double persistence)
        {
            NoiseGenerator.GENERATOR.generateNoise(octaves, amplitude, frequency, persistence);
            byte[,] heights = new byte[rows, cols];
            byte[,] lows = new byte[rows, cols];
            byte[,,] grid = new byte[rows, cols, 4];
            bool[,] trees = new bool[rows, cols];

            byte upperBound = 0x4c, thickness = 0x03, variation = 0x04;
            int spawnX, spawnY, spawnZ, mineX, mineY, mineZ, enemyX, enemyY, enemyZ;

            do
            {
                GenerateGrid(upperBound, variation, ref heights);
            } while (!FindPlateau(heights, 4, out spawnX, out spawnY, out spawnZ, null) || !FindPlateau(heights, 5, out mineX, out mineY, out mineZ, new ObjectTuple[] { new ObjectTuple(5, spawnX, spawnY) }) || !FindPlateau(heights, 2, out enemyX, out enemyY, out enemyZ, new ObjectTuple[] { new ObjectTuple(100, spawnX, spawnY), new ObjectTuple(100, mineX, mineY) }));

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    byte lowest = byte.MaxValue;

                    for (int r = i - 2; r < i + 2; r++)
                    {
                        for (int c = j - 2; c < j + 2; c++)
                        {
                            if (r >= 0 && r < rows && c >= 0 && c < cols
                                && heights[r, c] < lowest)
                                lowest = heights[r, c];
                        }
                    }

                    lowest -= thickness;
                    if (lowest < 0)
                        lowest = 0;

                    lows[i, j] = lowest;
                }
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    grid[i, j, 0] = lows[i, j];
                    grid[i, j, 1] = heights[i, j];
                    grid[i, j, 2] = 0x02;
                    grid[i, j, 3] = 0x01;
                }
            }

            FillTreeGrid(ref trees, new ObjectTuple[] { new ObjectTuple(10, spawnX, spawnY), new ObjectTuple(25, mineX, mineY), new ObjectTuple(25, enemyX, enemyY) });

#if DEBUG
            MapTerrainWriter.WorkingDirectory = "C:/Program Files (x86)/Steam/steamapps/common/Castle Story/Info/Niveaux/Test Random Generation/";
#endif
            MapTerrainWriter.WriteMap(grid);
            MapTerrainWriter.WriteStartPoint(spawnX, spawnY, spawnZ);
            MapTerrainWriter.WriteObjects(mineX, mineY, mineZ, enemyX, enemyY, enemyZ);
            MapTerrainWriter.WriteTrees(trees, heights);
        }

        private static void GenerateGrid(byte upperBound, byte variation, ref byte[,] heights)
        {
            for (int i = 0; i < heights.GetLength(0); i++)
            {
                for (int j = 0; j < heights.GetLength(1); j++)
                {
                    float offset = (float)NoiseGenerator.GENERATOR.Noise(i, j);
                    offset *= (float)variation;
                    int offsetInt = (int)offset;
                    byte offsetByte = (byte)offsetInt;
                    heights[i, j] = (byte)(upperBound + offsetByte);
                }
            }
        }

        private static bool FindPlateau(byte[,] heights, int size, out int xPos, out int yPos, out int zPos, ObjectTuple[] otherObjects)
        {
            int rows = heights.GetLength(0), cols = heights.GetLength(1);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    if (i-size >= 0 && i+size < rows && j-size >= 0 && j+size < cols)
                    {
                        Int16 height = heights[i, j];
                        for (int r = i-size; r < i+size; r++)
                        {
                            for (int c = j-size; c < j+size; c++)
                            {
                                if (heights[r, c] != height)
                                    goto badPlateau;
                            }
                        }
                        // If reached here, is plateau
                        if (otherObjects != null)
                        {
                            for (int k = 0; k < otherObjects.Length; k++)
                            {
                                if (Math.Abs((i - otherObjects[k].xPos) - (j - otherObjects[k].yPos)) < otherObjects[k].distance)
                                    goto badPlateau;
                            }
                        }
                        xPos = i;
                        yPos = j;
                        zPos = height;
                        return true;

                        badPlateau:
                        continue;
                    }
                }
            }

            xPos = -1;
            yPos = -1;
            zPos = -1;
            return false;
        }

        private static void FillTreeGrid(ref bool[,] trees, ObjectTuple[] otherObjects)
        {
            int octaves = NoiseGenerator.GENERATOR.Octaves;
            double amplitude = NoiseGenerator.GENERATOR.Amplitude;
            double frequency = NoiseGenerator.GENERATOR.Frequency;
            double persistence = NoiseGenerator.GENERATOR.Persistence;


            NoiseGenerator.GENERATOR.Octaves = 2;
            NoiseGenerator.GENERATOR.Amplitude = 10.0;
            NoiseGenerator.GENERATOR.Frequency = 0.085;
            NoiseGenerator.GENERATOR.Persistence = 0.3;

            const int sparsity = 3;
            int rows = trees.GetLength(0), cols = trees.GetLength(1);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    double noise = NoiseGenerator.GENERATOR.Noise(i, j);
                    if (noise != 1.0)
                        trees[i, j] = false;
                    else
                    {
                        if (otherObjects == null)
                            trees[i, j] = true;
                        else
                        {
                            // Check against otherObjects proximities
                            for (int k = 0; k < otherObjects.Length; k++)
                            {
                                if (Math.Abs((i - otherObjects[k].xPos) - (j - otherObjects[k].yPos)) < otherObjects[k].distance)
                                    goto invalidSpot;
                            }
                            // Check against other trees
                            for (int r = i - sparsity; r < i + sparsity; r++)
                            {
                                for (int c = j - sparsity; c < j + sparsity; c++)
                                {
                                    if (r >= 0 && r < rows && c >= 0 && c < cols
                                        && (trees[r, c] == true))
                                        goto invalidSpot;
                                }
                            }
                            // If reached here no collisions
                            trees[i, j] = true;
                            continue;

                            invalidSpot:
                            trees[i, j] = false;
                        }
                    }
                }
            }


            NoiseGenerator.GENERATOR.setParameters(octaves, amplitude, frequency, persistence);
        }
    }
}
