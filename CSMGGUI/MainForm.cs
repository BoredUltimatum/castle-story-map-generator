﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CSMGLibrary;

namespace CSMGGUI
{
    public partial class MainForm : Form
    {
        LevelOld level;

        public MainForm()
        {
            InitializeComponent();
            level = new LevelOld(128, 128, 1, 10, 0.085, 0.15);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }


        private void GenAll_Click(object sender, EventArgs e)
        {
            do
            {
                level.GenerateTerrain(0x4c, 0x04, 0x03);
            } while (!level.placeCrystalAndPlayerSpawn() || !level.placeMine() || !level.placeEnemySpawn());
            level.FillTreeGrid();
            level.WriteLevel();
        }

        private void GenTerrain_Click(object sender, EventArgs e)
        {
            level.GenerateTerrain(0x4c, 0x04, 0x03);
        }

        private void GenObjects_Click(object sender, EventArgs e)
        {
            level.placeCrystalAndPlayerSpawn();
            level.placeMine();
            level.placeEnemySpawn();
        }

        private void GenTrees_Click(object sender, EventArgs e)
        {
            level.FillTreeGrid();
        }

        private void Write_Click(object sender, EventArgs e)
        {
            level.WriteLevel();
        }
    }
}
