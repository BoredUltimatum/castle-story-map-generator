﻿namespace CSMGGUI
{
    partial class Generator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GenAll = new System.Windows.Forms.Button();
            this.Step1 = new System.Windows.Forms.Button();
            this.Step2 = new System.Windows.Forms.Button();
            this.Step3 = new System.Windows.Forms.Button();
            this.Write = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.Step1CheckBox = new System.Windows.Forms.CheckBox();
            this.Step2CheckBox = new System.Windows.Forms.CheckBox();
            this.Step3CheckBox = new System.Windows.Forms.CheckBox();
            this.WriteCheckBox = new System.Windows.Forms.CheckBox();
            this.TerrainDefault = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.objectsShowCheckBox = new System.Windows.Forms.CheckBox();
            this.treesShowCheckBox = new System.Windows.Forms.CheckBox();
            this.RowsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ColsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.TerrainPersistenceUpDown = new System.Windows.Forms.NumericUpDown();
            this.TerrainFrequencyUpDown = new System.Windows.Forms.NumericUpDown();
            this.TerrainAmplitudeUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.TerrainOctavesUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TreePersistenceUpDown = new System.Windows.Forms.NumericUpDown();
            this.TreeFrequencyUpDown = new System.Windows.Forms.NumericUpDown();
            this.TreeAmplitudeUpDown = new System.Windows.Forms.NumericUpDown();
            this.TreeOctavesUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TreeDefault = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.LowerBoundCheckBox = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TerrainUpperBoundUpDown = new System.Windows.Forms.NumericUpDown();
            this.TerrainLowerBoundUpDown = new System.Windows.Forms.NumericUpDown();
            this.TerrainVariationUpDown = new System.Windows.Forms.NumericUpDown();
            this.TerrainThicknessUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RowsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColsNumericUpDown)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainPersistenceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainFrequencyUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainAmplitudeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainOctavesUpDown)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePersistenceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeFrequencyUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeAmplitudeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeOctavesUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainUpperBoundUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainLowerBoundUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainVariationUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainThicknessUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // GenAll
            // 
            this.GenAll.Location = new System.Drawing.Point(12, 12);
            this.GenAll.Name = "GenAll";
            this.GenAll.Size = new System.Drawing.Size(125, 23);
            this.GenAll.TabIndex = 1;
            this.GenAll.Text = "Generate All and Write";
            this.GenAll.UseVisualStyleBackColor = true;
            this.GenAll.Click += new System.EventHandler(this.GenAll_Click);
            // 
            // Step1
            // 
            this.Step1.Location = new System.Drawing.Point(12, 41);
            this.Step1.Name = "Step1";
            this.Step1.Size = new System.Drawing.Size(125, 23);
            this.Step1.TabIndex = 2;
            this.Step1.Text = "Step 1: Terrain";
            this.Step1.UseVisualStyleBackColor = true;
            this.Step1.Click += new System.EventHandler(this.Step1_Click);
            // 
            // Step2
            // 
            this.Step2.Enabled = false;
            this.Step2.Location = new System.Drawing.Point(12, 70);
            this.Step2.Name = "Step2";
            this.Step2.Size = new System.Drawing.Size(125, 23);
            this.Step2.TabIndex = 3;
            this.Step2.Text = "Step 2: Objects";
            this.Step2.UseVisualStyleBackColor = true;
            this.Step2.Click += new System.EventHandler(this.Step2_Click);
            // 
            // Step3
            // 
            this.Step3.Enabled = false;
            this.Step3.Location = new System.Drawing.Point(12, 99);
            this.Step3.Name = "Step3";
            this.Step3.Size = new System.Drawing.Size(125, 23);
            this.Step3.TabIndex = 4;
            this.Step3.Text = "Step 3: Trees";
            this.Step3.UseVisualStyleBackColor = true;
            this.Step3.Click += new System.EventHandler(this.Step3_Click);
            // 
            // Write
            // 
            this.Write.Enabled = false;
            this.Write.Location = new System.Drawing.Point(12, 128);
            this.Write.Name = "Write";
            this.Write.Size = new System.Drawing.Size(125, 23);
            this.Write.TabIndex = 5;
            this.Write.Text = "Write Level";
            this.Write.UseVisualStyleBackColor = true;
            this.Write.Click += new System.EventHandler(this.Write_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 26);
            this.label9.TabIndex = 8;
            this.label9.Text = "Terrain\nParameters";
            // 
            // Step1CheckBox
            // 
            this.Step1CheckBox.AutoSize = true;
            this.Step1CheckBox.Enabled = false;
            this.Step1CheckBox.Location = new System.Drawing.Point(144, 46);
            this.Step1CheckBox.Name = "Step1CheckBox";
            this.Step1CheckBox.Size = new System.Drawing.Size(15, 14);
            this.Step1CheckBox.TabIndex = 10;
            this.Step1CheckBox.UseVisualStyleBackColor = true;
            // 
            // Step2CheckBox
            // 
            this.Step2CheckBox.AutoSize = true;
            this.Step2CheckBox.Enabled = false;
            this.Step2CheckBox.Location = new System.Drawing.Point(144, 79);
            this.Step2CheckBox.Name = "Step2CheckBox";
            this.Step2CheckBox.Size = new System.Drawing.Size(15, 14);
            this.Step2CheckBox.TabIndex = 11;
            this.Step2CheckBox.UseVisualStyleBackColor = true;
            // 
            // Step3CheckBox
            // 
            this.Step3CheckBox.AutoSize = true;
            this.Step3CheckBox.Enabled = false;
            this.Step3CheckBox.Location = new System.Drawing.Point(144, 108);
            this.Step3CheckBox.Name = "Step3CheckBox";
            this.Step3CheckBox.Size = new System.Drawing.Size(15, 14);
            this.Step3CheckBox.TabIndex = 12;
            this.Step3CheckBox.UseVisualStyleBackColor = true;
            // 
            // WriteCheckBox
            // 
            this.WriteCheckBox.AutoSize = true;
            this.WriteCheckBox.Enabled = false;
            this.WriteCheckBox.Location = new System.Drawing.Point(144, 137);
            this.WriteCheckBox.Name = "WriteCheckBox";
            this.WriteCheckBox.Size = new System.Drawing.Size(15, 14);
            this.WriteCheckBox.TabIndex = 13;
            this.WriteCheckBox.UseVisualStyleBackColor = true;
            // 
            // TerrainDefault
            // 
            this.TerrainDefault.Location = new System.Drawing.Point(3, 37);
            this.TerrainDefault.Name = "TerrainDefault";
            this.TerrainDefault.Size = new System.Drawing.Size(75, 23);
            this.TerrainDefault.TabIndex = 14;
            this.TerrainDefault.Text = "Default";
            this.TerrainDefault.UseVisualStyleBackColor = true;
            this.TerrainDefault.Click += new System.EventHandler(this.TerrainDefault_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 310);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(761, 411);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // objectsShowCheckBox
            // 
            this.objectsShowCheckBox.AutoSize = true;
            this.objectsShowCheckBox.Checked = true;
            this.objectsShowCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.objectsShowCheckBox.Location = new System.Drawing.Point(12, 287);
            this.objectsShowCheckBox.Name = "objectsShowCheckBox";
            this.objectsShowCheckBox.Size = new System.Drawing.Size(92, 17);
            this.objectsShowCheckBox.TabIndex = 17;
            this.objectsShowCheckBox.Text = "Show Objects";
            this.objectsShowCheckBox.UseVisualStyleBackColor = true;
            this.objectsShowCheckBox.CheckedChanged += new System.EventHandler(this.objectsShowCheckBox_CheckedChanged);
            // 
            // treesShowCheckBox
            // 
            this.treesShowCheckBox.AutoSize = true;
            this.treesShowCheckBox.Checked = true;
            this.treesShowCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.treesShowCheckBox.Location = new System.Drawing.Point(12, 264);
            this.treesShowCheckBox.Name = "treesShowCheckBox";
            this.treesShowCheckBox.Size = new System.Drawing.Size(83, 17);
            this.treesShowCheckBox.TabIndex = 18;
            this.treesShowCheckBox.Text = "Show Trees";
            this.treesShowCheckBox.UseVisualStyleBackColor = true;
            this.treesShowCheckBox.CheckedChanged += new System.EventHandler(this.treesShowCheckBox_CheckedChanged);
            // 
            // RowsNumericUpDown
            // 
            this.RowsNumericUpDown.Location = new System.Drawing.Point(78, 160);
            this.RowsNumericUpDown.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.RowsNumericUpDown.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.RowsNumericUpDown.Name = "RowsNumericUpDown";
            this.RowsNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.RowsNumericUpDown.TabIndex = 19;
            this.RowsNumericUpDown.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // ColsNumericUpDown
            // 
            this.ColsNumericUpDown.Location = new System.Drawing.Point(78, 186);
            this.ColsNumericUpDown.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.ColsNumericUpDown.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.ColsNumericUpDown.Name = "ColsNumericUpDown";
            this.ColsNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.ColsNumericUpDown.TabIndex = 20;
            this.ColsNumericUpDown.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Rows";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 188);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Columns";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Amplitude";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(468, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Persistence";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(340, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Frequency";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.TerrainPersistenceUpDown, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.TerrainFrequencyUpDown, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.TerrainAmplitudeUpDown, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.TerrainDefault, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.TerrainOctavesUpDown, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label9, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label10, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.LowerBoundCheckBox, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.label13, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.label15, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.TerrainUpperBoundUpDown, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.TerrainLowerBoundUpDown, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.TerrainVariationUpDown, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.TerrainThicknessUpDown, 5, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(180, 12);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(593, 139);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // TerrainPersistenceUpDown
            // 
            this.TerrainPersistenceUpDown.DecimalPlaces = 3;
            this.TerrainPersistenceUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.TerrainPersistenceUpDown.Location = new System.Drawing.Point(468, 37);
            this.TerrainPersistenceUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TerrainPersistenceUpDown.Name = "TerrainPersistenceUpDown";
            this.TerrainPersistenceUpDown.Size = new System.Drawing.Size(122, 20);
            this.TerrainPersistenceUpDown.TabIndex = 7;
            // 
            // TerrainFrequencyUpDown
            // 
            this.TerrainFrequencyUpDown.DecimalPlaces = 3;
            this.TerrainFrequencyUpDown.Location = new System.Drawing.Point(340, 37);
            this.TerrainFrequencyUpDown.Name = "TerrainFrequencyUpDown";
            this.TerrainFrequencyUpDown.Size = new System.Drawing.Size(122, 20);
            this.TerrainFrequencyUpDown.TabIndex = 6;
            // 
            // TerrainAmplitudeUpDown
            // 
            this.TerrainAmplitudeUpDown.DecimalPlaces = 3;
            this.TerrainAmplitudeUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.TerrainAmplitudeUpDown.Location = new System.Drawing.Point(212, 37);
            this.TerrainAmplitudeUpDown.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.TerrainAmplitudeUpDown.Name = "TerrainAmplitudeUpDown";
            this.TerrainAmplitudeUpDown.Size = new System.Drawing.Size(122, 20);
            this.TerrainAmplitudeUpDown.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Octaves";
            // 
            // TerrainOctavesUpDown
            // 
            this.TerrainOctavesUpDown.Location = new System.Drawing.Point(84, 37);
            this.TerrainOctavesUpDown.Name = "TerrainOctavesUpDown";
            this.TerrainOctavesUpDown.Size = new System.Drawing.Size(122, 20);
            this.TerrainOctavesUpDown.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(84, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Upper Bound";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.TreePersistenceUpDown, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.TreeFrequencyUpDown, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.TreeAmplitudeUpDown, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.TreeOctavesUpDown, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.TreeDefault, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(180, 162);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(593, 55);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // TreePersistenceUpDown
            // 
            this.TreePersistenceUpDown.DecimalPlaces = 3;
            this.TreePersistenceUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.TreePersistenceUpDown.Location = new System.Drawing.Point(468, 30);
            this.TreePersistenceUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TreePersistenceUpDown.Name = "TreePersistenceUpDown";
            this.TreePersistenceUpDown.Size = new System.Drawing.Size(122, 20);
            this.TreePersistenceUpDown.TabIndex = 11;
            // 
            // TreeFrequencyUpDown
            // 
            this.TreeFrequencyUpDown.DecimalPlaces = 3;
            this.TreeFrequencyUpDown.Location = new System.Drawing.Point(340, 30);
            this.TreeFrequencyUpDown.Name = "TreeFrequencyUpDown";
            this.TreeFrequencyUpDown.Size = new System.Drawing.Size(122, 20);
            this.TreeFrequencyUpDown.TabIndex = 10;
            // 
            // TreeAmplitudeUpDown
            // 
            this.TreeAmplitudeUpDown.DecimalPlaces = 3;
            this.TreeAmplitudeUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.TreeAmplitudeUpDown.Location = new System.Drawing.Point(212, 30);
            this.TreeAmplitudeUpDown.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.TreeAmplitudeUpDown.Name = "TreeAmplitudeUpDown";
            this.TreeAmplitudeUpDown.Size = new System.Drawing.Size(122, 20);
            this.TreeAmplitudeUpDown.TabIndex = 9;
            // 
            // TreeOctavesUpDown
            // 
            this.TreeOctavesUpDown.Location = new System.Drawing.Point(84, 30);
            this.TreeOctavesUpDown.Name = "TreeOctavesUpDown";
            this.TreeOctavesUpDown.Size = new System.Drawing.Size(122, 20);
            this.TreeOctavesUpDown.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(468, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Persistence";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Octaves";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(340, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Frequency";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(212, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Amplitude";
            // 
            // TreeDefault
            // 
            this.TreeDefault.Location = new System.Drawing.Point(3, 30);
            this.TreeDefault.Name = "TreeDefault";
            this.TreeDefault.Size = new System.Drawing.Size(75, 22);
            this.TreeDefault.TabIndex = 15;
            this.TreeDefault.Text = "Default";
            this.TreeDefault.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 26);
            this.label14.TabIndex = 9;
            this.label14.Text = "Tree\nParameters";
            // 
            // LowerBoundCheckBox
            // 
            this.LowerBoundCheckBox.AutoSize = true;
            this.LowerBoundCheckBox.Location = new System.Drawing.Point(212, 71);
            this.LowerBoundCheckBox.Name = "LowerBoundCheckBox";
            this.LowerBoundCheckBox.Size = new System.Drawing.Size(89, 17);
            this.LowerBoundCheckBox.TabIndex = 16;
            this.LowerBoundCheckBox.Text = "Lower Bound";
            this.LowerBoundCheckBox.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(340, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Variation";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(468, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Thickness";
            // 
            // TerrainUpperBoundUpDown
            // 
            this.TerrainUpperBoundUpDown.Location = new System.Drawing.Point(84, 105);
            this.TerrainUpperBoundUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.TerrainUpperBoundUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TerrainUpperBoundUpDown.Name = "TerrainUpperBoundUpDown";
            this.TerrainUpperBoundUpDown.Size = new System.Drawing.Size(120, 20);
            this.TerrainUpperBoundUpDown.TabIndex = 19;
            this.TerrainUpperBoundUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // TerrainLowerBoundUpDown
            // 
            this.TerrainLowerBoundUpDown.Location = new System.Drawing.Point(212, 105);
            this.TerrainLowerBoundUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.TerrainLowerBoundUpDown.Name = "TerrainLowerBoundUpDown";
            this.TerrainLowerBoundUpDown.Size = new System.Drawing.Size(120, 20);
            this.TerrainLowerBoundUpDown.TabIndex = 20;
            // 
            // TerrainVariationUpDown
            // 
            this.TerrainVariationUpDown.Location = new System.Drawing.Point(340, 105);
            this.TerrainVariationUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.TerrainVariationUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TerrainVariationUpDown.Name = "TerrainVariationUpDown";
            this.TerrainVariationUpDown.Size = new System.Drawing.Size(120, 20);
            this.TerrainVariationUpDown.TabIndex = 21;
            this.TerrainVariationUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // TerrainThicknessUpDown
            // 
            this.TerrainThicknessUpDown.Location = new System.Drawing.Point(468, 105);
            this.TerrainThicknessUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.TerrainThicknessUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TerrainThicknessUpDown.Name = "TerrainThicknessUpDown";
            this.TerrainThicknessUpDown.Size = new System.Drawing.Size(120, 20);
            this.TerrainThicknessUpDown.TabIndex = 22;
            this.TerrainThicknessUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 733);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ColsNumericUpDown);
            this.Controls.Add(this.RowsNumericUpDown);
            this.Controls.Add(this.treesShowCheckBox);
            this.Controls.Add(this.objectsShowCheckBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.WriteCheckBox);
            this.Controls.Add(this.Step3CheckBox);
            this.Controls.Add(this.Step2CheckBox);
            this.Controls.Add(this.Step1CheckBox);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.Write);
            this.Controls.Add(this.Step3);
            this.Controls.Add(this.Step2);
            this.Controls.Add(this.Step1);
            this.Controls.Add(this.GenAll);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(801, 347);
            this.Name = "Generator";
            this.Text = "Generator";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RowsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColsNumericUpDown)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainPersistenceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainFrequencyUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainAmplitudeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainOctavesUpDown)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePersistenceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeFrequencyUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeAmplitudeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeOctavesUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainUpperBoundUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainLowerBoundUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainVariationUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerrainThicknessUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GenAll;
        private System.Windows.Forms.Button Step1;
        private System.Windows.Forms.Button Step2;
        private System.Windows.Forms.Button Step3;
        private System.Windows.Forms.Button Write;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox Step1CheckBox;
        private System.Windows.Forms.CheckBox Step2CheckBox;
        private System.Windows.Forms.CheckBox Step3CheckBox;
        private System.Windows.Forms.CheckBox WriteCheckBox;
        private System.Windows.Forms.Button TerrainDefault;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox objectsShowCheckBox;
        private System.Windows.Forms.CheckBox treesShowCheckBox;
        private System.Windows.Forms.NumericUpDown RowsNumericUpDown;
        private System.Windows.Forms.NumericUpDown ColsNumericUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown TerrainPersistenceUpDown;
        private System.Windows.Forms.NumericUpDown TerrainFrequencyUpDown;
        private System.Windows.Forms.NumericUpDown TerrainAmplitudeUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown TerrainOctavesUpDown;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.NumericUpDown TreePersistenceUpDown;
        private System.Windows.Forms.NumericUpDown TreeFrequencyUpDown;
        private System.Windows.Forms.NumericUpDown TreeAmplitudeUpDown;
        private System.Windows.Forms.NumericUpDown TreeOctavesUpDown;
        private System.Windows.Forms.Button TreeDefault;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox LowerBoundCheckBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown TerrainUpperBoundUpDown;
        private System.Windows.Forms.NumericUpDown TerrainLowerBoundUpDown;
        private System.Windows.Forms.NumericUpDown TerrainVariationUpDown;
        private System.Windows.Forms.NumericUpDown TerrainThicknessUpDown;

    }
}