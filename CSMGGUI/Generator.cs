﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CSMGLibrary;

namespace CSMGGUI
{
    public partial class Generator : Form
    {
        private static int DEFAULT_TERRAIN_OCTAVES = 1;
        private static double DEFAULT_TERRAIN_AMPLITUDE = 10;
        private static double DEFAULT_TERRAIN_FREQUENCY = 0.04;
        private static double DEFAULT_TERRAIN_PERSISTENCE = 0.15;
        private static byte DEFAULT_TERRAIN_UPPERBOUND = 0x4c;
        private static byte DEFAULT_TERRAIN_LOWERBOUND = 0x3f;
        private static byte DEFAULT_TERRAIN_VARIATION = 0x04;
        private static byte DEFAULT_TERRAIN_THICKNESS = 0x03;
        private static int DEFAULT_TREE_OCTAVES = 1;
        private static double DEFAULT_TREE_AMPLITUDE = 10;
        private static double DEFAULT_TREE_FREQUENCY = 0.085;
        private static double DEFAULT_TREE_PERSISTENCE = 0.15;
        private LevelOld level;
        private Bitmap terrainBM;

        public Generator()
        {
            InitializeComponent();

            level = new LevelOld(128, 128, DEFAULT_TERRAIN_OCTAVES, DEFAULT_TERRAIN_AMPLITUDE, DEFAULT_TERRAIN_FREQUENCY, DEFAULT_TERRAIN_PERSISTENCE);
            terrainBM = new Bitmap(level.cols, level.rows);

            ResetTerrainValues();
            ResetTreeValues();
        }

        private void GenAll_Click(object sender, EventArgs e)
        {
            if (((int)RowsNumericUpDown.Value != level.rows) || ((int)ColsNumericUpDown.Value != level.cols))
            {
                level = new LevelOld((int)RowsNumericUpDown.Value, (int)ColsNumericUpDown.Value, DEFAULT_TERRAIN_OCTAVES, DEFAULT_TERRAIN_AMPLITUDE, DEFAULT_TERRAIN_FREQUENCY, DEFAULT_TERRAIN_PERSISTENCE);
                terrainBM = new Bitmap(level.cols, level.rows);
            }
            int tries = 0;
            do
            {
                if (tries++ == 100)
                {
                    MessageBox.Show("After 100 generations could not find suitable locations for objects", "Error");
                    return;
                }
                if (LowerBoundCheckBox.Checked)
                    level.GenerateTerrain(TerrainUpperBound, TerrainLowerBound, TerrainVariation, TerrainThickness, TerrainOctaves, TerrainAmplitude, TerrainFrequency, TerrainPersistence);
                level.GenerateTerrain(TerrainUpperBound, TerrainVariation, TerrainThickness, TerrainOctaves, TerrainAmplitude, TerrainFrequency, TerrainPersistence);
            } while (!level.placeCrystalAndPlayerSpawn() || !level.placeMine() || !level.placeEnemySpawn());
            level.FillTreeGrid(TreeOctaves, TreeAmplitude, TreeFrequency, TreePersistence);
            level.WriteLevel();

            makeImage();

            Step1CheckBox.Checked = true;
            Step2CheckBox.Checked = true;
            Step3CheckBox.Checked = true;
            WriteCheckBox.Checked = true;
            Step2.Enabled = true;
            Step3.Enabled = true;
            Write.Enabled = true;
        }

        private void Step1_Click(object sender, EventArgs e)
        {
            if (((int)RowsNumericUpDown.Value != level.rows) || ((int)ColsNumericUpDown.Value != level.cols))
            {
                level = new LevelOld((int)RowsNumericUpDown.Value, (int)ColsNumericUpDown.Value, DEFAULT_TERRAIN_OCTAVES, DEFAULT_TERRAIN_AMPLITUDE, DEFAULT_TERRAIN_FREQUENCY, DEFAULT_TERRAIN_PERSISTENCE);
                terrainBM = new Bitmap(level.cols, level.rows);
            }
            if (LowerBoundCheckBox.Checked)
                level.GenerateTerrain(TerrainUpperBound, TerrainLowerBound, TerrainVariation, TerrainThickness, TerrainOctaves, TerrainAmplitude, TerrainFrequency, TerrainPersistence);
            level.GenerateTerrain(TerrainUpperBound, TerrainVariation, TerrainThickness, TerrainOctaves, TerrainAmplitude, TerrainFrequency, TerrainPersistence);

            makeImage();

            Step1CheckBox.Checked = true;
            Step2CheckBox.Checked = false;
            Step3CheckBox.Checked = false;
            WriteCheckBox.Checked = false;
            Step2.Enabled = true;
            Step3.Enabled = false;
            Write.Enabled = false;
        }

        private void Step2_Click(object sender, EventArgs e)
        {
            if (!level.placeCrystalAndPlayerSpawn() || !level.placeMine() || !level.placeEnemySpawn())
            {
                MessageBox.Show("Failed to place objects (Too rough terrain?)", "Error");

                Step1CheckBox.Checked = true;
                Step2CheckBox.Checked = false;
                Step3CheckBox.Checked = false;
                WriteCheckBox.Checked = false;
                Step3.Enabled = false;
                Write.Enabled = false;
            }

            makeImage();

            Step1CheckBox.Checked = true;
            Step2CheckBox.Checked = true;
            Step3CheckBox.Checked = false;
            WriteCheckBox.Checked = false;
            Step3.Enabled = true;
            Write.Enabled = false;
        }

        private void Step3_Click(object sender, EventArgs e)
        {
            level.FillTreeGrid(TreeOctaves, TreeAmplitude, TreeFrequency, TreePersistence);

            makeImage();

            Step1CheckBox.Checked = true;
            Step2CheckBox.Checked = true;
            Step3CheckBox.Checked = true;
            WriteCheckBox.Checked = false;
            Write.Enabled = true;
        }

        private void Write_Click(object sender, EventArgs e)
        {
            level.WriteLevel();

            makeImage();

            Step1CheckBox.Checked = true;
            Step2CheckBox.Checked = true;
            Step3CheckBox.Checked = true;
            WriteCheckBox.Checked = true;
        }

        private void TerrainDefault_Click(object sender, EventArgs e)
        {
            ResetTerrainValues();
        }

        private void TreeDefault_Click(object sender, EventArgs e)
        {
            ResetTreeValues();
        }

        private void objectsShowCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            makeImage();
        }

        private void treesShowCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            makeImage();
        }


        private void makeImage()
        {
            for (int i = 0; i < level.rows; i++)
            {
                for (int j = 0; j < level.cols; j++)
                {
                    terrainBM.SetPixel(j, i, Color.FromArgb(Byte.MaxValue-level.Heights[i, j], Byte.MaxValue-level.Heights[i, j], Byte.MaxValue-level.Heights[i, j]));
                    if (treesShowCheckBox.Checked && level.Trees[i, j] == true)
                        terrainBM.SetPixel(j, i, Color.FromArgb(0, 128, 0));
                }
            }
            if (objectsShowCheckBox.Checked)
            {
                terrainBM.SetPixel(level.PlayerY, level.PlayerX, Color.FromArgb(128, 128, 0));
                terrainBM.SetPixel(level.MineY, level.MineX, Color.FromArgb(0, 0, 128));
                terrainBM.SetPixel(level.EnemyY, level.EnemyX, Color.FromArgb(128, 0, 0));
            }
            pictureBox1.Image = (Image)terrainBM;
            pictureBox1.Image.RotateFlip(RotateFlipType.RotateNoneFlipY);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void ResetTerrainValues()
        {
            TerrainOctaves = DEFAULT_TERRAIN_OCTAVES;
            TerrainAmplitude = DEFAULT_TERRAIN_AMPLITUDE;
            TerrainFrequency = DEFAULT_TERRAIN_FREQUENCY;
            TerrainPersistence = DEFAULT_TERRAIN_PERSISTENCE;
            TerrainUpperBound = DEFAULT_TERRAIN_UPPERBOUND;
            TerrainLowerBound = DEFAULT_TERRAIN_LOWERBOUND;
            TerrainVariation = DEFAULT_TERRAIN_VARIATION;
            TerrainThickness = DEFAULT_TERRAIN_THICKNESS;
        }

        private void ResetTreeValues()
        {
            TreeOctaves = DEFAULT_TREE_OCTAVES;
            TreeAmplitude = DEFAULT_TREE_AMPLITUDE;
            TreeFrequency = DEFAULT_TREE_FREQUENCY;
            TreePersistence = DEFAULT_TREE_PERSISTENCE;
        }

        private int TerrainOctaves
        {
            get { return (int)TerrainOctavesUpDown.Value; }
            set { TerrainOctavesUpDown.Value = (decimal)value; }
        }
        private double TerrainAmplitude
        {
            get { return (double)TerrainAmplitudeUpDown.Value; }
            set { TerrainAmplitudeUpDown.Value = (decimal)value; }
        }
        private double TerrainFrequency
        {
            get { return (double)TerrainFrequencyUpDown.Value; }
            set { TerrainFrequencyUpDown.Value = (decimal)value; }
        }
        private double TerrainPersistence
        {
            get { return (double)TerrainPersistenceUpDown.Value; }
            set { TerrainPersistenceUpDown.Value = (decimal)value; }
        }
        private byte TerrainUpperBound
        {
            get { return (byte)TerrainUpperBoundUpDown.Value; }
            set { TerrainUpperBoundUpDown.Value = value; }
        }
        private byte TerrainLowerBound
        {
            get { return (byte)TerrainLowerBoundUpDown.Value; }
            set { TerrainLowerBoundUpDown.Value = value; }
        }
        private byte TerrainVariation
        {
            get { return (byte)TerrainVariationUpDown.Value; }
            set { TerrainVariationUpDown.Value = value; }
        }
        private byte TerrainThickness
        {
            get { return (byte)TerrainThicknessUpDown.Value; }
            set { TerrainThicknessUpDown.Value = value; }
        }
        private int TreeOctaves
        {
            get { return (int)TreeOctavesUpDown.Value; }
            set { TreeOctavesUpDown.Value = (decimal)value; }
        }
        private double TreeAmplitude
        {
            get { return (double)TreeAmplitudeUpDown.Value; }
            set { TreeAmplitudeUpDown.Value = (decimal)value; }
        }
        private double TreeFrequency
        {
            get { return (double)TreeFrequencyUpDown.Value; }
            set { TreeFrequencyUpDown.Value = (decimal)value; }
        }
        private double TreePersistence
        {
            get { return (double)TreePersistenceUpDown.Value; }
            set { TreePersistenceUpDown.Value = (decimal)value; }
        }
    }
}
