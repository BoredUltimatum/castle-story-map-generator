﻿namespace CSMGGUI
{
    partial class CastleStoryMapGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yLengthUpDown = new System.Windows.Forms.NumericUpDown();
            this.xLengthUpDown = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.terrainCompletedCheckBox = new System.Windows.Forms.CheckBox();
            this.objectsCompletedCheckBox = new System.Windows.Forms.CheckBox();
            this.treesCompletedCheckBox = new System.Windows.Forms.CheckBox();
            this.writtenCheckBox = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.stageTabControl = new System.Windows.Forms.TabControl();
            this.tcMain_Terrain = new System.Windows.Forms.TabPage();
            this.tcm_Terrain_largestIslandButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_clearButton = new System.Windows.Forms.Button();
            this.stage_TerrainTabControl = new System.Windows.Forms.TabControl();
            this.tcMain_Terrain_NoiseGrid = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tcm_Terrain_NoiseGrid_DefaultAllButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton = new System.Windows.Forms.Button();
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox = new System.Windows.Forms.CheckBox();
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox = new System.Windows.Forms.CheckBox();
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox = new System.Windows.Forms.CheckBox();
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_FrequencyUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_LowerBoundUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_VariationUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_OctavesUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_HighPassUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Terrain_NoiseGrid_LowPassUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcMain_Terrain_Procedural = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.tcm_Terrain_generateButton = new System.Windows.Forms.Button();
            this.tcMain_Objects = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tcm_Objects_placeCrystalPlayerButton = new System.Windows.Forms.Button();
            this.tcm_Objects_removeLastEnemyButton = new System.Windows.Forms.Button();
            this.tcm_Objects_placeMineButton = new System.Windows.Forms.Button();
            this.tcm_Objects_removeLastMineButton = new System.Windows.Forms.Button();
            this.tcm_Objects_placeEnemyButton = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tcm_Objects_crystalMineDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_crystalEnemyDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_mineCrystalDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_mineMineDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_mineEnemyDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_enemyCrystalDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_enemyMineDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_enemyEnemyDistanceUpDown = new System.Windows.Forms.NumericUpDown();
            this.tcm_Objects_clearEnemiesButton = new System.Windows.Forms.Button();
            this.tcm_Objects_clearMinesButton = new System.Windows.Forms.Button();
            this.tcm_Objects_clearAllButton = new System.Windows.Forms.Button();
            this.tcm_Objects_autoPlaceButton = new System.Windows.Forms.Button();
            this.tcMain_Trees = new System.Windows.Forms.TabPage();
            this.tcm_Trees_clearButton = new System.Windows.Forms.Button();
            this.tcm_Trees_generateButton = new System.Windows.Forms.Button();
            this.tcMain_Write = new System.Windows.Forms.TabPage();
            this.tcm_Write_negateButton = new System.Windows.Forms.Button();
            this.tcm_Write_writeButton = new System.Windows.Forms.Button();
            this.previewPictureBox = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.yLengthUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLengthUpDown)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.stageTabControl.SuspendLayout();
            this.tcMain_Terrain.SuspendLayout();
            this.stage_TerrainTabControl.SuspendLayout();
            this.tcMain_Terrain_NoiseGrid.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_AmplitudeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_FrequencyUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_PersistenceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_UpperBoundUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_LowerBoundUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_VariationUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_ThicknessUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_OctavesUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_HighPassUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_LowPassUpDown)).BeginInit();
            this.tcMain_Terrain_Procedural.SuspendLayout();
            this.tcMain_Objects.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_crystalMineDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_crystalEnemyDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_mineCrystalDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_mineMineDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_mineEnemyDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_enemyCrystalDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_enemyMineDistanceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_enemyEnemyDistanceUpDown)).BeginInit();
            this.tcMain_Trees.SuspendLayout();
            this.tcMain_Write.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "X Length";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y Length";
            // 
            // yLengthUpDown
            // 
            this.yLengthUpDown.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.yLengthUpDown.Location = new System.Drawing.Point(59, 29);
            this.yLengthUpDown.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.yLengthUpDown.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.yLengthUpDown.Name = "yLengthUpDown";
            this.yLengthUpDown.Size = new System.Drawing.Size(55, 20);
            this.yLengthUpDown.TabIndex = 2;
            this.yLengthUpDown.Value = new decimal(new int[] {
            256,
            0,
            0,
            0});
            // 
            // xLengthUpDown
            // 
            this.xLengthUpDown.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.xLengthUpDown.Location = new System.Drawing.Point(59, 3);
            this.xLengthUpDown.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.xLengthUpDown.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.xLengthUpDown.Name = "xLengthUpDown";
            this.xLengthUpDown.Size = new System.Drawing.Size(55, 20);
            this.xLengthUpDown.TabIndex = 1;
            this.xLengthUpDown.Value = new decimal(new int[] {
            256,
            0,
            0,
            0});
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.yLengthUpDown, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.xLengthUpDown, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(118, 53);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.terrainCompletedCheckBox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.objectsCompletedCheckBox, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.treesCompletedCheckBox, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.writtenCheckBox, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label20, 0, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(13, 89);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(118, 135);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // terrainCompletedCheckBox
            // 
            this.terrainCompletedCheckBox.AutoSize = true;
            this.terrainCompletedCheckBox.Enabled = false;
            this.terrainCompletedCheckBox.Location = new System.Drawing.Point(101, 30);
            this.terrainCompletedCheckBox.Name = "terrainCompletedCheckBox";
            this.terrainCompletedCheckBox.Size = new System.Drawing.Size(14, 14);
            this.terrainCompletedCheckBox.TabIndex = 5;
            this.terrainCompletedCheckBox.UseVisualStyleBackColor = true;
            // 
            // objectsCompletedCheckBox
            // 
            this.objectsCompletedCheckBox.AutoSize = true;
            this.objectsCompletedCheckBox.Enabled = false;
            this.objectsCompletedCheckBox.Location = new System.Drawing.Point(101, 57);
            this.objectsCompletedCheckBox.Name = "objectsCompletedCheckBox";
            this.objectsCompletedCheckBox.Size = new System.Drawing.Size(14, 14);
            this.objectsCompletedCheckBox.TabIndex = 6;
            this.objectsCompletedCheckBox.UseVisualStyleBackColor = true;
            // 
            // treesCompletedCheckBox
            // 
            this.treesCompletedCheckBox.AutoSize = true;
            this.treesCompletedCheckBox.Enabled = false;
            this.treesCompletedCheckBox.Location = new System.Drawing.Point(101, 84);
            this.treesCompletedCheckBox.Name = "treesCompletedCheckBox";
            this.treesCompletedCheckBox.Size = new System.Drawing.Size(14, 14);
            this.treesCompletedCheckBox.TabIndex = 7;
            this.treesCompletedCheckBox.UseVisualStyleBackColor = true;
            // 
            // writtenCheckBox
            // 
            this.writtenCheckBox.AutoSize = true;
            this.writtenCheckBox.Enabled = false;
            this.writtenCheckBox.Location = new System.Drawing.Point(101, 111);
            this.writtenCheckBox.Name = "writtenCheckBox";
            this.writtenCheckBox.Size = new System.Drawing.Size(14, 14);
            this.writtenCheckBox.TabIndex = 8;
            this.writtenCheckBox.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Terrain";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "Objects";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "Trees";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 108);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Write Level";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Completed:";
            // 
            // stageTabControl
            // 
            this.stageTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stageTabControl.Controls.Add(this.tcMain_Terrain);
            this.stageTabControl.Controls.Add(this.tcMain_Objects);
            this.stageTabControl.Controls.Add(this.tcMain_Trees);
            this.stageTabControl.Controls.Add(this.tcMain_Write);
            this.stageTabControl.Location = new System.Drawing.Point(138, 13);
            this.stageTabControl.Name = "stageTabControl";
            this.stageTabControl.SelectedIndex = 0;
            this.stageTabControl.Size = new System.Drawing.Size(634, 211);
            this.stageTabControl.TabIndex = 3;
            // 
            // tcMain_Terrain
            // 
            this.tcMain_Terrain.Controls.Add(this.tcm_Terrain_largestIslandButton);
            this.tcMain_Terrain.Controls.Add(this.tcm_Terrain_clearButton);
            this.tcMain_Terrain.Controls.Add(this.stage_TerrainTabControl);
            this.tcMain_Terrain.Controls.Add(this.tcm_Terrain_generateButton);
            this.tcMain_Terrain.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Terrain.Name = "tcMain_Terrain";
            this.tcMain_Terrain.Size = new System.Drawing.Size(626, 185);
            this.tcMain_Terrain.TabIndex = 0;
            this.tcMain_Terrain.Text = "1 Terrain";
            this.tcMain_Terrain.UseVisualStyleBackColor = true;
            // 
            // tcm_Terrain_largestIslandButton
            // 
            this.tcm_Terrain_largestIslandButton.Location = new System.Drawing.Point(166, 3);
            this.tcm_Terrain_largestIslandButton.Name = "tcm_Terrain_largestIslandButton";
            this.tcm_Terrain_largestIslandButton.Size = new System.Drawing.Size(138, 23);
            this.tcm_Terrain_largestIslandButton.TabIndex = 6;
            this.tcm_Terrain_largestIslandButton.Text = "Reduce to Largest Island";
            this.tcm_Terrain_largestIslandButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_largestIslandButton.Click += new System.EventHandler(this.tcm_Terrain_largestIslandButton_Click);
            // 
            // tcm_Terrain_clearButton
            // 
            this.tcm_Terrain_clearButton.Location = new System.Drawing.Point(84, 3);
            this.tcm_Terrain_clearButton.Name = "tcm_Terrain_clearButton";
            this.tcm_Terrain_clearButton.Size = new System.Drawing.Size(75, 23);
            this.tcm_Terrain_clearButton.TabIndex = 5;
            this.tcm_Terrain_clearButton.Text = "Clear";
            this.tcm_Terrain_clearButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_clearButton.Click += new System.EventHandler(this.tcm_Terrain_clearButton_Click);
            // 
            // stage_TerrainTabControl
            // 
            this.stage_TerrainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stage_TerrainTabControl.Controls.Add(this.tcMain_Terrain_NoiseGrid);
            this.stage_TerrainTabControl.Controls.Add(this.tcMain_Terrain_Procedural);
            this.stage_TerrainTabControl.Location = new System.Drawing.Point(3, 32);
            this.stage_TerrainTabControl.Name = "stage_TerrainTabControl";
            this.stage_TerrainTabControl.SelectedIndex = 0;
            this.stage_TerrainTabControl.Size = new System.Drawing.Size(623, 150);
            this.stage_TerrainTabControl.TabIndex = 0;
            // 
            // tcMain_Terrain_NoiseGrid
            // 
            this.tcMain_Terrain_NoiseGrid.AutoScroll = true;
            this.tcMain_Terrain_NoiseGrid.Controls.Add(this.tableLayoutPanel3);
            this.tcMain_Terrain_NoiseGrid.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Terrain_NoiseGrid.Name = "tcMain_Terrain_NoiseGrid";
            this.tcMain_Terrain_NoiseGrid.Size = new System.Drawing.Size(615, 124);
            this.tcMain_Terrain_NoiseGrid.TabIndex = 2;
            this.tcMain_Terrain_NoiseGrid.Text = "Noise Grid";
            this.tcMain_Terrain_NoiseGrid.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153F));
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_DefaultAllButton, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_OctavesDefaultButton, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_VariationDefaultButton, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.label15, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_HighPassDefaultButton, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_LowPassDefaultButton, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.label17, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_HighPassCheckBox, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_LowPassCheckBox, 2, 10);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_AmplitudeUpDown, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_FrequencyUpDown, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_PersistenceUpDown, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_UpperBoundUpDown, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_LowerBoundUpDown, 3, 6);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_VariationUpDown, 3, 7);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_ThicknessUpDown, 3, 8);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_OctavesUpDown, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_HighPassUpDown, 3, 9);
            this.tableLayoutPanel3.Controls.Add(this.tcm_Terrain_NoiseGrid_LowPassUpDown, 3, 10);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 11;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(373, 228);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tcm_Terrain_NoiseGrid_DefaultAllButton
            // 
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.Location = new System.Drawing.Point(103, 3);
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.Name = "tcm_Terrain_NoiseGrid_DefaultAllButton";
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.Size = new System.Drawing.Size(54, 19);
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.TabIndex = 5;
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.Text = "Default";
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_DefaultAllButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_DefaultAllButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Parameter";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Octaves";
            // 
            // tcm_Terrain_NoiseGrid_OctavesDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton.Location = new System.Drawing.Point(103, 28);
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton.Name = "tcm_Terrain_NoiseGrid_OctavesDefaultButton";
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton.TabIndex = 3;
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_OctavesDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Amplitude";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Frequency";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Persistence";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Upper Bound";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Lower Bound";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 145);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Variation";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 165);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Thickness";
            // 
            // tcm_Terrain_NoiseGrid_AmplitudeDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton.Location = new System.Drawing.Point(103, 48);
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton.Name = "tcm_Terrain_NoiseGrid_AmplitudeDefaultButton";
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton.TabIndex = 13;
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_AmplitudeDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_FrequencyDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton.Location = new System.Drawing.Point(103, 68);
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton.Name = "tcm_Terrain_NoiseGrid_FrequencyDefaultButton";
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton.TabIndex = 14;
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_FrequencyDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_PersistenceDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton.Location = new System.Drawing.Point(103, 88);
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton.Name = "tcm_Terrain_NoiseGrid_PersistenceDefaultButton";
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton.TabIndex = 15;
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_PersistenceDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_UpperBoundDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton.Location = new System.Drawing.Point(103, 108);
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton.Name = "tcm_Terrain_NoiseGrid_UpperBoundDefaultButton";
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton.TabIndex = 16;
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_UpperBoundDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_LowerBoundDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton.Location = new System.Drawing.Point(103, 128);
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton.Name = "tcm_Terrain_NoiseGrid_LowerBoundDefaultButton";
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton.TabIndex = 17;
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_LowerBoundDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_VariationDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton.Location = new System.Drawing.Point(103, 148);
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton.Name = "tcm_Terrain_NoiseGrid_VariationDefaultButton";
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton.TabIndex = 18;
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_VariationDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_ThicknessDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton.Location = new System.Drawing.Point(103, 168);
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton.Name = "tcm_Terrain_NoiseGrid_ThicknessDefaultButton";
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton.TabIndex = 19;
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_ThicknessDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 185);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "High Pass";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Low Pass";
            // 
            // tcm_Terrain_NoiseGrid_HighPassDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton.Location = new System.Drawing.Point(103, 188);
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton.Name = "tcm_Terrain_NoiseGrid_HighPassDefaultButton";
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton.TabIndex = 22;
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_HighPassDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // tcm_Terrain_NoiseGrid_LowPassDefaultButton
            // 
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton.Location = new System.Drawing.Point(103, 208);
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton.Name = "tcm_Terrain_NoiseGrid_LowPassDefaultButton";
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton.Size = new System.Drawing.Size(54, 14);
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton.TabIndex = 23;
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_NoiseGrid_LowPassDefaultButton.Click += new System.EventHandler(this.tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(163, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Enabled";
            // 
            // tcm_Terrain_NoiseGrid_LowerBoundCheckBox
            // 
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox.AutoSize = true;
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox.Location = new System.Drawing.Point(163, 128);
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox.Name = "tcm_Terrain_NoiseGrid_LowerBoundCheckBox";
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox.Size = new System.Drawing.Size(15, 14);
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox.TabIndex = 11;
            this.tcm_Terrain_NoiseGrid_LowerBoundCheckBox.UseVisualStyleBackColor = true;
            // 
            // tcm_Terrain_NoiseGrid_HighPassCheckBox
            // 
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox.AutoSize = true;
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox.Location = new System.Drawing.Point(163, 188);
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox.Name = "tcm_Terrain_NoiseGrid_HighPassCheckBox";
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox.Size = new System.Drawing.Size(15, 14);
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox.TabIndex = 15;
            this.tcm_Terrain_NoiseGrid_HighPassCheckBox.UseVisualStyleBackColor = true;
            // 
            // tcm_Terrain_NoiseGrid_LowPassCheckBox
            // 
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox.AutoSize = true;
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox.Location = new System.Drawing.Point(163, 208);
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox.Name = "tcm_Terrain_NoiseGrid_LowPassCheckBox";
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox.Size = new System.Drawing.Size(15, 14);
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox.TabIndex = 17;
            this.tcm_Terrain_NoiseGrid_LowPassCheckBox.UseVisualStyleBackColor = true;
            // 
            // tcm_Terrain_NoiseGrid_AmplitudeUpDown
            // 
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.DecimalPlaces = 3;
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.Location = new System.Drawing.Point(223, 48);
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.Name = "tcm_Terrain_NoiseGrid_AmplitudeUpDown";
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.Size = new System.Drawing.Size(122, 20);
            this.tcm_Terrain_NoiseGrid_AmplitudeUpDown.TabIndex = 7;
            // 
            // tcm_Terrain_NoiseGrid_FrequencyUpDown
            // 
            this.tcm_Terrain_NoiseGrid_FrequencyUpDown.DecimalPlaces = 3;
            this.tcm_Terrain_NoiseGrid_FrequencyUpDown.Location = new System.Drawing.Point(223, 68);
            this.tcm_Terrain_NoiseGrid_FrequencyUpDown.Name = "tcm_Terrain_NoiseGrid_FrequencyUpDown";
            this.tcm_Terrain_NoiseGrid_FrequencyUpDown.Size = new System.Drawing.Size(122, 20);
            this.tcm_Terrain_NoiseGrid_FrequencyUpDown.TabIndex = 8;
            // 
            // tcm_Terrain_NoiseGrid_PersistenceUpDown
            // 
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.DecimalPlaces = 3;
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.Location = new System.Drawing.Point(223, 88);
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.Name = "tcm_Terrain_NoiseGrid_PersistenceUpDown";
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.Size = new System.Drawing.Size(122, 20);
            this.tcm_Terrain_NoiseGrid_PersistenceUpDown.TabIndex = 9;
            // 
            // tcm_Terrain_NoiseGrid_UpperBoundUpDown
            // 
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.Location = new System.Drawing.Point(223, 108);
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.Name = "tcm_Terrain_NoiseGrid_UpperBoundUpDown";
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.Size = new System.Drawing.Size(120, 20);
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.TabIndex = 10;
            this.tcm_Terrain_NoiseGrid_UpperBoundUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tcm_Terrain_NoiseGrid_LowerBoundUpDown
            // 
            this.tcm_Terrain_NoiseGrid_LowerBoundUpDown.Location = new System.Drawing.Point(223, 128);
            this.tcm_Terrain_NoiseGrid_LowerBoundUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_LowerBoundUpDown.Name = "tcm_Terrain_NoiseGrid_LowerBoundUpDown";
            this.tcm_Terrain_NoiseGrid_LowerBoundUpDown.Size = new System.Drawing.Size(120, 20);
            this.tcm_Terrain_NoiseGrid_LowerBoundUpDown.TabIndex = 12;
            // 
            // tcm_Terrain_NoiseGrid_VariationUpDown
            // 
            this.tcm_Terrain_NoiseGrid_VariationUpDown.Location = new System.Drawing.Point(223, 148);
            this.tcm_Terrain_NoiseGrid_VariationUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_VariationUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_VariationUpDown.Name = "tcm_Terrain_NoiseGrid_VariationUpDown";
            this.tcm_Terrain_NoiseGrid_VariationUpDown.Size = new System.Drawing.Size(120, 20);
            this.tcm_Terrain_NoiseGrid_VariationUpDown.TabIndex = 13;
            this.tcm_Terrain_NoiseGrid_VariationUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tcm_Terrain_NoiseGrid_ThicknessUpDown
            // 
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.Location = new System.Drawing.Point(223, 168);
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.Name = "tcm_Terrain_NoiseGrid_ThicknessUpDown";
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.Size = new System.Drawing.Size(120, 20);
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.TabIndex = 14;
            this.tcm_Terrain_NoiseGrid_ThicknessUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tcm_Terrain_NoiseGrid_OctavesUpDown
            // 
            this.tcm_Terrain_NoiseGrid_OctavesUpDown.Location = new System.Drawing.Point(223, 28);
            this.tcm_Terrain_NoiseGrid_OctavesUpDown.Name = "tcm_Terrain_NoiseGrid_OctavesUpDown";
            this.tcm_Terrain_NoiseGrid_OctavesUpDown.Size = new System.Drawing.Size(122, 20);
            this.tcm_Terrain_NoiseGrid_OctavesUpDown.TabIndex = 6;
            // 
            // tcm_Terrain_NoiseGrid_HighPassUpDown
            // 
            this.tcm_Terrain_NoiseGrid_HighPassUpDown.Location = new System.Drawing.Point(223, 188);
            this.tcm_Terrain_NoiseGrid_HighPassUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_HighPassUpDown.Name = "tcm_Terrain_NoiseGrid_HighPassUpDown";
            this.tcm_Terrain_NoiseGrid_HighPassUpDown.Size = new System.Drawing.Size(120, 20);
            this.tcm_Terrain_NoiseGrid_HighPassUpDown.TabIndex = 16;
            this.tcm_Terrain_NoiseGrid_HighPassUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tcm_Terrain_NoiseGrid_LowPassUpDown
            // 
            this.tcm_Terrain_NoiseGrid_LowPassUpDown.Location = new System.Drawing.Point(223, 208);
            this.tcm_Terrain_NoiseGrid_LowPassUpDown.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.tcm_Terrain_NoiseGrid_LowPassUpDown.Name = "tcm_Terrain_NoiseGrid_LowPassUpDown";
            this.tcm_Terrain_NoiseGrid_LowPassUpDown.Size = new System.Drawing.Size(120, 20);
            this.tcm_Terrain_NoiseGrid_LowPassUpDown.TabIndex = 18;
            this.tcm_Terrain_NoiseGrid_LowPassUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tcMain_Terrain_Procedural
            // 
            this.tcMain_Terrain_Procedural.Controls.Add(this.label4);
            this.tcMain_Terrain_Procedural.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Terrain_Procedural.Name = "tcMain_Terrain_Procedural";
            this.tcMain_Terrain_Procedural.Size = new System.Drawing.Size(615, 124);
            this.tcMain_Terrain_Procedural.TabIndex = 1;
            this.tcMain_Terrain_Procedural.Text = "Procedural";
            this.tcMain_Terrain_Procedural.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(272, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Coming Soon";
            // 
            // tcm_Terrain_generateButton
            // 
            this.tcm_Terrain_generateButton.Location = new System.Drawing.Point(3, 3);
            this.tcm_Terrain_generateButton.Name = "tcm_Terrain_generateButton";
            this.tcm_Terrain_generateButton.Size = new System.Drawing.Size(75, 23);
            this.tcm_Terrain_generateButton.TabIndex = 4;
            this.tcm_Terrain_generateButton.Text = "Generate";
            this.tcm_Terrain_generateButton.UseVisualStyleBackColor = true;
            this.tcm_Terrain_generateButton.Click += new System.EventHandler(this.tcm_Terrain_generateButton_Click);
            // 
            // tcMain_Objects
            // 
            this.tcMain_Objects.Controls.Add(this.label26);
            this.tcMain_Objects.Controls.Add(this.tableLayoutPanel4);
            this.tcMain_Objects.Controls.Add(this.tcm_Objects_clearEnemiesButton);
            this.tcMain_Objects.Controls.Add(this.tcm_Objects_clearMinesButton);
            this.tcMain_Objects.Controls.Add(this.tcm_Objects_clearAllButton);
            this.tcMain_Objects.Controls.Add(this.tcm_Objects_autoPlaceButton);
            this.tcMain_Objects.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Objects.Name = "tcMain_Objects";
            this.tcMain_Objects.Size = new System.Drawing.Size(626, 185);
            this.tcMain_Objects.TabIndex = 1;
            this.tcMain_Objects.Text = "2 Objects";
            this.tcMain_Objects.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 6;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 216F));
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_placeCrystalPlayerButton, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_removeLastEnemyButton, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_placeMineButton, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_removeLastMineButton, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_placeEnemyButton, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label22, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label23, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label24, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.label25, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_crystalMineDistanceUpDown, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_crystalEnemyDistanceUpDown, 4, 1);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_mineCrystalDistanceUpDown, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_mineMineDistanceUpDown, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_mineEnemyDistanceUpDown, 4, 2);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_enemyCrystalDistanceUpDown, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_enemyMineDistanceUpDown, 3, 3);
            this.tableLayoutPanel4.Controls.Add(this.tcm_Objects_enemyEnemyDistanceUpDown, 4, 3);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 54);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(626, 131);
            this.tableLayoutPanel4.TabIndex = 18;
            // 
            // tcm_Objects_placeCrystalPlayerButton
            // 
            this.tcm_Objects_placeCrystalPlayerButton.Location = new System.Drawing.Point(3, 28);
            this.tcm_Objects_placeCrystalPlayerButton.Name = "tcm_Objects_placeCrystalPlayerButton";
            this.tcm_Objects_placeCrystalPlayerButton.Size = new System.Drawing.Size(120, 19);
            this.tcm_Objects_placeCrystalPlayerButton.TabIndex = 11;
            this.tcm_Objects_placeCrystalPlayerButton.Text = "Place Crystal/Player";
            this.tcm_Objects_placeCrystalPlayerButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_placeCrystalPlayerButton.Click += new System.EventHandler(this.tcm_Objects_placeCrystalPlayerButton_Click);
            // 
            // tcm_Objects_removeLastEnemyButton
            // 
            this.tcm_Objects_removeLastEnemyButton.Enabled = false;
            this.tcm_Objects_removeLastEnemyButton.Location = new System.Drawing.Point(133, 78);
            this.tcm_Objects_removeLastEnemyButton.Name = "tcm_Objects_removeLastEnemyButton";
            this.tcm_Objects_removeLastEnemyButton.Size = new System.Drawing.Size(90, 19);
            this.tcm_Objects_removeLastEnemyButton.TabIndex = 17;
            this.tcm_Objects_removeLastEnemyButton.Text = "X Last Enemy";
            this.tcm_Objects_removeLastEnemyButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_removeLastEnemyButton.Click += new System.EventHandler(this.tcm_Objects_removeLastEnemyButton_Click);
            // 
            // tcm_Objects_placeMineButton
            // 
            this.tcm_Objects_placeMineButton.Location = new System.Drawing.Point(3, 53);
            this.tcm_Objects_placeMineButton.Name = "tcm_Objects_placeMineButton";
            this.tcm_Objects_placeMineButton.Size = new System.Drawing.Size(90, 19);
            this.tcm_Objects_placeMineButton.TabIndex = 12;
            this.tcm_Objects_placeMineButton.Text = "Place Mine";
            this.tcm_Objects_placeMineButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_placeMineButton.Click += new System.EventHandler(this.tcm_Objects_placeMineButton_Click);
            // 
            // tcm_Objects_removeLastMineButton
            // 
            this.tcm_Objects_removeLastMineButton.Enabled = false;
            this.tcm_Objects_removeLastMineButton.Location = new System.Drawing.Point(133, 53);
            this.tcm_Objects_removeLastMineButton.Name = "tcm_Objects_removeLastMineButton";
            this.tcm_Objects_removeLastMineButton.Size = new System.Drawing.Size(90, 19);
            this.tcm_Objects_removeLastMineButton.TabIndex = 16;
            this.tcm_Objects_removeLastMineButton.Text = "X Last Mine";
            this.tcm_Objects_removeLastMineButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_removeLastMineButton.Click += new System.EventHandler(this.tcm_Objects_removeLastMineButton_Click);
            // 
            // tcm_Objects_placeEnemyButton
            // 
            this.tcm_Objects_placeEnemyButton.Location = new System.Drawing.Point(3, 78);
            this.tcm_Objects_placeEnemyButton.Name = "tcm_Objects_placeEnemyButton";
            this.tcm_Objects_placeEnemyButton.Size = new System.Drawing.Size(90, 19);
            this.tcm_Objects_placeEnemyButton.TabIndex = 13;
            this.tcm_Objects_placeEnemyButton.Text = "Place Enemy";
            this.tcm_Objects_placeEnemyButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_placeEnemyButton.Click += new System.EventHandler(this.tcm_Objects_placeEnemyButton_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "Place Entity";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(133, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 13);
            this.label22.TabIndex = 19;
            this.label22.Text = "Remove Last";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(233, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 13);
            this.label23.TabIndex = 20;
            this.label23.Text = "Crystal";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(293, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "Mine";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(353, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 13);
            this.label25.TabIndex = 22;
            this.label25.Text = "Enemy";
            // 
            // tcm_Objects_crystalMineDistanceUpDown
            // 
            this.tcm_Objects_crystalMineDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_crystalMineDistanceUpDown.Location = new System.Drawing.Point(293, 28);
            this.tcm_Objects_crystalMineDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_crystalMineDistanceUpDown.Name = "tcm_Objects_crystalMineDistanceUpDown";
            this.tcm_Objects_crystalMineDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_crystalMineDistanceUpDown.TabIndex = 23;
            this.tcm_Objects_crystalMineDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_crystalEnemyDistanceUpDown
            // 
            this.tcm_Objects_crystalEnemyDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_crystalEnemyDistanceUpDown.Location = new System.Drawing.Point(353, 28);
            this.tcm_Objects_crystalEnemyDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_crystalEnemyDistanceUpDown.Name = "tcm_Objects_crystalEnemyDistanceUpDown";
            this.tcm_Objects_crystalEnemyDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_crystalEnemyDistanceUpDown.TabIndex = 24;
            this.tcm_Objects_crystalEnemyDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_mineCrystalDistanceUpDown
            // 
            this.tcm_Objects_mineCrystalDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_mineCrystalDistanceUpDown.Location = new System.Drawing.Point(233, 53);
            this.tcm_Objects_mineCrystalDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_mineCrystalDistanceUpDown.Name = "tcm_Objects_mineCrystalDistanceUpDown";
            this.tcm_Objects_mineCrystalDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_mineCrystalDistanceUpDown.TabIndex = 25;
            this.tcm_Objects_mineCrystalDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_mineMineDistanceUpDown
            // 
            this.tcm_Objects_mineMineDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_mineMineDistanceUpDown.Location = new System.Drawing.Point(293, 53);
            this.tcm_Objects_mineMineDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_mineMineDistanceUpDown.Name = "tcm_Objects_mineMineDistanceUpDown";
            this.tcm_Objects_mineMineDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_mineMineDistanceUpDown.TabIndex = 26;
            this.tcm_Objects_mineMineDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_mineEnemyDistanceUpDown
            // 
            this.tcm_Objects_mineEnemyDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_mineEnemyDistanceUpDown.Location = new System.Drawing.Point(353, 53);
            this.tcm_Objects_mineEnemyDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_mineEnemyDistanceUpDown.Name = "tcm_Objects_mineEnemyDistanceUpDown";
            this.tcm_Objects_mineEnemyDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_mineEnemyDistanceUpDown.TabIndex = 27;
            this.tcm_Objects_mineEnemyDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_enemyCrystalDistanceUpDown
            // 
            this.tcm_Objects_enemyCrystalDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_enemyCrystalDistanceUpDown.Location = new System.Drawing.Point(233, 78);
            this.tcm_Objects_enemyCrystalDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_enemyCrystalDistanceUpDown.Name = "tcm_Objects_enemyCrystalDistanceUpDown";
            this.tcm_Objects_enemyCrystalDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_enemyCrystalDistanceUpDown.TabIndex = 28;
            this.tcm_Objects_enemyCrystalDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_enemyMineDistanceUpDown
            // 
            this.tcm_Objects_enemyMineDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_enemyMineDistanceUpDown.Location = new System.Drawing.Point(293, 78);
            this.tcm_Objects_enemyMineDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_enemyMineDistanceUpDown.Name = "tcm_Objects_enemyMineDistanceUpDown";
            this.tcm_Objects_enemyMineDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_enemyMineDistanceUpDown.TabIndex = 29;
            this.tcm_Objects_enemyMineDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_enemyEnemyDistanceUpDown
            // 
            this.tcm_Objects_enemyEnemyDistanceUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tcm_Objects_enemyEnemyDistanceUpDown.Location = new System.Drawing.Point(353, 78);
            this.tcm_Objects_enemyEnemyDistanceUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.tcm_Objects_enemyEnemyDistanceUpDown.Name = "tcm_Objects_enemyEnemyDistanceUpDown";
            this.tcm_Objects_enemyEnemyDistanceUpDown.Size = new System.Drawing.Size(50, 20);
            this.tcm_Objects_enemyEnemyDistanceUpDown.TabIndex = 30;
            this.tcm_Objects_enemyEnemyDistanceUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tcm_Objects_clearEnemiesButton
            // 
            this.tcm_Objects_clearEnemiesButton.Location = new System.Drawing.Point(291, 3);
            this.tcm_Objects_clearEnemiesButton.Name = "tcm_Objects_clearEnemiesButton";
            this.tcm_Objects_clearEnemiesButton.Size = new System.Drawing.Size(90, 23);
            this.tcm_Objects_clearEnemiesButton.TabIndex = 15;
            this.tcm_Objects_clearEnemiesButton.Text = "Clear Enemies";
            this.tcm_Objects_clearEnemiesButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_clearEnemiesButton.Click += new System.EventHandler(this.tcm_Objects_clearEnemiesButton_Click);
            // 
            // tcm_Objects_clearMinesButton
            // 
            this.tcm_Objects_clearMinesButton.Location = new System.Drawing.Point(195, 3);
            this.tcm_Objects_clearMinesButton.Name = "tcm_Objects_clearMinesButton";
            this.tcm_Objects_clearMinesButton.Size = new System.Drawing.Size(90, 23);
            this.tcm_Objects_clearMinesButton.TabIndex = 14;
            this.tcm_Objects_clearMinesButton.Text = "Clear Mines";
            this.tcm_Objects_clearMinesButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_clearMinesButton.Click += new System.EventHandler(this.tcm_Objects_clearMinesButton_Click);
            // 
            // tcm_Objects_clearAllButton
            // 
            this.tcm_Objects_clearAllButton.Location = new System.Drawing.Point(99, 3);
            this.tcm_Objects_clearAllButton.Name = "tcm_Objects_clearAllButton";
            this.tcm_Objects_clearAllButton.Size = new System.Drawing.Size(90, 23);
            this.tcm_Objects_clearAllButton.TabIndex = 5;
            this.tcm_Objects_clearAllButton.Text = "Clear All";
            this.tcm_Objects_clearAllButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_clearAllButton.Click += new System.EventHandler(this.tcm_Objects_clearAllButton_Click);
            // 
            // tcm_Objects_autoPlaceButton
            // 
            this.tcm_Objects_autoPlaceButton.Location = new System.Drawing.Point(3, 3);
            this.tcm_Objects_autoPlaceButton.Name = "tcm_Objects_autoPlaceButton";
            this.tcm_Objects_autoPlaceButton.Size = new System.Drawing.Size(90, 23);
            this.tcm_Objects_autoPlaceButton.TabIndex = 4;
            this.tcm_Objects_autoPlaceButton.Text = "Auto Place";
            this.tcm_Objects_autoPlaceButton.UseVisualStyleBackColor = true;
            this.tcm_Objects_autoPlaceButton.Click += new System.EventHandler(this.tcm_Objects_autoPlaceButton_Click);
            // 
            // tcMain_Trees
            // 
            this.tcMain_Trees.Controls.Add(this.tcm_Trees_clearButton);
            this.tcMain_Trees.Controls.Add(this.tcm_Trees_generateButton);
            this.tcMain_Trees.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Trees.Name = "tcMain_Trees";
            this.tcMain_Trees.Size = new System.Drawing.Size(626, 185);
            this.tcMain_Trees.TabIndex = 2;
            this.tcMain_Trees.Text = "3 Trees";
            this.tcMain_Trees.UseVisualStyleBackColor = true;
            // 
            // tcm_Trees_clearButton
            // 
            this.tcm_Trees_clearButton.Location = new System.Drawing.Point(316, 81);
            this.tcm_Trees_clearButton.Name = "tcm_Trees_clearButton";
            this.tcm_Trees_clearButton.Size = new System.Drawing.Size(75, 23);
            this.tcm_Trees_clearButton.TabIndex = 5;
            this.tcm_Trees_clearButton.Text = "Clear";
            this.tcm_Trees_clearButton.UseVisualStyleBackColor = true;
            this.tcm_Trees_clearButton.Click += new System.EventHandler(this.tcm_Trees_clearButton_Click);
            // 
            // tcm_Trees_generateButton
            // 
            this.tcm_Trees_generateButton.Location = new System.Drawing.Point(235, 81);
            this.tcm_Trees_generateButton.Name = "tcm_Trees_generateButton";
            this.tcm_Trees_generateButton.Size = new System.Drawing.Size(75, 23);
            this.tcm_Trees_generateButton.TabIndex = 4;
            this.tcm_Trees_generateButton.Text = "Generate";
            this.tcm_Trees_generateButton.UseVisualStyleBackColor = true;
            this.tcm_Trees_generateButton.Click += new System.EventHandler(this.tcm_Trees_generateButton_Click);
            // 
            // tcMain_Write
            // 
            this.tcMain_Write.Controls.Add(this.tcm_Write_negateButton);
            this.tcMain_Write.Controls.Add(this.tcm_Write_writeButton);
            this.tcMain_Write.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Write.Name = "tcMain_Write";
            this.tcMain_Write.Size = new System.Drawing.Size(626, 185);
            this.tcMain_Write.TabIndex = 3;
            this.tcMain_Write.Text = "4 Write";
            this.tcMain_Write.UseVisualStyleBackColor = true;
            // 
            // tcm_Write_negateButton
            // 
            this.tcm_Write_negateButton.Location = new System.Drawing.Point(276, 95);
            this.tcm_Write_negateButton.Name = "tcm_Write_negateButton";
            this.tcm_Write_negateButton.Size = new System.Drawing.Size(75, 23);
            this.tcm_Write_negateButton.TabIndex = 7;
            this.tcm_Write_negateButton.Text = "Negate";
            this.tcm_Write_negateButton.UseVisualStyleBackColor = true;
            this.tcm_Write_negateButton.Visible = false;
            this.tcm_Write_negateButton.Click += new System.EventHandler(this.tcm_Write_negateButton_Click);
            // 
            // tcm_Write_writeButton
            // 
            this.tcm_Write_writeButton.Location = new System.Drawing.Point(276, 66);
            this.tcm_Write_writeButton.Name = "tcm_Write_writeButton";
            this.tcm_Write_writeButton.Size = new System.Drawing.Size(75, 23);
            this.tcm_Write_writeButton.TabIndex = 6;
            this.tcm_Write_writeButton.Text = "Write";
            this.tcm_Write_writeButton.UseVisualStyleBackColor = true;
            this.tcm_Write_writeButton.Click += new System.EventHandler(this.tcm_Write_writeButton_Click);
            // 
            // previewPictureBox
            // 
            this.previewPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewPictureBox.Location = new System.Drawing.Point(0, 0);
            this.previewPictureBox.Name = "previewPictureBox";
            this.previewPictureBox.Size = new System.Drawing.Size(759, 313);
            this.previewPictureBox.TabIndex = 17;
            this.previewPictureBox.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.previewPictureBox);
            this.panel1.Location = new System.Drawing.Point(13, 237);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(759, 313);
            this.panel1.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(236, 38);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(248, 13);
            this.label26.TabIndex = 19;
            this.label26.Text = "Minimum Distance (% of Terrain Size, -1 for Default)";
            // 
            // CastleStoryMapGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.stageTabControl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CastleStoryMapGenerator";
            this.Text = "Castle Story Map Generator";
            ((System.ComponentModel.ISupportInitialize)(this.yLengthUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLengthUpDown)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.stageTabControl.ResumeLayout(false);
            this.tcMain_Terrain.ResumeLayout(false);
            this.stage_TerrainTabControl.ResumeLayout(false);
            this.tcMain_Terrain_NoiseGrid.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_AmplitudeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_FrequencyUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_PersistenceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_UpperBoundUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_LowerBoundUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_VariationUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_ThicknessUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_OctavesUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_HighPassUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Terrain_NoiseGrid_LowPassUpDown)).EndInit();
            this.tcMain_Terrain_Procedural.ResumeLayout(false);
            this.tcMain_Terrain_Procedural.PerformLayout();
            this.tcMain_Objects.ResumeLayout(false);
            this.tcMain_Objects.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_crystalMineDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_crystalEnemyDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_mineCrystalDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_mineMineDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_mineEnemyDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_enemyCrystalDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_enemyMineDistanceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcm_Objects_enemyEnemyDistanceUpDown)).EndInit();
            this.tcMain_Trees.ResumeLayout(false);
            this.tcMain_Write.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yLengthUpDown;
        private System.Windows.Forms.NumericUpDown xLengthUpDown;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox terrainCompletedCheckBox;
        private System.Windows.Forms.CheckBox objectsCompletedCheckBox;
        private System.Windows.Forms.CheckBox treesCompletedCheckBox;
        private System.Windows.Forms.CheckBox writtenCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl stageTabControl;
        private System.Windows.Forms.TabPage tcMain_Terrain;
        private System.Windows.Forms.TabPage tcMain_Objects;
        private System.Windows.Forms.TabPage tcMain_Trees;
        private System.Windows.Forms.TabPage tcMain_Write;
        private System.Windows.Forms.PictureBox previewPictureBox;
        private System.Windows.Forms.Button tcm_Terrain_clearButton;
        private System.Windows.Forms.Button tcm_Terrain_generateButton;
        private System.Windows.Forms.Button tcm_Objects_clearAllButton;
        private System.Windows.Forms.Button tcm_Objects_autoPlaceButton;
        private System.Windows.Forms.Button tcm_Trees_clearButton;
        private System.Windows.Forms.Button tcm_Trees_generateButton;
        private System.Windows.Forms.TabControl stage_TerrainTabControl;
        private System.Windows.Forms.TabPage tcMain_Terrain_Procedural;
        private System.Windows.Forms.TabPage tcMain_Terrain_NoiseGrid;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_OctavesDefaultButton;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_OctavesUpDown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_AmplitudeDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_FrequencyDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_PersistenceDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_UpperBoundDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_LowerBoundDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_VariationDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_ThicknessDefaultButton;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_HighPassDefaultButton;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_LowPassDefaultButton;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox tcm_Terrain_NoiseGrid_LowerBoundCheckBox;
        private System.Windows.Forms.CheckBox tcm_Terrain_NoiseGrid_HighPassCheckBox;
        private System.Windows.Forms.CheckBox tcm_Terrain_NoiseGrid_LowPassCheckBox;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_AmplitudeUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_FrequencyUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_PersistenceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_UpperBoundUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_LowerBoundUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_VariationUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_ThicknessUpDown;
        private System.Windows.Forms.Button tcm_Terrain_NoiseGrid_DefaultAllButton;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_HighPassUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Terrain_NoiseGrid_LowPassUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button tcm_Objects_clearEnemiesButton;
        private System.Windows.Forms.Button tcm_Objects_clearMinesButton;
        private System.Windows.Forms.Button tcm_Objects_placeEnemyButton;
        private System.Windows.Forms.Button tcm_Objects_placeMineButton;
        private System.Windows.Forms.Button tcm_Objects_placeCrystalPlayerButton;
        private System.Windows.Forms.Button tcm_Write_negateButton;
        private System.Windows.Forms.Button tcm_Write_writeButton;
        private System.Windows.Forms.Button tcm_Objects_removeLastMineButton;
        private System.Windows.Forms.Button tcm_Objects_removeLastEnemyButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button tcm_Terrain_largestIslandButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown tcm_Objects_crystalMineDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_crystalEnemyDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_mineCrystalDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_mineMineDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_mineEnemyDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_enemyCrystalDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_enemyMineDistanceUpDown;
        private System.Windows.Forms.NumericUpDown tcm_Objects_enemyEnemyDistanceUpDown;
        private System.Windows.Forms.Label label26;
    }
}