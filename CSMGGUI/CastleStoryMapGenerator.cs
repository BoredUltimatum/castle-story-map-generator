﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CSMGLibrary;
using CSMGLibrary.TerrainGenerators;

namespace CSMGGUI
{
    public partial class CastleStoryMapGenerator : Form
    {
        private enum Stages
        {
            terrain,
            objects,
            trees,
            write
        }

        private Level level;
        private NoiseGrid gen;
        private Bitmap terrainBM;
        private bool terrainValid;
        private bool objectsValid;
        private bool minesValid;
        private bool enemiesValid;
        private bool treesValid;
        private bool levelWritten;
        private List<CSMGLibrary.LevelObjects.LevelObject> lastPlacedMines;
        private List<CSMGLibrary.LevelObjects.LevelObject> lastPlacedEnemies;
        Color playerColor = Color.FromArgb(128, 128, 0);
        Color mineColor = Color.FromArgb(0, 0, 128);
        Color enemyColor = Color.FromArgb(128, 0, 0);
        Color treeColor = Color.FromArgb(0, 128, 0);

        public CastleStoryMapGenerator()
        {
            InitializeComponent();

            terrainValid = false;
            objectsValid = false;
            treesValid = false;
            levelWritten = false;
            lastPlacedMines = new List<CSMGLibrary.LevelObjects.LevelObject>();
            lastPlacedEnemies = new List<CSMGLibrary.LevelObjects.LevelObject>();

            stageTabControl.TabPages.Remove(tcMain_Write);
            stageTabControl.TabPages.Remove(tcMain_Trees);
            stageTabControl.TabPages.Remove(tcMain_Objects);

            level = new Level((int)xLengthUpDown.Value, (int)yLengthUpDown.Value);
            gen = new NoiseGrid();
            terrainBM = new Bitmap(level.XLength, level.YLength);

            tcm_Terrain_NoiseGrid_DefaultAll();
        }

        private void completedStage(Stages stage, bool selectNextTab)
        {
            if (stage == Stages.terrain)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                if (!selectNextTab)
                    stageTabControl.SelectedTab = tcMain_Terrain;
                else
                    stageTabControl.SelectedTab = tcMain_Objects;

                terrainValid = true;
                objectsValid = false;
                minesValid = false;
                enemiesValid = false;
                treesValid = false;
                levelWritten = false;
                terrainCompletedCheckBox.Checked = true;
                objectsCompletedCheckBox.Checked = false;
                treesCompletedCheckBox.Checked = false;
                writtenCheckBox.Checked = false;
                lastPlacedMines.Clear();
                tcm_Objects_removeLastMineButton.Enabled = false;
                lastPlacedEnemies.Clear();
                tcm_Objects_removeLastEnemyButton.Enabled = false;
            } else
            if (stage == Stages.objects)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Trees);
                if (!selectNextTab)
                    stageTabControl.SelectedTab = tcMain_Objects;
                else
                    stageTabControl.SelectedTab = tcMain_Trees;

                terrainValid = true;
                objectsValid = true;
                treesValid = false;
                levelWritten = false;
                terrainCompletedCheckBox.Checked = true;
                objectsCompletedCheckBox.Checked = true;
                treesCompletedCheckBox.Checked = false;
                writtenCheckBox.Checked = false;
            } else
            if (stage == Stages.trees)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Trees);
                stageTabControl.TabPages.Add(tcMain_Write);
                if (!selectNextTab)
                    stageTabControl.SelectedTab = tcMain_Trees;
                else
                    stageTabControl.SelectedTab = tcMain_Write;

                terrainValid = true;
                objectsValid = true;
                treesValid = true;
                levelWritten = false;
                terrainCompletedCheckBox.Checked = true;
                objectsCompletedCheckBox.Checked = true;
                treesCompletedCheckBox.Checked = true;
                writtenCheckBox.Checked = false;
            } else
            if (stage == Stages.write)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Trees);
                stageTabControl.TabPages.Add(tcMain_Write);
                stageTabControl.SelectedTab = tcMain_Write;

                terrainValid = true;
                objectsValid = true;
                treesValid = true;
                levelWritten = true;
                terrainCompletedCheckBox.Checked = true;
                objectsCompletedCheckBox.Checked = true;
                treesCompletedCheckBox.Checked = true;
                writtenCheckBox.Checked = true;
            }

            updateImage();
        }

        private void negateStage(Stages stage)
        {
            if (stage == Stages.terrain)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.SelectedTab = tcMain_Terrain;

                terrainValid = false;
                objectsValid = false;
                minesValid = false;
                enemiesValid = false;
                treesValid = false;
                levelWritten = false;
                terrainCompletedCheckBox.Checked = false;
                objectsCompletedCheckBox.Checked = false;
                treesCompletedCheckBox.Checked = false;
                writtenCheckBox.Checked = false;
                lastPlacedMines.Clear();
                tcm_Objects_removeLastMineButton.Enabled = false;
                lastPlacedEnemies.Clear();
                tcm_Objects_removeLastEnemyButton.Enabled = false;
            } else
            if (stage == Stages.objects)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                stageTabControl.SelectedTab = tcMain_Objects;

                objectsValid = false;
                minesValid = false;
                enemiesValid = false;
                treesValid = false;
                levelWritten = false;
                objectsCompletedCheckBox.Checked = false;
                treesCompletedCheckBox.Checked = false;
                writtenCheckBox.Checked = false;
                lastPlacedMines.Clear();
                tcm_Objects_removeLastMineButton.Enabled = false;
                lastPlacedEnemies.Clear();
                tcm_Objects_removeLastEnemyButton.Enabled = false;
            } else
            if (stage == Stages.trees)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Trees);
                stageTabControl.SelectedTab = tcMain_Trees;

                treesValid = false;
                levelWritten = false;
                treesCompletedCheckBox.Checked = false;
                writtenCheckBox.Checked = false;
            } else
            if (stage == Stages.write)
            {
                stageTabControl.TabPages.Remove(tcMain_Terrain);
                stageTabControl.TabPages.Remove(tcMain_Write);
                stageTabControl.TabPages.Remove(tcMain_Trees);
                stageTabControl.TabPages.Remove(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Terrain);
                stageTabControl.TabPages.Add(tcMain_Objects);
                stageTabControl.TabPages.Add(tcMain_Trees);
                stageTabControl.TabPages.Add(tcMain_Write);
                stageTabControl.SelectedTab = tcMain_Write;

                levelWritten = false;
                writtenCheckBox.Checked = false;
            }

            updateImage();
        }


        private void updateImage()
        {
            if (level.XLength != terrainBM.Width || level.YLength != terrainBM.Height)
                terrainBM = new Bitmap(level.XLength, level.YLength);

            if (terrainValid)
            {
                for (int i = 0; i < level.XLength; i++)
                {
                    for (int j = 0; j < level.YLength; j++)
                    {
                        terrainBM.SetPixel(i, j, Color.FromArgb(Byte.MaxValue - level.Terrain.Heights[i, j], Byte.MaxValue - level.Terrain.Heights[i, j], Byte.MaxValue - level.Terrain.Heights[i, j]));
                        if (/*treesShowCheckBox.Checked &&*/ treesValid && level.Trees[i, j] == true)
                            terrainBM.SetPixel(i, j, treeColor);
                    }
                }
            }
            else
            {
                for (int i = 0; i < level.XLength; i++)
                    for (int j = 0; j < level.YLength; j++)
                        terrainBM.SetPixel(i, j, Color.FromArgb(Byte.MaxValue, Byte.MaxValue, Byte.MaxValue));
                goto completeImage;
            }
            if (/*objectsShowCheckBox.Checked &&*/ objectsValid)
            {
                terrainBM.SetPixel(level.PlayerSpawn.XPos, level.PlayerSpawn.YPos, playerColor);
                int xMin = Math.Max(0, level.PlayerSpawn.XPos-10);
                int xMax = Math.Min(level.PlayerSpawn.XPos+10, level.XLength);
                int yMin = Math.Max(0, level.PlayerSpawn.YPos-10);
                int yMax = Math.Min(level.YLength, level.PlayerSpawn.YPos+10);
                for (int i = xMin; i < xMax; i++)
                    terrainBM.SetPixel(i, level.PlayerSpawn.YPos, playerColor);
                for (int j = yMin; j < yMax; j++)
                    terrainBM.SetPixel(level.PlayerSpawn.XPos, j, playerColor);
            }
            if (minesValid)
            {
                foreach (CSMGLibrary.LevelObjects.LevelObject obj in level.Mines)
                {
                    terrainBM.SetPixel(obj.XPos, obj.YPos, mineColor);
                    int xMin = Math.Max(0, obj.XPos - 10);
                    int xMax = Math.Min(obj.XPos + 10, level.XLength);
                    int yMin = Math.Max(0, obj.YPos - 10);
                    int yMax = Math.Min(level.YLength, obj.YPos + 10);
                    for (int i = xMin; i < xMax; i++)
                        terrainBM.SetPixel(i, obj.YPos, mineColor);
                    for (int j = yMin; j < yMax; j++)
                        terrainBM.SetPixel(obj.XPos, j, mineColor);
                }
            }
            if (enemiesValid)
            {
                foreach (CSMGLibrary.LevelObjects.LevelObject obj in level.EnemySpawns)
                {
                    terrainBM.SetPixel(obj.XPos, obj.YPos, enemyColor);
                    int xMin = Math.Max(0, obj.XPos - 10);
                    int xMax = Math.Min(obj.XPos + 10, level.XLength);
                    int yMin = Math.Max(0, obj.YPos - 10);
                    int yMax = Math.Min(level.YLength, obj.YPos + 10);
                    for (int i = xMin; i < xMax; i++)
                        terrainBM.SetPixel(i, obj.YPos, enemyColor);
                    for (int j = yMin; j < yMax; j++)
                        terrainBM.SetPixel(obj.XPos, j, enemyColor);
                }
            }

            completeImage:
            previewPictureBox.Image = (Image)terrainBM;
            previewPictureBox.Image.RotateFlip(RotateFlipType.RotateNoneFlipY);
            previewPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
        }

    // TERRAIN CONTROLS
        private void tcm_Terrain_generateButton_Click(object sender, EventArgs e)
        {
            if (level.XLength != ((int)xLengthUpDown.Value) || level.YLength != ((int)yLengthUpDown.Value))
                level.Resize((int)xLengthUpDown.Value, (int)yLengthUpDown.Value);

            gen.setParameters(
                (byte)tcm_Terrain_NoiseGrid_UpperBoundUpDown.Value,
                (byte)tcm_Terrain_NoiseGrid_LowerBoundUpDown.Value,
                (byte)tcm_Terrain_NoiseGrid_ThicknessUpDown.Value,
                (byte)tcm_Terrain_NoiseGrid_VariationUpDown.Value,
                (int)tcm_Terrain_NoiseGrid_OctavesUpDown.Value,
                (double)tcm_Terrain_NoiseGrid_AmplitudeUpDown.Value,
                (double)tcm_Terrain_NoiseGrid_FrequencyUpDown.Value,
                (double)tcm_Terrain_NoiseGrid_PersistenceUpDown.Value,
                (byte)tcm_Terrain_NoiseGrid_LowPassUpDown.Value,
                (byte)tcm_Terrain_NoiseGrid_HighPassUpDown.Value);
            if (!tcm_Terrain_NoiseGrid_LowerBoundCheckBox.Checked)
                gen.lowerBound = Byte.MaxValue;
            gen.highPassFilterActive = tcm_Terrain_NoiseGrid_HighPassCheckBox.Checked;
            gen.lowPassFilterActive = tcm_Terrain_NoiseGrid_LowPassCheckBox.Checked;

            level.GenerateTerrain(gen);
            completedStage(Stages.terrain, false);
        }

        private void tcm_Terrain_clearButton_Click(object sender, EventArgs e)
        {
            level.clearAll();

            negateStage(Stages.terrain);
        }

        private void tcm_Terrain_largestIslandButton_Click(object sender, EventArgs e)
        {
            level.ReduceToLargestIsland();

            updateImage();
        }

    // OBJECTS CONTROLS
        private void tcm_Objects_placeCrystalPlayerButton_Click(object sender, EventArgs e)
        {
            level.placePlayer();

            completedStage(Stages.objects, false);
        }

        private void tcm_Objects_placeMineButton_Click(object sender, EventArgs e)
        {
            CSMGLibrary.LevelObjects.LevelObject newMine = level.placeMine((double)(tcm_Objects_mineMineDistanceUpDown.Value/100), (double)(tcm_Objects_mineCrystalDistanceUpDown.Value/100), (double)(tcm_Objects_mineCrystalDistanceUpDown.Value/100), (double)(tcm_Objects_mineEnemyDistanceUpDown.Value/100), 0);
            if (newMine != null)
            {
                lastPlacedMines.Add(newMine);
                tcm_Objects_removeLastMineButton.Enabled = true;
            }
            minesValid = true;

            updateImage();
        }

        private void tcm_Objects_removeLastMineButton_Click(object sender, EventArgs e)
        {
            if (lastPlacedMines.Count > 0)
            {
                CSMGLibrary.LevelObjects.LevelObject mine = lastPlacedMines[lastPlacedMines.Count - 1];
                level.clearMine(mine);
                lastPlacedMines.Remove(mine);
                if (lastPlacedMines.Count == 0)
                    tcm_Objects_removeLastMineButton.Enabled = false;
            }

            updateImage();
        }

        private void tcm_Objects_placeEnemyButton_Click(object sender, EventArgs e)
        {
            CSMGLibrary.LevelObjects.LevelObject newEnemy = level.placeEnemy((double)(tcm_Objects_enemyEnemyDistanceUpDown.Value/100), (double)(tcm_Objects_enemyCrystalDistanceUpDown.Value/100), (double)(tcm_Objects_enemyCrystalDistanceUpDown.Value/100), (double)(tcm_Objects_enemyMineDistanceUpDown.Value/100), 0);
            if (newEnemy != null)
            {
                lastPlacedEnemies.Add(newEnemy);
                tcm_Objects_removeLastEnemyButton.Enabled = true;
            }
            enemiesValid = true;

            updateImage();
        }

        private void tcm_Objects_removeLastEnemyButton_Click(object sender, EventArgs e)
        {
            if (lastPlacedEnemies.Count > 0)
            {
                CSMGLibrary.LevelObjects.LevelObject enemy = lastPlacedEnemies[lastPlacedEnemies.Count - 1];
                level.clearEnemy(enemy);
                lastPlacedEnemies.Remove(enemy);
                if (lastPlacedEnemies.Count == 0)
                    tcm_Objects_removeLastEnemyButton.Enabled = false;
            }

            updateImage();
        }

        private void tcm_Objects_clearMinesButton_Click(object sender, EventArgs e)
        {
            level.clearMines();
            lastPlacedMines.Clear();
            tcm_Objects_removeLastMineButton.Enabled = false;
            minesValid = false;

            updateImage();
        }

        private void tcm_Objects_clearEnemiesButton_Click(object sender, EventArgs e)
        {
            level.clearEnemies();
            lastPlacedEnemies.Clear();
            tcm_Objects_removeLastEnemyButton.Enabled = false;
            enemiesValid = false;

            updateImage();
        }

        private void tcm_Objects_autoPlaceButton_Click(object sender, EventArgs e)
        {
            level.placePlayer();
            List<CSMGLibrary.LevelObjects.LevelObject> newMines = level.placeMines(2);
            if (newMines != null)
            {
                lastPlacedMines.AddRange(newMines);
                if (lastPlacedMines.Count > 0)
                    tcm_Objects_removeLastMineButton.Enabled = true;
            }
            List<CSMGLibrary.LevelObjects.LevelObject> newEnemies = level.placeEnemies(2);
            if (newEnemies != null)
            {
                lastPlacedEnemies.AddRange(newEnemies);
                if (lastPlacedEnemies.Count > 0)
                    tcm_Objects_removeLastEnemyButton.Enabled = true;
            }
            minesValid = true;
            enemiesValid = true;

            completedStage(Stages.objects, false);
        }

        private void tcm_Objects_clearAllButton_Click(object sender, EventArgs e)
        {
            level.clearObjects();
            level.resetPlayer();
            level.resetCrystal();
            lastPlacedMines.Clear();
            tcm_Objects_removeLastMineButton.Enabled = false;
            minesValid = false;
            lastPlacedEnemies.Clear();
            tcm_Objects_removeLastEnemyButton.Enabled = false;
            enemiesValid = false;

            negateStage(Stages.objects);
        }

    // TREES CONTROLS
        private void tcm_Trees_generateButton_Click(object sender, EventArgs e)
        {
            level.GenerateTrees();

            completedStage(Stages.trees, false);
        }

        private void tcm_Trees_clearButton_Click(object sender, EventArgs e)
        {
            level.clearTrees();

            negateStage(Stages.trees);
        }

    // WRITE CONTROLS
        private void tcm_Write_writeButton_Click(object sender, EventArgs e)
        {
            level.WriteLevel();

            completedStage(Stages.write, false);
        }

        private void tcm_Write_negateButton_Click(object sender, EventArgs e)
        {
            negateStage(Stages.write);
        }

    // DEFAULTS
        private void tcm_Terrain_NoiseGrid_DefaultAllButton_Click(object sender, EventArgs e)
        {
            tcm_Terrain_NoiseGrid_DefaultAll();
        }

        private void tcm_Terrain_NoiseGrid_DefaultAll()
        {
            tcm_Terrain_NoiseGrid_OctavesUpDown.Value = NoiseGrid.DEFAULT_OCTAVES;
            tcm_Terrain_NoiseGrid_AmplitudeUpDown.Value = (decimal)NoiseGrid.DEFAULT_AMPLITUDE;
            tcm_Terrain_NoiseGrid_FrequencyUpDown.Value = (decimal)NoiseGrid.DEFAULT_FREQUENCY;
            tcm_Terrain_NoiseGrid_PersistenceUpDown.Value = (decimal)NoiseGrid.DEFAULT_PERSISTENCE;
            tcm_Terrain_NoiseGrid_UpperBoundUpDown.Value = NoiseGrid.DEFAULT_UPPERBOUND;
            tcm_Terrain_NoiseGrid_LowerBoundUpDown.Value = NoiseGrid.DEFAULT_LOWERBOUND;
            tcm_Terrain_NoiseGrid_LowerBoundCheckBox.Checked = false;
            tcm_Terrain_NoiseGrid_VariationUpDown.Value = NoiseGrid.DEFAULT_VARIATION;
            tcm_Terrain_NoiseGrid_ThicknessUpDown.Value = NoiseGrid.DEFAULT_THICKNESS;
            tcm_Terrain_NoiseGrid_HighPassUpDown.Value = NoiseGrid.DEFAULT_HIGHPASS;
            tcm_Terrain_NoiseGrid_HighPassCheckBox.Checked = NoiseGrid.DEFAULT_HIGHPASSACTIVE;
            tcm_Terrain_NoiseGrid_LowPassUpDown.Value = NoiseGrid.DEFAULT_LOWPASS;
            tcm_Terrain_NoiseGrid_LowPassCheckBox.Checked = NoiseGrid.DEFAULT_LOWPASSACTIVE;
        }

        private void tcm_Terrain_NoiseGrid_ParameterDefaultButton_Click(object sender, EventArgs e)
        {
            if (sender == tcm_Terrain_NoiseGrid_OctavesDefaultButton)
            {
                tcm_Terrain_NoiseGrid_OctavesUpDown.Value = NoiseGrid.DEFAULT_OCTAVES;
            } else
            if (sender == tcm_Terrain_NoiseGrid_AmplitudeDefaultButton)
            {
                tcm_Terrain_NoiseGrid_AmplitudeUpDown.Value = (decimal)NoiseGrid.DEFAULT_AMPLITUDE;
            } else
            if (sender == tcm_Terrain_NoiseGrid_FrequencyDefaultButton)
            {
                tcm_Terrain_NoiseGrid_FrequencyUpDown.Value = (decimal)NoiseGrid.DEFAULT_FREQUENCY;
            } else
            if (sender == tcm_Terrain_NoiseGrid_PersistenceDefaultButton)
            {
                tcm_Terrain_NoiseGrid_PersistenceUpDown.Value = (decimal)NoiseGrid.DEFAULT_PERSISTENCE;
            } else
            if (sender == tcm_Terrain_NoiseGrid_UpperBoundDefaultButton)
            {
                tcm_Terrain_NoiseGrid_UpperBoundUpDown.Value = NoiseGrid.DEFAULT_UPPERBOUND;
            } else
            if (sender == tcm_Terrain_NoiseGrid_LowerBoundDefaultButton)
            {
                tcm_Terrain_NoiseGrid_LowerBoundUpDown.Value = NoiseGrid.DEFAULT_LOWERBOUND;
                tcm_Terrain_NoiseGrid_LowerBoundCheckBox.Checked = false;
            } else
            if (sender == tcm_Terrain_NoiseGrid_VariationDefaultButton)
            {
                tcm_Terrain_NoiseGrid_VariationUpDown.Value = NoiseGrid.DEFAULT_VARIATION;
            } else
            if (sender == tcm_Terrain_NoiseGrid_ThicknessDefaultButton)
            {
                tcm_Terrain_NoiseGrid_ThicknessUpDown.Value = NoiseGrid.DEFAULT_THICKNESS;
            } else
            if (sender == tcm_Terrain_NoiseGrid_HighPassDefaultButton)
            {
                tcm_Terrain_NoiseGrid_HighPassUpDown.Value = NoiseGrid.DEFAULT_HIGHPASS;
                tcm_Terrain_NoiseGrid_HighPassCheckBox.Checked = NoiseGrid.DEFAULT_HIGHPASSACTIVE;
            } else
            if (sender == tcm_Terrain_NoiseGrid_LowPassDefaultButton)
            {
                tcm_Terrain_NoiseGrid_LowPassUpDown.Value = NoiseGrid.DEFAULT_LOWPASS;
                tcm_Terrain_NoiseGrid_LowPassCheckBox.Checked = NoiseGrid.DEFAULT_LOWPASSACTIVE;
            }
        }
    }
}
