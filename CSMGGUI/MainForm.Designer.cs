﻿namespace CSMGGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GenTerrain = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GenObjects = new System.Windows.Forms.Button();
            this.GenTrees = new System.Windows.Forms.Button();
            this.Write = new System.Windows.Forms.Button();
            this.GenAll = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GenTerrain
            // 
            this.GenTerrain.Location = new System.Drawing.Point(3, 55);
            this.GenTerrain.Name = "GenTerrain";
            this.GenTerrain.Size = new System.Drawing.Size(253, 23);
            this.GenTerrain.TabIndex = 1;
            this.GenTerrain.Text = "Step 1: Terrain";
            this.GenTerrain.UseVisualStyleBackColor = true;
            this.GenTerrain.Click += new System.EventHandler(this.GenTerrain_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.GenTerrain, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.GenObjects, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.GenTrees, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Write, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.GenAll, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 264);
            this.tableLayoutPanel1.TabIndex = 2;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // GenObjects
            // 
            this.GenObjects.Location = new System.Drawing.Point(3, 107);
            this.GenObjects.Name = "GenObjects";
            this.GenObjects.Size = new System.Drawing.Size(253, 23);
            this.GenObjects.TabIndex = 2;
            this.GenObjects.Text = "Step 2: Objects";
            this.GenObjects.UseVisualStyleBackColor = true;
            this.GenObjects.Click += new System.EventHandler(this.GenObjects_Click);
            // 
            // GenTrees
            // 
            this.GenTrees.Location = new System.Drawing.Point(3, 159);
            this.GenTrees.Name = "GenTrees";
            this.GenTrees.Size = new System.Drawing.Size(253, 23);
            this.GenTrees.TabIndex = 3;
            this.GenTrees.Text = "Step 3: Trees";
            this.GenTrees.UseVisualStyleBackColor = true;
            this.GenTrees.Click += new System.EventHandler(this.GenTrees_Click);
            // 
            // Write
            // 
            this.Write.Location = new System.Drawing.Point(3, 211);
            this.Write.Name = "Write";
            this.Write.Size = new System.Drawing.Size(253, 23);
            this.Write.TabIndex = 4;
            this.Write.Text = "Write Level";
            this.Write.UseVisualStyleBackColor = true;
            this.Write.Click += new System.EventHandler(this.Write_Click);
            // 
            // GenAll
            // 
            this.GenAll.Location = new System.Drawing.Point(3, 3);
            this.GenAll.Name = "GenAll";
            this.GenAll.Size = new System.Drawing.Size(256, 23);
            this.GenAll.TabIndex = 5;
            this.GenAll.Text = "Generate All and Write";
            this.GenAll.UseVisualStyleBackColor = true;
            this.GenAll.Click += new System.EventHandler(this.GenAll_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 288);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Castle Story Map Generator";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GenTerrain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button GenObjects;
        private System.Windows.Forms.Button GenTrees;
        private System.Windows.Forms.Button Write;
        private System.Windows.Forms.Button GenAll;
    }
}

